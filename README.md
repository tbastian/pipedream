
# Install

```bash
  ## create X86 instruction database from Intel XED.
  git submodule init
  git submodule update
  cd tools/extract-xed-instruction-database/
  cmake -B build -S .
  make -C build -j
  build/extract-xed-instruction-database print-instr-db -o ../../src/pipedream/asm/x86/instructions_xed.py
```
