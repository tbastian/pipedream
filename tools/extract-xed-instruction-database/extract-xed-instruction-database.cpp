///
/// Extract Pipedream instruction descriptions for Intel X86 from Intel XED.
///
/// The goal: Extract a comprehensive, simple, machine readable database
///           of all x86 instructions.
///           It must contain the number and type of operands and output for
///           every instruction as well as the flags it modifies.
/// Problem 1:  XEDs instruction database is a complicated, undocumented DSL.
///             The parser for the DSL and the AST for it is equally
///             undocumented and hard to understand.
/// Solution 1: Just use XEDs well documented, robust C API.
/// Problem 2:  XEDs C API only exposes the information we need in
///             xed_decoded_inst_t, the output of XEDs disassembler.
///             (if you look in the source the information is all there in
///              the xed_inst_t tables, it is just not exposed in the API)
/// Solution 2: Just iterate over all possible instructions, encode them,
///             decode them, and extract the information.
/// Problem 3:  XEDs encoder API only accepts 'iclass', not 'iform'.
///             So we can not reliably encode all variations of all x86
///             instructions.
///             In fact, sometimes if we encode and then decode an instruction
///             XED sometimes happily changes the 'iclass' and number and 'type'
///             of operands.
/// Solution 3: Beats me. We'll just have an incomplete database.
///             This also means that can not feed our instructions back into
///             the XED encoder and expect something reasonable.


/// We do this the most stupid way possible:
///   For every iclass in XED_ICLASS:
///      For every possible combination of operands for iclass:
///        inst = xed_decode(xed_decode(iclass, operands))
///
/// This way we can find the register classes etc. valid for every instruction
/// without parsing XEDs horrid machine description format.
///
/// Our notion of instruction is a bit different from that of XED.
/// One ICLASS can be represented as more than one pipedream instruction.
/// And since we don't have XEDs notion of scalable operation's we can even have
/// more than one pipedream instruction for an IFORM.
///
///
/// TODO: print more instruction info (see xed/examples/xed-ex1.c):
///         * flags read/written
///         * rep/repne prefix (F2, F3)
///         * address size prefix (67)
///         * the various 66 prefixes
///         * exceptions
///         * is broadcast
///         * AVX mask behaviour
///         * required CPU info bits
///         * valid chipsets

#include "config.hpp"

#include "Pipedream_Info.hpp"
#include "utils.hpp"
#include "XED_DB_Extraction_Utils.hpp"
#include "XED_Info.hpp"
#include <algorithm>
#include <cassert>
#include <cinttypes>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <functional>
#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <random>       // std::default_random_engine
#include <set>
#include <string>
#include <vector>
#include <xed/xed-interface.h>

struct Debug {
  static void print_operand(std::ostream &O, const XED_Instruction_Operand op) {
    print_operand_common(O, op);

    switch (op.type()) {
      case XED_OPERAND_TYPE_NT_LOOKUP_FN:
        O << "lookup: " << op.get_lookup_fn_name() << " ";
        break;
      case XED_OPERAND_TYPE_REG:
        O << "reg: " << op.fixed_reg_str() << " ";
        break;
      case XED_OPERAND_TYPE_IMM_CONST:
        O << "imm: " << op.imm_const() << " ";
        break;

      case XED_OPERAND_TYPE_IMM:
      case XED_OPERAND_TYPE_NT_LOOKUP_FN2:
      case XED_OPERAND_TYPE_NT_LOOKUP_FN4:
      case XED_OPERAND_TYPE_INVALID:
      case XED_OPERAND_TYPE_ERROR:
      case XED_OPERAND_TYPE_LAST:
        break;
    }
    abort();
  }

  static void print_operand(std::ostream &O, const XED_Decoded_Instruction_Operand op) {
    print_operand_common(O, op.static_op());

    switch (op.type()) {
      case XED_OPERAND_TYPE_NT_LOOKUP_FN:
        O << "lookup: " << op.nonterminal_str() << " ";
        O << "reg: " << op.reg_str() << " ";
        break;
      case XED_OPERAND_TYPE_REG:
        O << "reg: " << op.reg_str() << " ";
        break;
      case XED_OPERAND_TYPE_IMM_CONST:
        O << "imm: " << op.imm_const() << " ";
        break;

      case XED_OPERAND_TYPE_IMM:
      case XED_OPERAND_TYPE_NT_LOOKUP_FN4:
      case XED_OPERAND_TYPE_INVALID:
      case XED_OPERAND_TYPE_ERROR:
      case XED_OPERAND_TYPE_LAST:
        break;

      default:
        abort();
    }
  }

  static void print_operand(std::ostream &O, const xed_encoder_operand_t &op) {
    switch (op.type) {
      case XED_ENCODER_OPERAND_TYPE_REG:
        O << "REG=" << Pipedream::register_name(op.u.reg);
        break;
      case XED_ENCODER_OPERAND_TYPE_MEM:
        O << "MEM" << op.width_bits;
        O << "(";
        if (op.u.mem.base) {
          O << "base="  << Pipedream::register_name(op.u.mem.base) << ", ";
        }
        if (op.u.mem.index) {
          O << "index=" << Pipedream::register_name(op.u.mem.index) << ", ";
        }
        if (op.u.mem.disp.displacement_bits) {
          O << "disp="  << op.u.mem.disp.displacement_bits << "/" << op.u.mem.disp.displacement << ", ";
        }
        O << ")";
        break;
      case XED_ENCODER_OPERAND_TYPE_IMM0:
        O << "IMM" << op.width_bits << "=" << op.u.imm0;
        break;
      case XED_ENCODER_OPERAND_TYPE_IMM1:
        O << "IMM" << op.width_bits << "=" << op.u.imm1;
        break;
      default:
        abort();
    }
  }

  static void print_inst(std::ostream &O, const XED_Instruction inst) {
    O << "  XED_INST " << unsigned(inst.raw() - xed_inst_table_base()) << "\n";
    print_inst_commmon(O, inst);

    O << "    #operands:         " << inst.num_operands() << "\n";
    for(unsigned j = 0; j < inst.num_operands(); j++) {
      O << "      " << j << " ";
      print_operand(O, inst.operand(j));
      O << "\n";
    }
  }

  static void print_inst(std::ostream &O, const xed_encoder_instruction_t &inst) {
    O << "  xed_encoder_instruction_t{\n";
    O << "    iclass                   " << xed_iclass_enum_t2str(inst.iclass) << "\n";
    O << "    effective-operand-width: " << inst.effective_operand_width << "\n";
    O << "    effective-address-width: " << inst.effective_address_width << "\n";
    // @todo encoder state
    O << "    #operands:               " << inst.noperands << "\n";
    for(unsigned j = 0; j < inst.noperands; j++) {
      O << "      " << j << " ";
      print_operand(O, inst.operands[j]);
      O << "\n";
    }
    O << "}";
  }

  static void print_inst(std::ostream &O, const XED_Decoded_Instruction dec) {
    O << "  XED_DECODED_INST\n";
    O << "    machine-mode:            " << dec.get_machine_mode_bits() << "\n";
    O << "    stack-address-mode:      " << dec.get_stack_address_bits() << "\n";
    O << "    effective-operand-width: " << dec.effective_operand_width() << "\n";
    O << "    effective-address-width: " << dec.effective_address_width() << "\n";
    print_inst_commmon(O, dec.inst());

    O << "    #operands:         " << dec.num_operands() << "\n";
    for(unsigned j = 0; j < dec.num_operands(); j++) {
      O << "      " << j << " ";
      print_operand(O, dec.operand(j));
      O << "\n";
    }
  }

  static void print_inst_commmon(std::ostream &O, const XED_Instruction inst) {
    O << "    iclass                   " << inst.iclass_str() << " (" << inst.iclass() << ")\n";
    O << "    iform                    " << inst.iform_str() << "\n";
    O << "    category                 " << inst.category_str() << "\n";
    O << "    extension                " << inst.extension_str() << "\n";
    O << "    ISA                      " << inst.isa_set_str() << "\n";

    O << "    attributes:         [";
    for(const char *attr : inst.attribute_strs()) {
      O << attr << ", ";
    }
    O << "]\n";
  }

  static void print_operand_common(std::ostream &O, const XED_Instruction_Operand op) {
    O << "name: " << op.name_str() << " ";
    O << "visibility: " << op.visibility_str() << " ";
    O << "action: " << op.action_str() << " ";
    O << "type: " << op.type_str() << " ";
    O << "xtype: " << op.xtype_str() << " ";
  }

  static void print_encoder_state(std::ostream &O, const XED_Encoder_State &state) {
    O << "XED_ENCODER_STATE:\n";
    O << "  machine-mode:            " << state.cpu_mode.mode_str() << "\n";
    O << "  stack-address-mode:      " << state.cpu_mode.stack_addr_width_str() << "\n";
    O << "  effective-operand-width: " << state.effective_operand_width << "\n";
    O << "  effective-address-width: " << state.effective_address_width << "\n";
    O << "  allow-rex:               " << (state.allow_rex ? "YES" : "NO") << "\n";
  }
};

/// try to find XED instructions we can represent
struct Encode_Decode {
  /// Encode opcode + operands + cpu-state into machine code and have XED decode it back.
  /// Return None if the opcode + operands + cpu-state don't form a valid instruction.
  static std::optional<XED_Decoded_Instruction> encode_then_decode(
    XED_Encoder_State state,
    xed_iform_enum_t iform,
    xed_encoder_instruction_t encoder_instruction
  ) {
    /// NOTE: The XED encoder is pretty lenient. The decoder is not.
    ///       More precisely, instead of failing to encode the encoder uses illegal prefix bytes if you ask it to use bad operand/address widths.
    const bool strict = (state.effective_operand_width != 16);

    const xed_iclass_enum_t iclass = encoder_instruction.iclass;

    /// encode instruction to bytes

    xed_encoder_request_t encoder_request;
    const xed_bool_t request_ok = xed_convert_to_encoder_request(&encoder_request, &encoder_instruction);
    if (!request_ok) {
      if (strict) {
        std::cerr << "error: xed_convert_to_encoder_request failed\n";
        exit(1);
      }
      return std::nullopt;
    }

    xed_uint8_t instr_bytes[XED_MAX_INSTRUCTION_BYTES];
    unsigned int num_instr_bytes = 0;

    const xed_error_enum_t encode_error = xed_encode(&encoder_request, instr_bytes, sizeof(instr_bytes),
                                                     &num_instr_bytes);
    if (encode_error != XED_ERROR_NONE) {
      /// only ever returns XED_ERROR_GENERAL_ERROR or XED_ERROR_NONE
    #if XED_EXTRACT_DEVELOPER_MODE
      {
        const char *name = (iform != XED_IFORM_INVALID) ? xed_iform_enum_t2str(iform) : xed_iclass_enum_t2str(iclass);
        std::cerr << "error: xed_encode failed for " << name << std::endl;
        abort();
      }
    #endif
      return std::nullopt;
    }

    /// decode bytes back to instruction

    XED_Decoded_Instruction decoded_inst;
    const xed_error_enum_t decode_error = decode(state.cpu_mode, num_instr_bytes, instr_bytes, decoded_inst);

    switch (decode_error) {
      case XED_ERROR_NONE:
        /// no error. great
        break;
      case XED_ERROR_BAD_LEGACY_PREFIX:
        /// the encoder allowed a legacy prefix on something where it is not actually legal
        return std::nullopt;
      default:
        if (strict) {
          auto &O = std::cerr;

          O << "cannot decode what we just encoded? " << xed_error_enum_t2str(decode_error) << "\n";
          O << " iclass:                  " << xed_iclass_enum_t2str(iclass) << " (" << iclass << ")\n";
          O << " iform:                   " << xed_iform_enum_t2str(iform) << " (" << iform << ")\n";
          O << " src machine-mode:        " << state.cpu_mode.mode_str() << "(" << state.cpu_mode.mode() << ")\n";
          O << " src stack-address-width: " << state.cpu_mode.stack_addr_width_str() << " (" << state.cpu_mode.stack_addr_width() << ")\n";
          O << " effective-operand-width: " << state.effective_operand_width << "\n";
          O << " effective-address-width: " << state.effective_address_width << "\n";

          XED_CPU_Mode enc_mode{encoder_instruction.mode};

          O << " dst machine-mode:        " << enc_mode.mode_str() << " (" << enc_mode.mode_str() << ")\n";
          O << " dst stack-address-width: " << enc_mode.stack_addr_width_str() << " (" << enc_mode.stack_addr_width() << ")\n";
          O << " bytes:                  ";
          for (unsigned i = 0; i < num_instr_bytes; i++) {
            O << " " << std::setw(2) << std::setfill('0') << std::hex << int(instr_bytes[i]) << std::dec;
          }
          O << "\n";
          abort();
        }
        return std::nullopt;
    }

    return decoded_inst;
  }

  static std::optional<XED_Decoded_Instruction> encode_then_decode(const XED_Encoder_State &state, const xed_iclass_enum_t iclass, const xed_iform_enum_t iform, size_t noperands, const xed_encoder_operand_t *operands) {
    xed_encoder_instruction_t encoder_instruction;
    xed_inst(&encoder_instruction, state.cpu_mode.raw(), iclass, state.effective_operand_width, noperands, operands);

    return encode_then_decode(state, iform, encoder_instruction);
  }

  static xed_error_enum_t decode(const XED_CPU_Mode &mode, size_t num_bytes, const uint8_t *code, XED_Decoded_Instruction &decoded_inst) {
    const xed_state_t state = mode.raw();

    xed_decoded_inst_zero_set_mode(decoded_inst.raw(), &state);
    const xed_error_enum_t err = xed_decode(decoded_inst.raw(), code, num_bytes);
    return err;
  }



  static std::set<Pipedream_Instruction> find_all_encodings(std::vector<XED_Instruction> &instructions) {
    std::set<Pipedream_Instruction> output;

    #ifdef _OPENMP
      {
        /// Vector instructions have more operands and bigger register classes and thus take
        /// much longer to process.
        /// They are also grouped together at the end of XEDs instruction list.
        /// This leads to a big imbalance in the workload.
        /// By shuffling the input vector we make sure tasks are more balanced.

        std::random_device rd;
        std::shuffle(instructions.begin(), instructions.end(), std::default_random_engine(rd()));
      }
    #endif

    #ifdef _OPENMP
    #pragma omp parallel
    #endif
    {
      std::set<Pipedream_Instruction> chunk;

      const size_t num_instructions = instructions.size();

    #ifdef _OPENMP
      #pragma omp for schedule(dynamic, 32)
    #endif
      for (size_t i = 0; i < num_instructions; i++) {
        std::cerr << "# " << i << "/" << num_instructions << " - " << instructions[i].iform_str() << "\n";

        _find_all_encodings(instructions[i], chunk);
      }

    #ifdef _OPENMP
      #pragma omp critical
    #endif
      {
        output.merge(chunk);
      }
    }

    return output;
  }

  static std::set<Pipedream_Instruction> find_all_encodings(XED_Instruction inst) {
    std::set<Pipedream_Instruction> output;
    _find_all_encodings(inst, output);
    return output;
  }

  static void _find_all_encodings(XED_Instruction xed_inst, std::set<Pipedream_Instruction> &out) {
    const unsigned memory_widths_all[]  = {8, 16, 32, 64, 128, 256, 512, 1024};
    const unsigned memory_widths_none[] = {0};

    const Span<unsigned> memory_widths = [&]() {
      if (xed_inst.has_memory_operands()) {
        return Span<unsigned>{memory_widths_all};
      } else {
        return Span<unsigned>{memory_widths_none};
      }
    }();

    /// iterate over all possible CPU states
    for (XED_Encoder_State xed_state : XED_Encoder_State_iterator::all()) {
      if (Encode_Decode::xed_extractor_cannot_handle(xed_inst, xed_state)) {
    #if XED_EXTRACT_DEVELOPER_MODE
          std::cerr << "# SKIP: " << xed_inst.iform_str() << std::endl;
    #endif
        continue;
      }

      for (unsigned memory_width : memory_widths) {
        std::unique_ptr<xed_encoder_instruction_t_iterator> inst_iter = xed_encoder_instruction_t_iterator::make(
          xed_inst, xed_state, memory_width
        );

        if (!inst_iter) {
    #if XED_EXTRACT_DEVELOPER_MODE
          std::cerr << "error: can not create a xed_encoder_instruction_t for " << xed_inst.iform_str() << std::endl;
          abort();
    #endif
          continue;
        }

        /// iterate over all possible encodings
        do {
          const xed_encoder_instruction_t &encoder_instruction = inst_iter->get();

          std::optional<XED_Decoded_Instruction> decoded_inst = Encode_Decode::encode_then_decode(
            xed_state, xed_inst.iform(), encoder_instruction
          );

          if (!decoded_inst) {
    #if XED_EXTRACT_DEVELOPER_MODE
            std::cerr << "error: can not encode then decode " << xed_inst.iform_str() << std::endl;
            abort();
    #endif
            continue;
          }

          Pipedream_Instruction pipe_inst;
          pipe_inst.operands       = Convert::pipedream_operands_from_xed(*decoded_inst, xed_state.cpu_mode, xed_state.allow_rex);
          pipe_inst.iclass         = decoded_inst->inst().iclass();
          pipe_inst.isa            = decoded_inst->inst().isa_set();
          pipe_inst.extension      = decoded_inst->inst().extension();
          pipe_inst.category       = decoded_inst->inst().category();
          pipe_inst.tags           = Pipedream::tags(*decoded_inst, pipe_inst.operands);
          pipe_inst.att_mnemonic   = Convert::make_att_mnemonic(*decoded_inst, pipe_inst.operands);
          pipe_inst.intel_mnemonic = Convert::make_intel_mnemonic(*decoded_inst);

          if (xed_produced_invalid_instruction(pipe_inst)) { continue; }
          xed_extractor_fixup(pipe_inst);

          out.insert(pipe_inst);
        } while (inst_iter->advance());
      }
    }
  }
public:
  /// Check if we can handle a given XED instruction.
  /// There are lots of corner cases we cannot handle yet.
  /// Returns true iff we should skip this instruction.
  static bool xed_extractor_cannot_handle(XED_Instruction inst, const XED_Encoder_State &state) {
    /// for now we only do 64bit mode
    if (state.cpu_mode.mode() != XED_MACHINE_MODE_LONG_64) { return true; }

    if (inst.has_rep_prefix()) {
      /// skip instructions with rep prefix
      /// TODO: add repe/repne support
      return true;
    }
    if (inst.has_lock_prefix()) {
      /// skip instructions with lock prefix
      /// TODO: add lock support
      return true;
    }

    /// drop well known weird instructions
    switch (inst.iclass()) {
      /// ignore loop instructions for now
      case XED_ICLASS_LOOP:
      case XED_ICLASS_LOOPE:
      case XED_ICLASS_LOOPNE:
        return true;

      /// gets decoded as NOP since we don't set some exotic CPU flags
      case XED_ICLASS_ENDBR32:
      case XED_ICLASS_ENDBR64:
      case XED_ICLASS_RDSSPD:
      case XED_ICLASS_RDSSPQ:
      case XED_ICLASS_CLDEMOTE:
        return true;

      /// GAS mnemonic for XED_ICLASS_PEXTRW (SSE2) and XED_ICLASS_PEXTRW_SSE4 is the same,
      /// so we cannot distinguish them (they are the same except for some encoding bits)
      case XED_ICLASS_PEXTRW_SSE4:
        return true;

      /// AMD XOP instructions: have a 4-bit immediate operand, but XED says the immediate has 8 bits.
      /// TODO: patch operand in Pipedream_Operand::from_xed
      case XED_ICLASS_VPERMIL2PS:
      case XED_ICLASS_VPERMIL2PD:
        return true;

      /// multi-byte NOP instructions: in the encoding they have a memory BI operand, but XED says they have two register operands.
      /// TODO: patch operand in Pipedream_Operand::from_xed
      case XED_ICLASS_NOP:
      case XED_ICLASS_NOP2:
      case XED_ICLASS_NOP3:
      case XED_ICLASS_NOP4:
      case XED_ICLASS_NOP5:
      case XED_ICLASS_NOP6:
      case XED_ICLASS_NOP7:
      case XED_ICLASS_NOP8:
      case XED_ICLASS_NOP9:
        return true;

      /// gets decoded as PREFETCHWT1 since we don't set some exotic CPU flags
      case XED_ICLASS_PREFETCH_RESERVED:
        return true;

      /// GAS does not know this instruction? (also not on Felix Cloutier's X86 DB??)
      case XED_ICLASS_FDISI8087_NOP:
      case XED_ICLASS_FENI8087_NOP:
      case XED_ICLASS_FSETPM287_NOP:
      case XED_ICLASS_FSTPNCE:
        return true;

      //// TODO: find a way to detect what valid effective operand sizes are
      /// Works only on 32-bit operands.
      /// If you give XED encoder a 64-bit operand it changes the iclass
      case XED_ICLASS_INCSSPD:    // becomes XED_ICLASS_INCSSPQ
      case XED_ICLASS_MOVD:       // becomes XED_ICLASS_MOVQ
      case XED_ICLASS_WRSSD:      // becomes XED_ICLASS_WRSSQ
      case XED_ICLASS_WRUSSD:     // becomes XED_ICLASS_WRUSSQ
      case XED_ICLASS_PEXTRD:     // becomes XED_ICLASS_PEXTRQ
      case XED_ICLASS_PINSRD:     // becomes XED_ICLASS_PINSRQ
      case XED_ICLASS_PCMPESTRI:  // becomes XED_ICLASS_PCMPESTRI64
      case XED_ICLASS_PCMPESTRM:  // becomes XED_ICLASS_PCMPESTRM64
      case XED_ICLASS_PCMPISTRI:  // becomes XED_ICLASS_PCMPISTRI64
        return state.effective_operand_width != EOSZ_32;

      default:
        break;
    }

    for (size_t i = 0, end = inst.num_operands(); i < end; i++) {
      XED_Instruction_Operand op = inst.operand(i);

      switch (op.name()) {
        /// cannot handle far pointer operands yet
        case XED_OPERAND_PTR:
          return true;

        default:
          break;
      }

      switch (op.type()) {
        /// @todo handle more register classes
        case XED_OPERAND_TYPE_NT_LOOKUP_FN2:
        case XED_OPERAND_TYPE_NT_LOOKUP_FN4:
          return true;

        default:
          break;
      }

      if (!op.is_lookup_fn()) { continue; }

      switch (op.get_lookup_fn()) {
        /// cannot handle segment registers yet
        case XED_NONTERMINAL_SEG:
        case XED_NONTERMINAL_SEG_MOV:
          return true;

        /// cannot handle mask registers yet
        case XED_NONTERMINAL_MASK1:
        case XED_NONTERMINAL_MASKNOT0:
        case XED_NONTERMINAL_MASK_B:
        case XED_NONTERMINAL_MASK_N:
        case XED_NONTERMINAL_MASK_N32:
        case XED_NONTERMINAL_MASK_N64:
        case XED_NONTERMINAL_MASK_R:
          return true;

        /// cannot handle control & debug register operands yet
        case XED_NONTERMINAL_CR_R:
        case XED_NONTERMINAL_DR_R:
          return true;

        /// cannot handle bound register operands yet
        case XED_NONTERMINAL_BND_R:
          return true;

        /// cannot handle 'address space sized GPR' operands size.
        /// The A_GPR_R/A_GPR_B register class in XED mixes 16-bit and 64-bit registers?
        /// The XED patterns around instructions that use it are a bit weird too.
        /// For now we ignore it, needs more investigation.
        case XED_NONTERMINAL_A_GPR_R:
        case XED_NONTERMINAL_A_GPR_B:
          return true;

        default:
          break;
      }
    }

    return false;
  }

  /// Sometimes XED messes up, and its output is not valid for GAS.
  /// Fix this
  static void xed_extractor_fixup(Pipedream_Instruction &inst) {
    switch (inst.iclass) {
      case XED_ICLASS_FIADD:
      case XED_ICLASS_FICOM:
      case XED_ICLASS_FICOMP:
      case XED_ICLASS_FIDIV:
      case XED_ICLASS_FIDIVR:
      case XED_ICLASS_FILD:
      case XED_ICLASS_FIMUL:
      case XED_ICLASS_FIST:
      case XED_ICLASS_FISTTP:
      case XED_ICLASS_FISTP:
      case XED_ICLASS_FISUB:
      case XED_ICLASS_FISUBR:
      case XED_ICLASS_FST:
      case XED_ICLASS_FSTP:
        assert(inst.operands.size() == 3);

        if (inst.operands[0].is_reg() && (inst.operands[0].reg() == XED_REG_ST0)) {
          /// XED thinks the fixed register operand is implicit, it is suppressed
          inst.operands[0].visibility = XED_OPVIS_SUPPRESSED;
        }
        if (inst.operands[1].is_reg() && (inst.operands[1].reg() == XED_REG_ST0)) {
          /// XED thinks the fixed register operand is implicit, it is suppressed
          inst.operands[1].visibility = XED_OPVIS_SUPPRESSED;
        }

        assert(!(inst.operands[0].reg() && inst.operands[1].reg()));
      default:
        break;
    }
  }

  /// XED sometimes produces nonsense instructions (well, at least GAS does not accept them).
  /// Filter these out here.
  /// Returns true iff we should drop this instruction.
  static bool xed_produced_invalid_instruction(const Pipedream_Instruction &inst) {
    /// x86 allows encoding many different variants of movsx/movsxd,
    /// unfortunately GNU GAS only accepts very few of them.
    switch (inst.iclass) {
      case XED_ICLASS_MOVSX:
      case XED_ICLASS_MOVZX: {
        assert(inst.operands.size() == 2);

        const Pipedream_Operand_Template &dst_op = inst.operands[0];
        const Pipedream_Operand_Template &src_op = inst.operands[1];

        // drop AH/BH/CD/DH variant
        if (dst_op.value.is_reg() && dst_op.value.reg_class().contains(XED_REG_AH)) { return true; }
        if (src_op.value.is_reg() && src_op.value.reg_class().contains(XED_REG_AH)) { return true; }

        const unsigned dst_bits = Pipedream::type_bits(dst_op.type);
        const unsigned src_bits = Pipedream::type_bits(src_op.type);

        if (dst_bits == src_bits) { return true; }
        if ((src_bits ==  8) && (dst_bits == 16)) { return false; }
        if ((src_bits ==  8) && (dst_bits == 32)) { return false; }
        if ((src_bits ==  8) && (dst_bits == 64)) { return false; }
        if ((src_bits == 16) && (dst_bits == 32)) { return false; }
        if ((src_bits == 16) && (dst_bits == 64)) { return false; }
        return true;
      }
      case XED_ICLASS_MOVSXD: {
        assert(inst.operands.size() == 2);

        const Pipedream_Operand_Template &dst_op = inst.operands[0];
        const Pipedream_Operand_Template &src_op = inst.operands[1];

        // drop AH/BH/CD/DH variant
        if (dst_op.value.is_reg() && dst_op.value.reg_class().contains(XED_REG_AH)) { return true; }
        if (src_op.value.is_reg() && src_op.value.reg_class().contains(XED_REG_AH)) { return true; }

        const unsigned dst_bits = Pipedream::type_bits(dst_op.type);
        const unsigned src_bits = Pipedream::type_bits(src_op.type);

        if (dst_bits == src_bits) { return true; }
        // 16 -> 16 & 32 -> 32 are also encodeable, but GAS does not accept them.
        if ((src_bits == 32) && (dst_bits == 64)) { return false; }
        return true;
      }
      case XED_ICLASS_FADD:
      case XED_ICLASS_FCOMP:
      case XED_ICLASS_FDIV:
      case XED_ICLASS_FDIVR:
      case XED_ICLASS_FDIVRP:
      case XED_ICLASS_FIDIV:
      case XED_ICLASS_FIDIVR:
      case XED_ICLASS_FIMUL:
      case XED_ICLASS_FMUL:
      case XED_ICLASS_FSUB:
      case XED_ICLASS_FSUBR:
        for (const Pipedream_Operand_Template &op : inst.operands) {
          /// TODO: cannot figure out get GAS to accept memory operands on x87 instructions
          if (op.value.is_mem()) { return true; }
        }
        return false;
      case XED_ICLASS_FILD:
      case XED_ICLASS_FIADD:
      case XED_ICLASS_FISUB:
      case XED_ICLASS_FISUBR: {
        assert(inst.operands.size() == 3);

        const Pipedream_Operand_Template &src_op = inst.operands[1];
        const unsigned src_bits = Pipedream::type_bits(src_op.type);

        /// GAS only accepts the 8 and 16 bit versions
        if ((src_bits == 8) && (src_bits == 16)) { return false; }
        return true;
      }
      case XED_ICLASS_FIST:
      case XED_ICLASS_FISTP:
      case XED_ICLASS_FISTTP: {
        assert(inst.operands.size() == 3);

        const Pipedream_Operand_Template &dst_op = inst.operands[0];
        const unsigned dst_bits = Pipedream::type_bits(dst_op.type);

        /// GAS does not accept the 16 bit version
        if (dst_bits == 16) { return true; }
        return false;
      }
      default:
        break;
    }

    /// does the instruction mix GPR8norex and other register operands?
    {
      bool has_gpr_rex_op = false;
      bool has_gpr_8norex_op = false;

      for (const Pipedream_Operand_Template &op : inst.operands) {
        if (op.value.is_reg()) {
          for (xed_reg_enum_t reg : op.value.reg_class()) {
            has_gpr_8norex_op |= XED_Register_Info::is_GPR8norex(reg);

            has_gpr_rex_op |= XED_Register_Info::is_GPR16(reg);
            has_gpr_rex_op |= XED_Register_Info::is_GPR32(reg);
            has_gpr_rex_op |= XED_Register_Info::is_GPR64(reg);
          }
        }
      }

      if (has_gpr_rex_op && has_gpr_8norex_op) {
        /// XED happily allows mixing GPR8norex and other register classes.
        /// GAS does not.
        return true;
      }
    }

    return false;
  }
};

/// find unique names for instructions
struct Instruction_Namer {
  struct Cannot_Disambiguate {
    Pipedream_Instruction a, b;
  };

  using Disambiguated = std::map<std::string, Pipedream_Instruction>;

  /// find unique names for all instructions in @p instructions
  ///
  /// every instruction is always at least identified by
  ///  a) its name
  ///  b) its explicit operands
  ///
  /// we then try to find the shortest possible sequence of the following
  /// that uniquely identifies it among all instructions:
  ///   a) its implicit operands
  ///   b) its ISA set.
  static std::variant<Disambiguated, Cannot_Disambiguate> disambiguate(const std::set<Pipedream_Instruction> &instructions) {
    std::vector<Worklist_Item> worklist;
    worklist.reserve(instructions.size());

    for (const Pipedream_Instruction &p : instructions) {
      worklist.emplace_back(p);
    }

    return _disambiguate(worklist);
  }

  static std::variant<Disambiguated, Cannot_Disambiguate> disambiguate(const std::vector<Pipedream_Instruction> &instructions) {
    std::vector<Worklist_Item> worklist;
    worklist.reserve(instructions.size());

    for (const Pipedream_Instruction &p : instructions) {
      worklist.emplace_back(p);
    }

    return _disambiguate(worklist);
  }
private:
  struct Worklist_Item {
    Worklist_Item(const Worklist_Item &) = default;
    Worklist_Item(Worklist_Item &&) = default;

    Worklist_Item(const Pipedream_Instruction &instruction) : payload{instruction} {
      Span<Pipedream_Operand_Template> explicit_ops, implicit_ops;

      const Pipedream_Operand_Vector &operands = instruction.operands;

      if (!operands.empty()) {
        size_t num_explicit = operands.size();

        while (num_explicit && operands[num_explicit - 1].visibility != XED_OPVIS_EXPLICIT) {
          num_explicit--;
        }

        explicit_ops = Span{num_explicit, operands.data()};
        implicit_ops = Span{operands.size() - num_explicit, operands.data() + num_explicit};
      }

      prefix = xed_iclass_enum_t2str(instruction.iclass);
      for (const auto &op : explicit_ops) {
        prefix += "_";
        prefix += op.short_name();
      }

      for (const auto &op : implicit_ops) {
        remaining.push_back("_" + op.short_name());
      }

      /// needed to disambiguate some vector instructions (VCVTUSI2SD)
       // if (inst.has_attribute(XED_ATTRIBUTE_MXCSR) || inst.has_attribute(XED_ATTRIBUTE_MXCSR_RD)) {
       //   p.remaining.push_back("_MXCSR");
       // }

      remaining.push_back(std::string{"_"} + xed_isa_set_enum_t2str(instruction.isa));
    }

    Pipedream_Instruction payload;
    std::string prefix;
    std::vector<std::string> remaining;

    bool operator<(const Worklist_Item &p) const {
      return payload < p.payload;
    }

    void pop_key() {
      assert(!remaining.empty());

      const std::string &disambiguator = remaining.front();
      prefix += disambiguator;

      remaining.erase(remaining.begin());
    }
  };

  static std::variant<Disambiguated, Cannot_Disambiguate> _disambiguate(std::vector<Worklist_Item> &worklist) {
    const size_t num_instructions = worklist.size();
    (void) num_instructions;

    std::map<std::string, Worklist_Item> disambig;

    while (!worklist.empty()) {
      assert((worklist.size() + disambig.size()) == num_instructions);

      Worklist_Item &new_item = worklist.back();

      auto it = disambig.find(new_item.prefix);

      if (it == disambig.end()) {
        /// new element. done
        disambig.emplace_hint(it, new_item.prefix, new_item);
        worklist.pop_back();
        continue;
      }

      Worklist_Item &old_item = it->second;

      if (new_item.payload == old_item.payload) {
        assert(new_item.prefix    == old_item.prefix);
        assert(new_item.remaining == old_item.remaining);

        worklist.pop_back();
        continue;
      }

      if (new_item.remaining.empty() || old_item.remaining.empty()) {
        return Cannot_Disambiguate{new_item.payload, old_item.payload};
      }

      /// old_item is no longer unambiguously identified by its prefix
      /// remove move map and put back on worklist

      new_item.pop_key();
      old_item.pop_key();

      worklist.emplace_back(std::move(old_item));
      disambig.erase(it);
    }

    assert((worklist.size() + disambig.size()) == num_instructions);

    Disambiguated out;

    for (const auto &pair : disambig) {
      out[pair.first] = pair.second.payload;
    }

    return out;
  }
};

template<class... Ts> struct overload : Ts... { using Ts::operator()...; };
template<class... Ts> overload(Ts...) -> overload<Ts...>;

/// Helper for handling std::ifstream/std::ofstream objects
template<typename Stream>
struct Stream_Handle {
  explicit Stream_Handle(Stream &file) : _owns_file{false}, _file{&file} {}

  ~Stream_Handle() {
    reset(nullptr);
  }

  Stream_Handle& operator=(const Stream_Handle &) = delete;

  operator Stream&() {
    assert(_file);
    return *_file;
  }

  void reset(Stream *new_file) {
    if (_owns_file) { delete _file; }

    _file      = new_file;
    _owns_file = true;
  }
private:
  bool _owns_file;
  Stream *_file;
};


struct Main {
  static int main(int argc, char **argv) {
    /// help
    for (int i = 1; i < argc; i++) {
      const std::string arg = argv[i];
      if ((arg == "-h") || (arg == "--help")) {
        print_usage(0, std::cout, argc, argv);
      }
    }

    /// get command
    if (argc < 2) {
      print_usage(1, std::cerr, argc, argv);
    }

    const std::string command = argv[1];

    /// remove command from argv
    // shift everything right of command (including NULL terminator) one left
    for (int i = 1; i <= argc; i++) {
      argv[i] = argv[i + 1];
    }
    argc--;

    if (command == "print-instr-db") {
      const struct option longopts[] = {
        { .name = "output", .has_arg = true, .flag = NULL, .val = 'o' },
        { .name = NULL, .has_arg = false, .flag = NULL, .val = 0 }
      };

      Stream_Handle<std::ostream> output{std::cout};

      while (true) {
        int longopt_index;
        const int c = getopt_long(argc, argv, "ho:", longopts, &longopt_index);

        if (c == -1) { break; }

        switch (c) {
          case 'o':
            output.reset(open_output_file(optarg));
            break;
        }
      }

      assert(argc >= optind);
      const unsigned num_positional_args = argc - optind;

      if (num_positional_args > 0) {
        std::cerr << "error: unexpected argument '" << argv[optind] << "'\n";
        print_usage(1, std::cerr, argc, argv);
      }

      return print_pipedream_instruction_db(output);
    } else if (command == "disasm") {
      const struct option longopts[] = {
        { .name = "output", .has_arg = true, .flag = NULL, .val = 'o' },
        { .name = NULL, .has_arg = false, .flag = NULL, .val = 0 }
      };

      Stream_Handle<std::istream> input{std::cin};
      Stream_Handle<std::ostream> output{std::cout};

      while (true) {
        int longopt_index;
        const int c = getopt_long(argc, argv, "ho:", longopts, &longopt_index);

        if (c == -1) { break; }

        switch (c) {
          case 'o':
            output.reset(open_output_file(optarg));
            break;
        }
      }

      assert(argc >= optind);
      const unsigned num_positional_args = argc - optind;

      switch (num_positional_args) {
        case 0:
          break;
        case 1:
          input.reset(open_input_file(argv[optind]));
          break;
        default:
          std::cerr << "error: unexpected argument '" << argv[optind + 1] << "'\n";
          print_usage(1, std::cerr, argc, argv);
      }

      return disassemble_instructions(input, output);
    } else if (command == "playground") {
      return playground();
    } else {
      std::cerr << "error: invalid command '" << command << "'\n";
      print_usage(1, std::cerr, argc, argv);
    }
  }

  static void print_usage[[noreturn]](int retcode, std::ostream &O, int argc, char **argv) {
    const char *const cmd = argc > 0 ? argv[0] : "extract-xed-instruction-database";

    O << "usage: " << cmd << " print-instr-db [-o OUT]\n";
    O << "       " << cmd << " disasm [-o OUT] [INPUT]\n";
    O << "       " << cmd << " playground\n";
    O << "\n";
    O << "COMMANDS:\n";
    O << "  print-instr-db  Print the pipedream X86 instr DB.\n";
    O << "                  Basically a big file with python\n";
    O << "\n";
    O << "  disasm          Read instruction bytes from stdin and write names of\n";
    O << "                  instructions to stdout.\n";
    O << "                  INPUT specifies a path to read code from.";
    O << "                  If not specified input is read from stdin.";
    O << "\n";
    O << "  playground      Run some basic disassembly experiments.\n";
    O << "                  Will probably crash.\n";
    O << "\n";
    O << "OPTIONS:\n";
    O << "  Note: Not all commands accept all options.\n";
    O << "\n";
    O << "  -o OUT\n";
    O << "  --output=OUT\n";
    O << "                  File to write to\n";
    exit(retcode);
  }

  /// Print Python code that is the Pippedream X86 instruction DB
  static int print_pipedream_instruction_db(std::ostream &O) {
    xed_tables_init();

    std::vector<XED_Instruction> xed_insts;
    for (XED_Instruction inst : xed_inst_t_iterator::all()) {
      xed_insts.push_back(inst);
    }

    std::cerr << "## find all instructions & encodings\n";

    std::set<Pipedream_Instruction> instructions = Encode_Decode::find_all_encodings(xed_insts);

    std::cerr << "## compute unique names for all instruction encodings\n";

    auto names = Instruction_Namer::disambiguate(instructions);

    std::visit(overload{
      [](const Instruction_Namer::Cannot_Disambiguate &err) {
        std::cerr << "ERROR: CANNOT DISAMBIGUATE\n";
        std::cerr << err.a;
        std::cerr << err.b;
        std::exit(1);
      },
      [&O](const Instruction_Namer::Disambiguated &names) {
        std::cerr << "## emit instruction database\n";

        emit_pipedream_instruction_file_header(O);

        for (const auto &[name, inst] : names) {
          emit_pipedream_instruction_descriptor(O, name, inst);
        }

        emit_pipedream_instruction_file_footer(O);

        std::cerr << "## done\n";
      }
    }, names);

    return 0;
  }

  static int disassemble_instructions(std::istream &I, std::ostream &O) {
    xed_tables_init();

    std::vector<unsigned char> code;

    /// this is a really slow way to read a file, but let's assume we aren't passed GBs of code.
    while (true) {
      char byte;
      I.read(&byte, 1);

      if (I.eof()) { break; }

      code.push_back(byte);
    }

    /// disassemble and print instruction names

    size_t pos = 0;

    // FIXME: for now we onlt do 64-bit mode
    const XED_CPU_Mode            cpu_mode  = XED_CPU_Mode::make(XED_MACHINE_MODE_LONG_64, XED_ADDRESS_WIDTH_64b);
    const bool                    allow_rex = true;

    std::vector<Pipedream_Instruction> instructions;

    while (pos < code.size()) {
      const size_t   num_bytes = code.size() - pos;
      const uint8_t *bytes     = &code[pos];

      XED_Decoded_Instruction decoded_inst;
      const xed_error_enum_t decode_error = Encode_Decode::decode(cpu_mode, num_bytes, bytes, decoded_inst);

      switch (decode_error) {
        case XED_ERROR_NONE:
          pos += decoded_inst.get_length();
          break;
        case XED_ERROR_BUFFER_TOO_SHORT:
          std::cerr << "error: Truncated instruction\n";
          std::cerr << "       There were not enough bytes in the input.\n";
          exit(1);
        case XED_ERROR_GENERAL_ERROR:
          std::cerr << "error: XED could not decode the instruction.\n";
          exit(1);
          break;
        case XED_ERROR_INVALID_FOR_CHIP:
          std::cerr << "error: The instruction is not valid for the specified chip.\n";
          exit(1);
        case XED_ERROR_BAD_REGISTER:
          std::cerr << "error: XED could not decode the given instruction because an invalid register encoding was used.\n";
          exit(1);
        case XED_ERROR_BAD_LOCK_PREFIX:
          std::cerr << "error: A lock prefix was found where none is allowed.\n";
          exit(1);
        case XED_ERROR_BAD_REP_PREFIX:
          std::cerr << "error: An F2 or F3 prefix was found where none is allowed.\n";
          exit(1);
        case XED_ERROR_BAD_LEGACY_PREFIX:
          std::cerr << "error: A 66, F2 or F3 prefix was found where none is allowed.\n";
          exit(1);
        case XED_ERROR_BAD_REX_PREFIX:
          std::cerr << "error: A REX prefix was found where none is allowed.\n";
          exit(1);
        case XED_ERROR_BAD_EVEX_UBIT:
          std::cerr << "error: An illegal value for the EVEX.U bit was present in the instruction.\n";
          exit(1);
        case XED_ERROR_BAD_MAP:
          std::cerr << "error: An illegal value for the MAP field was detected in the instruction.\n";
          exit(1);
        case XED_ERROR_BAD_EVEX_V_PRIME:
          std::cerr << "error: EVEX.V'=0 was detected in a non-64b mode instruction.\n";
          exit(1);
        case XED_ERROR_BAD_EVEX_Z_NO_MASKING:
          std::cerr << "error: EVEX.Z!=0 when EVEX.aaa==0.\n";
          exit(1);
        case XED_ERROR_NO_OUTPUT_POINTER:
          std::cerr << "error: The output pointer for xed_agen was zero.\n";
          exit(1);
        case XED_ERROR_NO_AGEN_CALL_BACK_REGISTERED:
          std::cerr << "error: One or both of the callbacks for xed_agen were missing.\n";
          exit(1);
        case XED_ERROR_BAD_MEMOP_INDEX:
          std::cerr << "error: Memop indices must be 0 or 1.\n";
          exit(1);
        case XED_ERROR_CALLBACK_PROBLEM:
          std::cerr << "error: The register or segment callback for xed_agen experienced a problem.\n";
          exit(1);
        case XED_ERROR_GATHER_REGS:
          std::cerr << "error: The index, dest and mask regs for AVX2 gathers must be different.\n";
          exit(1);
        case XED_ERROR_INSTR_TOO_LONG:
          std::cerr << "error: Full decode of instruction would exeed 15B.\n";
          exit(1);
        case XED_ERROR_INVALID_MODE:
          std::cerr << "error: The instruction was not valid for the specified mode.\n";
          exit(1);
        case XED_ERROR_BAD_EVEX_LL:
          std::cerr << "error: EVEX.LL must not ==3 unless using embedded rounding.\n";
          exit(1);

        case XED_ERROR_LAST:
          __builtin_unreachable();
      }

      const Effective_Operand_Width EOSZ = decoded_inst.effective_operand_width();
      const Effective_Address_Width EASZ = decoded_inst.effective_address_width();

      const XED_Encoder_State state = {cpu_mode, EOSZ, EASZ, allow_rex};

      if (Encode_Decode::xed_extractor_cannot_handle(decoded_inst.inst(), state)) {
        continue;
      }

      Pipedream_Instruction enc;
      enc.iclass         = decoded_inst.inst().iclass();
      enc.operands       = Convert::pipedream_operands_from_xed(decoded_inst, cpu_mode, allow_rex);

      Encode_Decode::xed_extractor_fixup(enc);

      instructions.push_back(enc);
    }

    /// FIXME: This does not reproduce all the funky corner cases the Instruction_Namer handles.
    ///        But the Instruction_Namer needs to have ALL x86 instructions to work properly.
    ///        I don't see any easy way to get that here.
    ///        Let's just hope no corner cases crop up for the moment.
    /// TODO: we'll probably need to handle leading implicit args too.
    for (const Pipedream_Instruction &inst : instructions) {
      O << xed_iclass_enum_t2str(inst.iclass);

      for (const auto &op : inst.operands) {
        if (op.visibility != XED_OPVIS_EXPLICIT) {
          break;
        }

        O << "_" << op.short_name();
      }

      O << "\n";
    }

    return 0;
  }

  static int playground() {
    xed_tables_init();

    const auto try_encode = [](xed_iclass_enum_t iclass, const Effective_Operand_Width EOSZ, const Effective_Address_Width EASZ, bool allow_rex, auto ...ops) {
      const XED_CPU_Mode cpu_mode = XED_CPU_Mode::make(XED_MACHINE_MODE_LONG_64, XED_ADDRESS_WIDTH_64b);

      const size_t noperands = sizeof...(ops);
      const xed_encoder_operand_t operands[noperands] = { ops... };

      const XED_Encoder_State state = {cpu_mode, EOSZ, EASZ, allow_rex};

      auto inst = Encode_Decode::encode_then_decode(state, iclass, XED_IFORM_INVALID, noperands, operands);

      auto &O = std::cerr;

      if (inst) {
        O << "playground" << ": encoded: " << xed_iclass_enum_t2str(iclass) << "\n";
        O << inst->num_operands() << "\n";
        Debug::print_inst(O, *inst);
      } else {
        O << "playground" << ": could not encode: " << xed_iclass_enum_t2str(iclass) << "\n";
        // Debug::print_inst(O, inst);
        Debug::print_encoder_state(O, state);
        O << "  ENCODER OPERANDS:\n";
        for (size_t i = 0; i < noperands; i++) {
          O << "    ";
          Debug::print_operand(O, operands[i]);
          O << "\n";
        }
        O << "\n";
        abort();
      }
    };

    try_encode(XED_ICLASS_ADC, EOSZ_64, EASZ_64, true, xed_mem_b(XED_REG_RAX, 64), xed_reg(XED_REG_RAX));
    try_encode(XED_ICLASS_ADC, EOSZ_64, EASZ_64, true, xed_mem_b(XED_REG_RAX, 64), xed_imm0(1, 8));
    try_encode(XED_ICLASS_ADC, EOSZ_64, EASZ_64, true, xed_mem_b(XED_REG_RAX, 64), xed_imm0(1, 32));

    try_encode(XED_ICLASS_FADD, EOSZ_64, EASZ_64, true, xed_reg(XED_REG_ST0), xed_reg(XED_REG_ST0));

    try_encode(XED_ICLASS_SHLD, EOSZ_64, EASZ_64, true, xed_reg(XED_REG_RAX), xed_reg(XED_REG_RBX), xed_reg(XED_REG_CL));

    try_encode(XED_ICLASS_INC, EOSZ_16, EASZ_64, true, xed_reg(XED_REG_AL));
    try_encode(XED_ICLASS_INC, EOSZ_16, EASZ_64, true, xed_reg(XED_REG_AX));
    try_encode(XED_ICLASS_INC, EOSZ_64, EASZ_64, true, xed_reg(XED_REG_RAX));

    try_encode(XED_ICLASS_MOVD, EOSZ_32, EASZ_32, true, xed_reg(XED_REG_XMM0), xed_mem_b(XED_REG_EAX, 32));
    /// does not make sense (movd has a 32-bit operand), so XED turns it into a MOVQ
    // try_encode(XED_ICLASS_MOVD, EOSZ_64, EASZ_32, true, xed_reg(XED_REG_XMM0), xed_mem_b(XED_REG_EAX, 32));

    try_encode(XED_ICLASS_XOR, EOSZ_16, EASZ_64, true, xed_reg(XED_REG_AL), xed_imm0(1, 8));

    try_encode(XED_ICLASS_XOR, EOSZ_16, EASZ_64, true, xed_reg(XED_REG_AL), xed_reg(XED_REG_AL));
    try_encode(XED_ICLASS_XOR, EOSZ_16, EASZ_64, true, xed_reg(XED_REG_AL), xed_reg(XED_REG_R15B));

    return 0;
  }

  static void emit_pipedream_instruction_file_header(std::ostream &O) {
    O << R"(
## This file is derived from the instruction data files of Intel XED
## The original files are available at https://intelxed.github.io
##
##  Original copyright (c) 2018 Intel Corporation
##  Modified copyright (c) 2019 Fabian Gruber
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##   http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

from pipedream.asm.x86 import registers, flags
from pipedream.asm.ir import Use_Def, Operand_Visibility

import enum
from typing import *

## CPU mode
LONG_64 = 'LONG_64'

## read/write
R   = Use_Def.USE
W   = Use_Def.DEF
RW  = Use_Def.USE_DEF
CW  = Use_Def.DEF
RCW = Use_Def.USE_DEF # fixme: add read + conditional write
CRW = Use_Def.USE_DEF # fixme: add conditional read + write

## operand visibility
EXPLICIT   = Operand_Visibility.EXPLICIT
IMPLICIT   = Operand_Visibility.IMPLICIT
SUPPRESSED = Operand_Visibility.SUPPRESSED

## operand type
i1   = 'i1'
i8   = 'i8'
i16  = 'i16'
i32  = 'i32'
i64  = 'i64'

u8   = 'u8'
u16  = 'u16'
u32  = 'u32'
u64  = 'u64'
u128 = 'u128'
u256 = 'u256'

f16  = 'f16'
f32  = 'f32'
f64  = 'f64'
f80  = 'f80'

OTHER = 'OTHER'

)";

  O << "## ISA\n";
  O << "class ISA(enum.Enum):\n";
  for (xed_isa_set_enum_t isa = xed_isa_set_enum_t(XED_ISA_SET_INVALID + 1); isa < XED_ISA_SET_LAST; isa = xed_isa_set_enum_t(int(isa) + 1)) {
    std::string txt  = xed_isa_set_enum_t2str(isa);
    std::string name = txt;

    if (std::isdigit(txt.front())) { name = '_' + name; }

    O << "  " << std::setw(20) << std::left << name << " = '" << txt << "'\n";
  }
  O << "\n";
  O << "## ISA extension\n";
  O << "class ISA_Extension(enum.Enum):\n";
  for (xed_extension_enum_t ext = xed_extension_enum_t(XED_EXTENSION_INVALID + 1); ext < XED_EXTENSION_LAST; ext = xed_extension_enum_t(int(ext) + 1)) {
    std::string txt  = xed_extension_enum_t2str(ext);
    std::string name = txt;

    if (std::isdigit(txt.front())) { name = '_' + name; }

    O << "  " << std::setw(20) << std::left << name << " = '" << txt << "'\n";
  }
  O << "\n";

  O << R"(
EXT = ISA_Extension

## register classes
class RC:
  GPR8      = registers.GPR8
  GPR8NOREX = registers.GPR8NOREX
  GPR16     = registers.GPR16
  GPR32     = registers.GPR32
  GPR64     = registers.GPR64
  VR64      = registers.VR64
  VR128     = registers.VR128
  VRX128    = registers.VRX128
  VR256     = registers.VR256
  VRX256    = registers.VRX256
  VRX512    = registers.VRX512
  FPST      = registers.FPST
)";
  for (const xed_reg_enum_t *regs : Pipedream::singleton_register_classes()) {
    Register_Class reg_class{1, regs};

    switch (*regs) {
      case XED_REG_STACKPUSH:
      case XED_REG_STACKPOP:
      case XED_REG_X87POP:
      case XED_REG_X87POP2:
      case XED_REG_X87PUSH:
        break;
      default:
        O << "  " << Pipedream::register_class_name(reg_class) << " = registers.RC_" << Pipedream::register_class_name(reg_class) << "\n";
        break;
    }
  }
  O << "\n";
  O << "## registers\n";
  for (const xed_reg_enum_t *regs : Pipedream::singleton_register_classes()) {
    Register_Class reg_class{1, regs};

    switch (*regs) {
      case XED_REG_STACKPUSH:
      case XED_REG_STACKPOP:
      case XED_REG_X87POP:
      case XED_REG_X87POP2:
      case XED_REG_X87PUSH:
        break;
      default:
        O << Pipedream::register_class_name(reg_class) << " = registers." << Pipedream::register_class_name(reg_class)
          << "\n";
        break;
    }
  }

  O << "\n";
  O << "## CPU flags\n";
  O << "F = flags.X86_Flags\n";
  O << "\n";

  O << R"(

def make_instruction_database(*, make_instruction, reg_op, mem_op, addr_op, imm_op, brdisp_op, flags_op):
)";
  }

  static void emit_pipedream_instruction_file_footer(std::ostream &O) {
    O << R"(

if __name__ == '__main__':
  def make_instruction(**kwargs):
    print(kwargs)

  make_op = lambda **kwargs: kwargs

  make_instruction_database(
    make_instruction = make_instruction,
    reg_op           = make_op,
    mem_op           = make_op,
    addr_op          = make_op,
    imm_op           = make_op,
    brdisp_op        = make_op,
    flags_op         = make_op,
  )
)";
  }

  static void emit_pipedream_instruction_operands(std::ostream &O, const Pipedream_Operand_Vector &operands) {
    unsigned num_uses = 0, num_defs = 0, num_use_defs = 0;

    for (const auto &op : operands) {
      switch (op.reg().value_or(XED_REG_INVALID)) {
        case XED_REG_FLAGS:
        case XED_REG_EFLAGS:
        case XED_REG_RFLAGS:
        case XED_REG_IP:
        case XED_REG_EIP:
        case XED_REG_RIP:
          break;
        default:
          switch (op.action) {
          case XED_OPERAND_ACTION_R:
          case XED_OPERAND_ACTION_CR:
            num_uses++;
            break;
          case XED_OPERAND_ACTION_W:
          case XED_OPERAND_ACTION_CW:
            num_defs++;
            break;
          case XED_OPERAND_ACTION_RW:
          case XED_OPERAND_ACTION_RCW:
          case XED_OPERAND_ACTION_CRW:
            num_use_defs++;
            break;
          case XED_OPERAND_ACTION_INVALID:
          case XED_OPERAND_ACTION_LAST:
            abort();
        }
      }
    }

    unsigned curr_use = 0, curr_def = 0, curr_use_def = 0;

    for (const Pipedream_Operand_Template &op : operands) {
//      const Pipedream::Type type = Pipedream::type(op.element_xtype, op.element_type, op.element_bits);

//      assert((type == Pipedream::OTHER) || op.element_bits == Pipedream::type_bits(type));

      /// operand name

      std::string name;
      if (op.is_brdisp()) {
        name = "target";
      } else {
        switch (op.reg().value_or(XED_REG_INVALID)) {
          case XED_REG_FLAGS:
          case XED_REG_EFLAGS:
          case XED_REG_RFLAGS:
            name = "flags";
            break;
          case XED_REG_IP:
          case XED_REG_EIP:
          case XED_REG_RIP:
            name = "ip";
            break;
          case XED_REG_SP:
          case XED_REG_ESP:
          case XED_REG_RSP:
            name = "sp";
            break;
          default:
            switch (op.action) {
              case XED_OPERAND_ACTION_R:
              case XED_OPERAND_ACTION_CR:
                curr_use++;
                name = "src";
                if (num_uses > 1) { name += std::to_string(curr_use); }
                break;
              case XED_OPERAND_ACTION_W:
              case XED_OPERAND_ACTION_CW:
                curr_def++;
                name = "dst";
                if (num_defs > 1) { name += std::to_string(curr_def); }
                break;
              case XED_OPERAND_ACTION_RW:
              case XED_OPERAND_ACTION_RCW:
              case XED_OPERAND_ACTION_CRW:
                curr_use_def++;
                name = "src-dst";
                if (num_use_defs > 1) { name += std::to_string(curr_use_def); }
                break;
              case XED_OPERAND_ACTION_INVALID:
              case XED_OPERAND_ACTION_LAST:
                abort();
            }
        }
      }

      O << "      ";
      switch (op.value.kind()) {
        case Machine_Value::REG:
          O << "reg_op(";
          break;
        case Machine_Value::ADDR:
          O << "addr_op(";
          break;
        case Machine_Value::MEM:
          O << "mem_op(";
          break;
        case Machine_Value::BRDISP:
          O << "brdisp_op(";
          break;
        case Machine_Value::IMM:
          O << "imm_op(";
          break;
        case Machine_Value::FLAGS:
          O << "flags_op(";
          break;
      }

      O << "name='" << name << "', ";

      switch (op.value.kind()) {
        case Machine_Value::REG:
          if (auto reg = op.value.reg()) {
            O << "reg=" << Pipedream::register_name(*reg) << ", ";
          }
          O << "reg_class=RC." << Pipedream::register_class_name(op.value.reg_class()) << ", ";
          O << "action=" << xed_operand_action_enum_t2str(op.action) << ", ";
          break;
        case Machine_Value::MEM:
          O << "addr_bits=" << op.value.address_bits() << ", ";
          O << "mem_bits=" << op.value.memory_bits() << ", ";
          if (auto reg = op.value.base_reg()) {
            O << "base=" << Pipedream::register_name(*reg) << ", ";
          }
          O << "action=" << xed_operand_action_enum_t2str(op.action) << ", ";
          break;
        case Machine_Value::ADDR:
          O << "addr_bits=" << op.value.address_bits() << ", ";
          break;
        case Machine_Value::BRDISP:
          O << "disp_bits=" << op.value.brdisp_bits() << ", ";
          break;
        case Machine_Value::IMM:
          O << "imm_bits=" << op.value.imm_bits() << ", ";
          break;
        case Machine_Value::FLAGS:
          O << "reg=" << Pipedream::register_name(op.value.flag_reg()) << ", ";
          O << "read="; Pipedream::print_flag_set(op.value.flags_read(), O, "F."); O << ", ";
          O << "write="; Pipedream::print_flag_set(op.value.flags_written(), O, "F."); O << ", ";
          break;
      }

      if (op.value.kind() != Machine_Value::FLAGS) {
        O << "type=" << Pipedream::type_name(op.type) << ", ";
        O << "elems=" << op.num_elements << ", ";
      }
      O << "visibility=" << xed_operand_visibility_enum_t2str(op.visibility); // << ", ";

      O << "),\n";
    }
  }

  static void emit_pipedream_instruction_descriptor(std::ostream &O, const std::string &name, const Pipedream_Instruction &inst) {
    std::string isa_set = xed_isa_set_enum_t2str(inst.isa);
    assert(!isa_set.empty());
    if (std::isdigit(isa_set[0])) {
      isa_set = '_' + isa_set;
    }

    std::string isa_extension = xed_extension_enum_t2str(inst.extension);
    assert(!isa_extension.empty());
    if (std::isdigit(isa_extension[0])) {
      isa_extension = '_' + isa_extension;
    }

    O << "  make_instruction(\n";
    O << "    name                    = '" << name << "',\n";
    O << "    intel_mnemonic          = '" << inst.intel_mnemonic << "',\n";
    O << "    att_mnemonic            = '" << inst.att_mnemonic << "',\n";
//    O << "    cpu_mode                = " << inst.cpu_mode.mode_str() << ",\n";
//    O << "    stack_address_width     = " << inst.cpu_mode.stack_addr_width_bits() << ",\n";
//    O << "    effective_operand_width = " << inst.effective_operand_width << ",\n";
//    O << "    effective_address_width = " << inst.effective_address_width << ",\n";
//    O << "    is_nop                  = " << (inst.is_nop ? "True" : "False") << ",\n";
    O << "    isa_set                 = ISA." << isa_set << ",\n";
    O << "    isa_extension           = EXT." << isa_extension << ",\n";
    O << "    tags                    = [";
    {
      bool first = true;
      for (const std::string &tag : inst.tags) {
        if (!first) { O << ", "; }

        first = false;

        O << "'" << tag << "'";
      }
    }
    O << "],\n";
    O << "    operands                = [\n";
    emit_pipedream_instruction_operands(O, inst.operands);
    O << "    ],\n";
    O << "  )\n";
  }

  /// open a file for writing
  static std::ostream *open_output_file(const std::string &path) {
    std::ofstream *file = new std::ofstream();

    file->open(path);
    if (!file->is_open() || file->bad()) {
      std::cerr << "error: could not open '" << path << "' for writing\n";
      exit(1);
    }

    return file;
  }

  /// open a file for writing
  static std::istream *open_input_file(const std::string &path) {
    std::ifstream *file = new std::ifstream();

    file->open(path);
    if (!file->is_open() || file->bad()) {
      std::cerr << "error: could not open '" << path << "' for reading\n";
      exit(1);
    }

    return file;
  }
};

int main(int argc, char **argv) {
  return Main::main(argc, argv);
}
