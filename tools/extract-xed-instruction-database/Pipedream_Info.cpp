//
// Created by fader on 14.03.19.
//

#include "Pipedream_Info.hpp"

static const xed_reg_enum_t SINGLETON_REGISTER_CLASSES[][1] = {
  {XED_REG_FLAGS},
  {XED_REG_EFLAGS},
  {XED_REG_RFLAGS},

  {XED_REG_IP},
  {XED_REG_EIP},
  {XED_REG_RIP},

  {XED_REG_SP},
  {XED_REG_ESP},
  {XED_REG_RSP},

  {XED_REG_CS},
  {XED_REG_DS},
  {XED_REG_ES},
  {XED_REG_FS},
  {XED_REG_GS},
  {XED_REG_SS},
  {XED_REG_FSBASE},
  {XED_REG_GSBASE},
  {XED_REG_TR},
  {XED_REG_LDTR},
  {XED_REG_GDTR},
  {XED_REG_SSP},
  {XED_REG_TSCAUX},

  {XED_REG_CR0},
  {XED_REG_CR1},
  {XED_REG_CR2},
  {XED_REG_CR3},
  {XED_REG_CR4},
  {XED_REG_CR5},
  {XED_REG_CR6},
  {XED_REG_CR7},
  {XED_REG_CR8},
  {XED_REG_CR9},
  {XED_REG_CR10},
  {XED_REG_CR11},
  {XED_REG_CR12},
  {XED_REG_CR13},
  {XED_REG_CR14},
  {XED_REG_CR15},

  {XED_REG_AL},
  {XED_REG_CL},

  {XED_REG_AH},

  {XED_REG_AX},
  {XED_REG_BX},
  {XED_REG_CX},
  {XED_REG_DX},
  {XED_REG_DI},
  {XED_REG_SI},
  {XED_REG_BP},

  {XED_REG_EAX},
  {XED_REG_EBX},
  {XED_REG_ECX},
  {XED_REG_EDX},
  {XED_REG_EDI},
  {XED_REG_ESI},
  {XED_REG_EBP},

  {XED_REG_RAX},
  {XED_REG_RBX},
  {XED_REG_RCX},
  {XED_REG_RDX},
  {XED_REG_RDI},
  {XED_REG_RSI},
  {XED_REG_RBP},
  {XED_REG_R8},
  {XED_REG_R9},
  {XED_REG_R10},
  {XED_REG_R11},
  {XED_REG_R12},
  {XED_REG_R13},
  {XED_REG_R14},
  {XED_REG_R15},

  {XED_REG_XMM0},

  {XED_REG_ST0},
  {XED_REG_ST1},
  {XED_REG_ST2},
  {XED_REG_ST3},
  {XED_REG_ST4},
  {XED_REG_ST5},
  {XED_REG_ST6},
  {XED_REG_ST7},

  {XED_REG_X87STATUS},
  {XED_REG_X87CONTROL},
  {XED_REG_X87TAG},

  {XED_REG_MXCSR},
  {XED_REG_TSC},
  {XED_REG_MSRS},
  {XED_REG_XCR0},

  {XED_REG_STACKPUSH},
  {XED_REG_STACKPOP},
  {XED_REG_X87POP},
  {XED_REG_X87POP2},
  {XED_REG_X87PUSH},
};

namespace {

struct Register_Class_Names {
  Register_Class_Names() : _names{
    // useable only with a rex prefix
    {
      {XED_REG_AL, XED_REG_CL, XED_REG_DL, XED_REG_BL, XED_REG_SPL, XED_REG_BPL,
       XED_REG_SIL, XED_REG_DIL, XED_REG_R8B, XED_REG_R9B, XED_REG_R10B,
       XED_REG_R11B, XED_REG_R12B, XED_REG_R13B, XED_REG_R14B, XED_REG_R15B}, "GPR8"
    },
    // useable only without a rex prefix
    {
      {XED_REG_AL, XED_REG_CL, XED_REG_DL, XED_REG_BL,
       XED_REG_AH, XED_REG_CH, XED_REG_DH, XED_REG_BH}, "GPR8NOREX"
    },
    {
      {XED_REG_AX, XED_REG_CX, XED_REG_DX, XED_REG_BX, XED_REG_SP, XED_REG_BP, XED_REG_SI,
       XED_REG_DI, XED_REG_R8W, XED_REG_R9W, XED_REG_R10W, XED_REG_R11W, XED_REG_R12W,
       XED_REG_R13W, XED_REG_R14W, XED_REG_R15W}, "GPR16"
    },
    {
      {XED_REG_EAX, XED_REG_ECX, XED_REG_EDX, XED_REG_EBX, XED_REG_ESP, XED_REG_EBP,
       XED_REG_ESI, XED_REG_EDI, XED_REG_R8D, XED_REG_R9D, XED_REG_R10D, XED_REG_R11D,
       XED_REG_R12D, XED_REG_R13D, XED_REG_R14D, XED_REG_R15D}, "GPR32"
    },
    {
      {XED_REG_RAX, XED_REG_RCX, XED_REG_RDX, XED_REG_RBX, XED_REG_RSP, XED_REG_RBP,
       XED_REG_RSI, XED_REG_RDI, XED_REG_R8, XED_REG_R9, XED_REG_R10, XED_REG_R11,
       XED_REG_R12, XED_REG_R13, XED_REG_R14, XED_REG_R15}, "GPR64"
    },
    {
      {XED_REG_MMX0, XED_REG_MMX1, XED_REG_MMX2, XED_REG_MMX3,
       XED_REG_MMX4, XED_REG_MMX5, XED_REG_MMX6, XED_REG_MMX7}, "VR64"
    },
    {
      {XED_REG_XMM0, XED_REG_XMM1, XED_REG_XMM2, XED_REG_XMM3, XED_REG_XMM4, XED_REG_XMM5,
       XED_REG_XMM6, XED_REG_XMM7, XED_REG_XMM8, XED_REG_XMM9, XED_REG_XMM10, XED_REG_XMM11,
       XED_REG_XMM12, XED_REG_XMM13, XED_REG_XMM14, XED_REG_XMM15}, "VR128"
    },
    {
      {XED_REG_XMM0, XED_REG_XMM1, XED_REG_XMM2, XED_REG_XMM3, XED_REG_XMM4, XED_REG_XMM5,
       XED_REG_XMM6, XED_REG_XMM7, XED_REG_XMM8, XED_REG_XMM9, XED_REG_XMM10, XED_REG_XMM11,
       XED_REG_XMM12, XED_REG_XMM13, XED_REG_XMM14, XED_REG_XMM15, XED_REG_XMM16, XED_REG_XMM17,
       XED_REG_XMM18, XED_REG_XMM19, XED_REG_XMM20, XED_REG_XMM21, XED_REG_XMM22, XED_REG_XMM23,
       XED_REG_XMM24, XED_REG_XMM25, XED_REG_XMM26, XED_REG_XMM27, XED_REG_XMM28, XED_REG_XMM29,
       XED_REG_XMM30, XED_REG_XMM31}, "VRX128"
    },
    {
      {XED_REG_YMM0, XED_REG_YMM1, XED_REG_YMM2, XED_REG_YMM3, XED_REG_YMM4, XED_REG_YMM5,
       XED_REG_YMM6, XED_REG_YMM7, XED_REG_YMM8, XED_REG_YMM9, XED_REG_YMM10, XED_REG_YMM11,
       XED_REG_YMM12, XED_REG_YMM13, XED_REG_YMM14, XED_REG_YMM15}, "VR256"
    },
    {
      {XED_REG_YMM0, XED_REG_YMM1, XED_REG_YMM2, XED_REG_YMM3, XED_REG_YMM4, XED_REG_YMM5,
       XED_REG_YMM6, XED_REG_YMM7, XED_REG_YMM8, XED_REG_YMM9, XED_REG_YMM10, XED_REG_YMM11,
       XED_REG_YMM12, XED_REG_YMM13, XED_REG_YMM14, XED_REG_YMM15, XED_REG_YMM16, XED_REG_YMM17,
       XED_REG_YMM18, XED_REG_YMM19, XED_REG_YMM20, XED_REG_YMM21, XED_REG_YMM22, XED_REG_YMM23,
       XED_REG_YMM24, XED_REG_YMM25, XED_REG_YMM26, XED_REG_YMM27, XED_REG_YMM28, XED_REG_YMM29,
       XED_REG_YMM30, XED_REG_YMM31}, "VRX256"
    },
    {
      {XED_REG_ZMM0, XED_REG_ZMM1, XED_REG_ZMM2, XED_REG_ZMM3, XED_REG_ZMM4, XED_REG_ZMM5,
       XED_REG_ZMM6, XED_REG_ZMM7, XED_REG_ZMM8, XED_REG_ZMM9, XED_REG_ZMM10, XED_REG_ZMM11,
       XED_REG_ZMM12, XED_REG_ZMM13, XED_REG_ZMM14, XED_REG_ZMM15, XED_REG_ZMM16, XED_REG_ZMM17,
       XED_REG_ZMM18, XED_REG_ZMM19, XED_REG_ZMM20, XED_REG_ZMM21, XED_REG_ZMM22, XED_REG_ZMM23,
       XED_REG_ZMM24, XED_REG_ZMM25, XED_REG_ZMM26, XED_REG_ZMM27, XED_REG_ZMM28, XED_REG_ZMM29,
       XED_REG_ZMM30, XED_REG_ZMM31}, "VRX512"
    },
    {
      {XED_REG_ST0, XED_REG_ST1, XED_REG_ST2, XED_REG_ST3, XED_REG_ST4, XED_REG_ST5, XED_REG_ST6, XED_REG_ST7},
      "FPST"
    },
  } {
    for (const xed_reg_enum_t *reg : Pipedream::singleton_register_classes()) {
      std::set<xed_reg_enum_t> reg_class;
      reg_class.insert(*reg);

      _names.emplace(reg_class, Pipedream::register_name(*reg));
    }
  }

  const std::string &operator[](const std::set<xed_reg_enum_t> &regs) const {
    return _names.at(regs);
  }

  std::map<std::set<xed_reg_enum_t>, std::string> _names;
};

} // end anonymous namespace

Span<xed_reg_enum_t[1]> Pipedream::singleton_register_classes() {
  return SINGLETON_REGISTER_CLASSES;
}

const std::string &Pipedream::register_class_name(Register_Class reg_class) {
  static Register_Class_Names REGISTER_CLASS_NAMES;

  std::set<xed_reg_enum_t> key{reg_class.begin(), reg_class.end()};

  return REGISTER_CLASS_NAMES[key];
}

Register_Class Pipedream::singleton_register_class(xed_reg_enum_t reg) {
  for (const xed_reg_enum_t *clss : SINGLETON_REGISTER_CLASSES) {
    if (clss[0] == reg) { return Register_Class{1, clss}; }
  }

  std::cerr << __func__ << ": bad register " << Pipedream::register_name(reg) << "\n";
  abort();
}

std::set<std::string> Pipedream::tags(XED_Decoded_Instruction inst, const Pipedream_Operand_Vector &operands) {
  std::set<std::string> tags;

  if (inst.inst().is_branch()) {
    tags.insert("branch");
  }

  if (xed_classify_sse(inst.raw())) {
    tags.insert("SSE");
  }
  if (xed_classify_avx(inst.raw())) {
    tags.insert("AVX");
  }
  if (xed_classify_avx512(inst.raw())) {
    tags.insert("AVX512");
  }

  switch (inst.inst().extension()) {
    case XED_EXTENSION_X87:
      tags.insert("x87");
      break;
    case XED_EXTENSION_WAITPKG:
      tags.insert("waitpkg");
      break;
    default:
      break;
  }

  for (const Pipedream_Operand_Template &op : operands) {
    switch (op.value.kind()) {
      case Machine_Value::REG:
        {
          const Register_Class reg_class = op.value.reg_class();

          if (Pipedream::is_vector_register_class(reg_class)) {
            tags.insert("vector");
          }
          if (Pipedream::is_scalar_register_class(reg_class)) {
            tags.insert("scalar");
          }
          if (Pipedream::is_segment_register_class(reg_class)) {
            tags.insert("segmentation");
          }
        }
        break;
      case Machine_Value::MEM:
        tags.insert("memory");
        break;
      default:
        break;
    }
  }

  for(unsigned i = 0, end = xed_attribute_max(); i < end; i++) {
    const xed_attribute_enum_t attr = xed_attribute(i);

    if (!inst.inst().has_attribute(attr)) {
      continue;
    }

    switch (attr) {
      case XED_ATTRIBUTE_AMDONLY:
        tags.insert("amdonly");
        break;
      case XED_ATTRIBUTE_ATT_OPERAND_ORDER_EXCEPTION:
      case XED_ATTRIBUTE_BROADCAST_ENABLED:
      case XED_ATTRIBUTE_BYTEOP:
      case XED_ATTRIBUTE_DISP8_EIGHTHMEM:
      case XED_ATTRIBUTE_DISP8_FULL:
      case XED_ATTRIBUTE_DISP8_FULLMEM:
      case XED_ATTRIBUTE_DISP8_GPR_READER:
      case XED_ATTRIBUTE_DISP8_GPR_READER_BYTE:
      case XED_ATTRIBUTE_DISP8_GPR_READER_WORD:
      case XED_ATTRIBUTE_DISP8_GPR_WRITER_LDOP_D:
      case XED_ATTRIBUTE_DISP8_GPR_WRITER_LDOP_Q:
      case XED_ATTRIBUTE_DISP8_GPR_WRITER_STORE:
      case XED_ATTRIBUTE_DISP8_GPR_WRITER_STORE_BYTE:
      case XED_ATTRIBUTE_DISP8_GPR_WRITER_STORE_WORD:
      case XED_ATTRIBUTE_DISP8_GSCAT:
      case XED_ATTRIBUTE_DISP8_HALF:
      case XED_ATTRIBUTE_DISP8_HALFMEM:
      case XED_ATTRIBUTE_DISP8_MEM128:
      case XED_ATTRIBUTE_DISP8_MOVDDUP:
      case XED_ATTRIBUTE_DISP8_QUARTERMEM:
      case XED_ATTRIBUTE_DISP8_SCALAR:
      case XED_ATTRIBUTE_DISP8_TUPLE1:
      case XED_ATTRIBUTE_DISP8_TUPLE1_4X:
      case XED_ATTRIBUTE_DISP8_TUPLE1_BYTE:
      case XED_ATTRIBUTE_DISP8_TUPLE1_WORD:
      case XED_ATTRIBUTE_DISP8_TUPLE2:
      case XED_ATTRIBUTE_DISP8_TUPLE4:
      case XED_ATTRIBUTE_DISP8_TUPLE8:
      case XED_ATTRIBUTE_DOUBLE_WIDE_MEMOP:
      case XED_ATTRIBUTE_DOUBLE_WIDE_OUTPUT:
      case XED_ATTRIBUTE_DWORD_INDICES:
      case XED_ATTRIBUTE_ELEMENT_SIZE_D:
      case XED_ATTRIBUTE_ELEMENT_SIZE_Q:
      case XED_ATTRIBUTE_EXCEPTION_BR:
      case XED_ATTRIBUTE_FAR_XFER:
      case XED_ATTRIBUTE_FIXED_BASE0:
      case XED_ATTRIBUTE_FIXED_BASE1:
      case XED_ATTRIBUTE_GATHER:
      case XED_ATTRIBUTE_HALF_WIDE_OUTPUT:
      case XED_ATTRIBUTE_HLE_ACQ_ABLE:
      case XED_ATTRIBUTE_HLE_REL_ABLE:
      case XED_ATTRIBUTE_IGNORES_OSFXSR:
      case XED_ATTRIBUTE_IMPLICIT_ONE:
      case XED_ATTRIBUTE_INDEX_REG_IS_POINTER:
      case XED_ATTRIBUTE_INDIRECT_BRANCH:
      case XED_ATTRIBUTE_KMASK:
      case XED_ATTRIBUTE_LOCKABLE:
      case XED_ATTRIBUTE_LOCKED:
      case XED_ATTRIBUTE_MASKOP:
      case XED_ATTRIBUTE_MASKOP_EVEX:
      case XED_ATTRIBUTE_MASK_AS_CONTROL:
      case XED_ATTRIBUTE_MASK_VARIABLE_MEMOP:
      case XED_ATTRIBUTE_MEMORY_FAULT_SUPPRESSION:
      case XED_ATTRIBUTE_MMX_EXCEPT:
      case XED_ATTRIBUTE_MPX_PREFIX_ABLE:
      case XED_ATTRIBUTE_MULTISOURCE4:
      case XED_ATTRIBUTE_MULTIDEST2:
        break;
      case XED_ATTRIBUTE_MXCSR:
      case XED_ATTRIBUTE_MXCSR_RD:
//          tags.insert("MXCSR");
        break;
      case XED_ATTRIBUTE_NONTEMPORAL:
        tags.insert("nontemporal");
        break;
      case XED_ATTRIBUTE_NOP:
        tags.insert("nop");
        break;
      case XED_ATTRIBUTE_NOTSX:
      case XED_ATTRIBUTE_NOTSX_COND:
      case XED_ATTRIBUTE_NO_RIP_REL:
        break;
      case XED_ATTRIBUTE_PREFETCH:
        tags.insert("prefetch");
        break;
      case XED_ATTRIBUTE_PROTECTED_MODE:
        tags.insert("protected-mode");
        break;
      case XED_ATTRIBUTE_QWORD_INDICES:
        break;
      case XED_ATTRIBUTE_REP:
        tags.insert("rep");
        break;
      case XED_ATTRIBUTE_REQUIRES_ALIGNMENT:
        tags.insert("requires-alignment");
        break;
      case XED_ATTRIBUTE_RING0:
        tags.insert("ring0");
        break;
      case XED_ATTRIBUTE_SCALABLE:
      case XED_ATTRIBUTE_SCATTER:
        break;
      case XED_ATTRIBUTE_SIMD_SCALAR:
        tags.insert("SIMD-scalar");
        break;
      case XED_ATTRIBUTE_SKIPLOW32:
      case XED_ATTRIBUTE_SKIPLOW64:
      case XED_ATTRIBUTE_SPECIAL_AGEN_REQUIRED:
        break;
      case XED_ATTRIBUTE_STACKPOP0:
      case XED_ATTRIBUTE_STACKPOP1:
      case XED_ATTRIBUTE_STACKPUSH0:
      case XED_ATTRIBUTE_STACKPUSH1:
        tags.insert("stack");
        break;
      case XED_ATTRIBUTE_X87_CONTROL:
      case XED_ATTRIBUTE_X87_MMX_STATE_CW:
      case XED_ATTRIBUTE_X87_MMX_STATE_R:
      case XED_ATTRIBUTE_X87_MMX_STATE_W:
      case XED_ATTRIBUTE_X87_NOWAIT:
        tags.insert("x87");
        break;
      case XED_ATTRIBUTE_XMM_STATE_CW:
      case XED_ATTRIBUTE_XMM_STATE_R:
      case XED_ATTRIBUTE_XMM_STATE_W:
        break;
      case XED_ATTRIBUTE_INVALID:
      case XED_ATTRIBUTE_LAST:
        abort();
    }
  }

  return tags;
}

bool Pipedream::is_vector_register_class(Register_Class reg_class) {
  for (xed_reg_enum_t reg : reg_class) {
    switch (reg) {
      case XED_REG_MMX0:
      case XED_REG_MMX1:
      case XED_REG_MMX2:
      case XED_REG_MMX3:
      case XED_REG_MMX4:
      case XED_REG_MMX5:
      case XED_REG_MMX6:
      case XED_REG_MMX7:

      case XED_REG_XMM0:
      case XED_REG_XMM1:
      case XED_REG_XMM2:
      case XED_REG_XMM3:
      case XED_REG_XMM4:
      case XED_REG_XMM5:
      case XED_REG_XMM6:
      case XED_REG_XMM7:
      case XED_REG_XMM8:
      case XED_REG_XMM9:
      case XED_REG_XMM10:
      case XED_REG_XMM11:
      case XED_REG_XMM12:
      case XED_REG_XMM13:
      case XED_REG_XMM14:
      case XED_REG_XMM15:
      case XED_REG_XMM16:
      case XED_REG_XMM17:
      case XED_REG_XMM18:
      case XED_REG_XMM19:
      case XED_REG_XMM20:
      case XED_REG_XMM21:
      case XED_REG_XMM22:
      case XED_REG_XMM23:
      case XED_REG_XMM24:
      case XED_REG_XMM25:
      case XED_REG_XMM26:
      case XED_REG_XMM27:
      case XED_REG_XMM28:
      case XED_REG_XMM29:
      case XED_REG_XMM30:
      case XED_REG_XMM31:

      case XED_REG_YMM0:
      case XED_REG_YMM1:
      case XED_REG_YMM2:
      case XED_REG_YMM3:
      case XED_REG_YMM4:
      case XED_REG_YMM5:
      case XED_REG_YMM6:
      case XED_REG_YMM7:
      case XED_REG_YMM8:
      case XED_REG_YMM9:
      case XED_REG_YMM10:
      case XED_REG_YMM11:
      case XED_REG_YMM12:
      case XED_REG_YMM13:
      case XED_REG_YMM14:
      case XED_REG_YMM15:
      case XED_REG_YMM16:
      case XED_REG_YMM17:
      case XED_REG_YMM18:
      case XED_REG_YMM19:
      case XED_REG_YMM20:
      case XED_REG_YMM21:
      case XED_REG_YMM22:
      case XED_REG_YMM23:
      case XED_REG_YMM24:
      case XED_REG_YMM25:
      case XED_REG_YMM26:
      case XED_REG_YMM27:
      case XED_REG_YMM28:
      case XED_REG_YMM29:
      case XED_REG_YMM30:
      case XED_REG_YMM31:

      case XED_REG_ZMM0:
      case XED_REG_ZMM1:
      case XED_REG_ZMM2:
      case XED_REG_ZMM3:
      case XED_REG_ZMM4:
      case XED_REG_ZMM5:
      case XED_REG_ZMM6:
      case XED_REG_ZMM7:
      case XED_REG_ZMM8:
      case XED_REG_ZMM9:
      case XED_REG_ZMM10:
      case XED_REG_ZMM11:
      case XED_REG_ZMM12:
      case XED_REG_ZMM13:
      case XED_REG_ZMM14:
      case XED_REG_ZMM15:
      case XED_REG_ZMM16:
      case XED_REG_ZMM17:
      case XED_REG_ZMM18:
      case XED_REG_ZMM19:
      case XED_REG_ZMM20:
      case XED_REG_ZMM21:
      case XED_REG_ZMM22:
      case XED_REG_ZMM23:
      case XED_REG_ZMM24:
      case XED_REG_ZMM25:
      case XED_REG_ZMM26:
      case XED_REG_ZMM27:
      case XED_REG_ZMM28:
      case XED_REG_ZMM29:
      case XED_REG_ZMM30:
      case XED_REG_ZMM31:
        break;
      default:
        return false;
    }
  }

  return true;
}

bool Pipedream::is_scalar_register_class(Register_Class reg_class) {
  for (xed_reg_enum_t reg : reg_class) {
    switch (reg) {
      case XED_REG_AL:
      case XED_REG_CL:
      case XED_REG_DL:
      case XED_REG_BL:
      case XED_REG_AH:
      case XED_REG_CH:
      case XED_REG_DH:
      case XED_REG_BH:
      case XED_REG_SPL:
      case XED_REG_BPL:
      case XED_REG_SIL:
      case XED_REG_DIL:
      case XED_REG_R8B:
      case XED_REG_R9B:
      case XED_REG_R10B:
      case XED_REG_R11B:
      case XED_REG_R12B:
      case XED_REG_R13B:
      case XED_REG_R14B:
      case XED_REG_R15B:
      case XED_REG_AX:
      case XED_REG_CX:
      case XED_REG_DX:
      case XED_REG_BX:
      case XED_REG_SP:
      case XED_REG_BP:
      case XED_REG_SI:
      case XED_REG_DI:
      case XED_REG_R8W:
      case XED_REG_R9W:
      case XED_REG_R10W:
      case XED_REG_R11W:
      case XED_REG_R12W:
      case XED_REG_R13W:
      case XED_REG_R14W:
      case XED_REG_R15W:
      case XED_REG_EAX:
      case XED_REG_ECX:
      case XED_REG_EDX:
      case XED_REG_EBX:
      case XED_REG_ESP:
      case XED_REG_EBP:
      case XED_REG_ESI:
      case XED_REG_EDI:
      case XED_REG_R8D:
      case XED_REG_R9D:
      case XED_REG_R10D:
      case XED_REG_R11D:
      case XED_REG_R12D:
      case XED_REG_R13D:
      case XED_REG_R14D:
      case XED_REG_R15D:
      case XED_REG_RAX:
      case XED_REG_RCX:
      case XED_REG_RDX:
      case XED_REG_RBX:
      case XED_REG_RSP:
      case XED_REG_RBP:
      case XED_REG_RSI:
      case XED_REG_RDI:
      case XED_REG_R8:
      case XED_REG_R9:
      case XED_REG_R10:
      case XED_REG_R11:
      case XED_REG_R12:
      case XED_REG_R13:
      case XED_REG_R14:
      case XED_REG_R15:
        break;
      default:
        return false;
    }
  }

  return true;
}

bool Pipedream::is_segment_register_class(Register_Class reg_class) {
  for (xed_reg_enum_t reg : reg_class) {
    switch (reg) {
      case XED_REG_ES:
      case XED_REG_FS:
      case XED_REG_GS:
      case XED_REG_SS:
      case XED_REG_FSBASE:
      case XED_REG_GSBASE:
        break;
      default:
        return false;
    }
  }

  return true;
}
