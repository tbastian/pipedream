///
/// Helpers for emitting instruction descriptions pipedream can understand
///

#pragma once

#include "config.hpp"

#include "XED_Info.hpp"
#include <map>      // for std::map
#include <string>   // for std::string
#include <cstring>  // for strlen
#include <set>      // for std::set
#include <optional> // for std::optional

struct Pipedream_Operand_Template;

using Pipedream_Operand_Vector = std::vector<Pipedream_Operand_Template>;

/// Constants and helper functions for working with pipedreams view of an
/// instruction.
struct Pipedream {
  /// @name types
  /// @{

  enum Type {
    /// floating point
    F16,
    F32,
    F64,
    F80,

    /// signed int
    I1,
    I8,
    I16,
    I32,
    I64,

    /// unsigned int
    U4,
    U8,
    U16,
    U32,
    U64,
    U128,
    U256,

    /// binary coded decimal
    BCD80,

    /// too complex or variable sized
    OTHER,
  };

  static Type type(xed_operand_element_xtype_enum_t xtype, xed_operand_element_type_enum_t element_type, unsigned element_bits) {
    auto int_for_bits = [element_bits]() -> Type {
      switch (element_bits) {
        case  1:  return I1;
        case  8:  return I8;
        case  16: return I16;
        case  32: return I32;
        case  64: return I64;
        case   0: return OTHER; // for RDTSCP
        case 128: return OTHER; // for RET_FAR
        case  80: return OTHER; // treating FP stack as an int (for IRET)
        case 160: return OTHER; // treating FP stack as an int (for IRETD)
        case 320: return OTHER; // treating FP stack as an int (for IRETQ)
      }
      std::cerr << "error: Pipedream::type: invalid element_bits for int: " << element_bits << "\n";
      abort();
    };
    auto uint_for_bits = [element_bits]() -> Type {
      switch (element_bits) {
        case 8:   return U8;
        case 16:  return U16;
        case 32:  return U32;
        case 64:  return U64;
        case 128: return U128;
        case 256: return U256;
      }
      std::cerr << "error: Pipedream::type: invalid element_bits for uint: " << element_bits << "\n";
      abort();
    };

    switch (xtype) {
      case XED_OPERAND_XTYPE_B80: return BCD80;

      case XED_OPERAND_XTYPE_F16: return F16;
      case XED_OPERAND_XTYPE_F32: return F32;
      case XED_OPERAND_XTYPE_F64: return F64;
      case XED_OPERAND_XTYPE_F80: return F80;

      case XED_OPERAND_XTYPE_I1:  return I1;
      case XED_OPERAND_XTYPE_I8:  return I8;
      case XED_OPERAND_XTYPE_I16: return I16;
      case XED_OPERAND_XTYPE_I32: return I32;
      case XED_OPERAND_XTYPE_I64: return I64;
      case XED_OPERAND_XTYPE_INT: return int_for_bits();

      case XED_OPERAND_XTYPE_U8:   return U8;
      case XED_OPERAND_XTYPE_U16:  return U16;
      case XED_OPERAND_XTYPE_U32:  return U32;
      case XED_OPERAND_XTYPE_U64:  return U64;
      case XED_OPERAND_XTYPE_U128: return U128;
      case XED_OPERAND_XTYPE_U256: return U256;
      case XED_OPERAND_XTYPE_UINT: return uint_for_bits();

      case XED_OPERAND_XTYPE_STRUCT:
      case XED_OPERAND_XTYPE_VAR:
        return OTHER;

      case XED_OPERAND_XTYPE_INVALID:
        switch (element_type) {
          case XED_OPERAND_ELEMENT_TYPE_UINT:       return uint_for_bits();
          case XED_OPERAND_ELEMENT_TYPE_INT:        return int_for_bits();
          case XED_OPERAND_ELEMENT_TYPE_SINGLE:     return F32;
          case XED_OPERAND_ELEMENT_TYPE_DOUBLE:     return F64;
          case XED_OPERAND_ELEMENT_TYPE_LONGDOUBLE: return F80;
          case XED_OPERAND_ELEMENT_TYPE_FLOAT16:    return F16;
          case XED_OPERAND_ELEMENT_TYPE_LONGBCD:    return BCD80;

          case XED_OPERAND_ELEMENT_TYPE_STRUCT:
          case XED_OPERAND_ELEMENT_TYPE_VARIABLE:
            return OTHER;

          case XED_OPERAND_ELEMENT_TYPE_INVALID:
          case XED_OPERAND_ELEMENT_TYPE_LAST:
          default:
            abort();
        }

      case XED_OPERAND_XTYPE_LAST:
      default:
        abort();
    }
  }

  static const char *type_name(Type t) {
    switch (t) {
      case F16:   return "f16";
      case F32:   return "f32";
      case F64:   return "f64";
      case F80:   return "f80";
      case I1:    return "i1";
      case I8:    return "i8";
      case I16:   return "i16";
      case I32:   return "i32";
      case I64:   return "i64";
      case U4:    return "u4";
      case U8:    return "u8";
      case U16:   return "u16";
      case U32:   return "u32";
      case U64:   return "u64";
      case U128:  return "u128";
      case U256:  return "u256";
      case BCD80: return "bcd80";
      case OTHER: return "OTHER";
    }
    abort();
  }

  static unsigned type_bits(Type t) {
    switch (t) {
      case F16:   return 16;
      case F32:   return 32;
      case F64:   return 64;
      case F80:   return 80;
      case I1:    return 1;
      case I8:    return 8;
      case I16:   return 16;
      case I32:   return 32;
      case I64:   return 64;
      case U4:    return 4;
      case U8:    return 8;
      case U16:   return 16;
      case U32:   return 32;
      case U64:   return 64;
      case U128:  return 128;
      case U256:  return 256;
      case BCD80: return 80;
      case OTHER: abort();
    }
    abort();
  }

  /// @}

  /// @name CPU flags
  /// @{

  /// flags for CPU flags register
  enum class Flag : uint64_t {
    CF   = 0x0000'0000'0001, /// Carry flag
    PF   = 0x0000'0000'0004, /// Parity flag
    AF   = 0x0000'0000'0010, /// Adjust flag
    ZF   = 0x0000'0000'0040, /// Zero flag
    SF   = 0x0000'0000'0080, /// Sign flag
    TF   = 0x0000'0000'0100, /// Trap flag
    IF   = 0x0000'0000'0200, /// Interrupt enable flag
    DF   = 0x0000'0000'0400, /// Direction flag
    OF   = 0x0000'0000'0800, /// Overflow flag
    IOPL = 0x0000'0000'3000, /// I/O privilege level (286+ only)
    NT   = 0x0000'0000'4000, /// Nested task flag (286+ only)
    /// EFLAGS
    RF   = 0x0000'0001'0000, /// Resume flag (386+ only)
    VM   = 0x0000'0002'0000, /// Virtual 8086 mode flag (386+ only)
    AC   = 0x0000'0004'0000, /// Alignment check (486SX+ only)
    VIF  = 0x0000'0008'0000, /// Virtual interrupt flag (Pentium+)
    VIP  = 0x0000'0010'0000, /// Virtual interrupt pending (Pentium+)
    ID   = 0x0000'0020'0000, /// Able to use CPUID instruction (Pentium+)
    /// RFLAGS

    /// X87 FLAGS
    FC0 = 0x0001'0000'0000,
    FC1 = 0x0002'0000'0000,
    FC2 = 0x0004'0000'0000,
    FC3 = 0x0008'0000'0000,
  };

  using Flag_Set = uint64_t;

  static const char *flag_name(Flag flag) {
    switch (flag) {
      case Flag::CF:   return "CF";
      case Flag::PF:   return "PF";
      case Flag::AF:   return "AF";
      case Flag::ZF:   return "ZF";
      case Flag::SF:   return "SF";
      case Flag::TF:   return "TF";
      case Flag::IF:   return "IF";
      case Flag::DF:   return "DF";
      case Flag::OF:   return "OF";
      case Flag::IOPL: return "IOPL";
      case Flag::NT:   return "NT";
      case Flag::RF:   return "RF";
      case Flag::VM:   return "VM";
      case Flag::AC:   return "AC";
      case Flag::VIF:  return "VIF";
      case Flag::VIP:  return "VIP";
      case Flag::ID:   return "ID";
      case Flag::FC0:  return "FC0";
      case Flag::FC1:  return "FC1";
      case Flag::FC2:  return "FC2";
      case Flag::FC3:  return "FC3";
    }
    abort();
  }

  static Flag flag_first() { return Flag::CF; }

  static std::optional<Flag> flag_next(Flag flag) {
    switch (flag) {
      case Flag::CF:   return Flag::PF;
      case Flag::PF:   return Flag::AF;
      case Flag::AF:   return Flag::ZF;
      case Flag::ZF:   return Flag::SF;
      case Flag::SF:   return Flag::TF;
      case Flag::TF:   return Flag::IF;
      case Flag::IF:   return Flag::DF;
      case Flag::DF:   return Flag::OF;
      case Flag::OF:   return Flag::IOPL;
      case Flag::IOPL: return Flag::NT;
      case Flag::NT:   return Flag::RF;
      case Flag::RF:   return Flag::VM;
      case Flag::VM:   return Flag::AC;
      case Flag::AC:   return Flag::VIF;
      case Flag::VIF:  return Flag::VIP;
      case Flag::VIP:  return Flag::ID;
      case Flag::ID:   return Flag::FC0;
      case Flag::FC0:  return Flag::FC1;
      case Flag::FC1:  return Flag::FC2;
      case Flag::FC2:  return Flag::FC3;
      case Flag::FC3:  return std::nullopt;
    }
    abort();
  }

  static void print_flag_set(Flag_Set flags, std::ostream &O, const char *name_prefix = "") {
    std::vector<const char *> names;

    for (std::optional<Flag> it = Pipedream::flag_first(); it; it = Pipedream::flag_next(*it)) {
      if (Pipedream::Flag_Set(*it) & flags) {
        names.push_back(Pipedream::flag_name(*it));
      }
    }

    if (names.empty()) {
      O << "0";
    } else {
      O << name_prefix << names.front();

      for (auto it = names.begin() + 1, end = names.end(); it != end; it++) {
        O << " | " << name_prefix << *it;
      }
    }
  }

  /// @}

  /// @name registers
  /// @{

  static const std::string &register_class_name(Register_Class reg_class);

  static Span<xed_reg_enum_t[1]> singleton_register_classes();

  static Register_Class singleton_register_class(xed_reg_enum_t reg);

  static bool is_vector_register_class(Register_Class reg_class);

  static bool is_scalar_register_class(Register_Class reg_class);

  static bool is_segment_register_class(Register_Class reg_class);

  static const char *register_name(xed_reg_enum_t reg) {
    switch (reg) {
      case XED_REG_ST0: return "ST0";
      case XED_REG_ST1: return "ST1";
      case XED_REG_ST2: return "ST2";
      case XED_REG_ST3: return "ST3";
      case XED_REG_ST4: return "ST4";
      case XED_REG_ST5: return "ST5";
      case XED_REG_ST6: return "ST6";
      case XED_REG_ST7: return "ST7";
      default:          return xed_reg_enum_t2str(reg);
   }
  }

  /// @}

  /// compute the tags this instruction should have
  static std::set<std::string> tags(XED_Decoded_Instruction inst, const Pipedream_Operand_Vector &operands);
private:
  Pipedream() = delete;
};


/// describes one *value* in machine code (a register, an immediate, a memory reference)
struct Machine_Value {
  enum Kind {
    REG,
    IMM,
    /// calculate address but do not access it (LEA, BND, ...)
    ADDR,
    /// load/store
    MEM,
    /// branch displacement
    BRDISP,
    /// read/write CPU flags
    FLAGS,
  };

  /// @name ctor/dtor
  /// @{

  Machine_Value() : _kind{IMM} { _imm.imm_bits = 0; }

  static Machine_Value make_reg(Register_Class reg_class, xed_reg_enum_t reg) {
    Machine_Value v;
    v._kind          = REG;
    v._reg.reg       = reg;
    v._reg.reg_class = reg_class;
    return v;
  }

  static Machine_Value make_reg(Register_Class reg_class) {
    Machine_Value v;
    v._kind          = REG;
    v._reg.reg       = std::nullopt;
    v._reg.reg_class = reg_class;
    return v;
  }

  static Machine_Value make_imm(uint64_t imm_bits) {
    Machine_Value v;
    v._kind         = IMM;
    v._imm.imm_bits = imm_bits;
    return v;
  }

  static Machine_Value make_addr(uint64_t address_bits) {
    Machine_Value v;
    v._kind             = ADDR;
    v._mem.address_bits = address_bits;
    v._mem.memory_bits  = 0;
    v._mem.base_reg     = std::nullopt;
    return v;
  }

  static Machine_Value make_mem(uint64_t address_bits, uint64_t memory_bits, std::optional<xed_reg_enum_t> base) {
    Machine_Value v;
    v._kind             = MEM;
    v._mem.address_bits = address_bits;
    v._mem.memory_bits  = memory_bits;
    v._mem.base_reg     = base;
    return v;
  }

  static Machine_Value make_brdisp(uint64_t disp_bits) {
    Machine_Value v;
    v._kind             = BRDISP;
    v._brdisp.disp_bits = disp_bits;
    return v;
  }

  static Machine_Value make_flags(xed_reg_enum_t reg, Pipedream::Flag_Set read, Pipedream::Flag_Set written) {
    Machine_Value v;
    v._kind        = FLAGS;
    v._flags.reg   = reg;
    v._flags.read  = read;
    v._flags.write = written;
    return v;
  }

  /// @}

  /// @name misc
  /// @{

  Kind kind() const { return _kind; }

  /// @}

  /// @name register operands
  /// @{

  bool is_reg() const {
    return kind() == REG;
  }

  std::optional<xed_reg_enum_t> reg() const {
    assert(kind() == REG);
    return _reg.reg;
  }
  Register_Class reg_class() const {
    assert(kind() == REG);
    return _reg.reg_class;
  }

  /// @}

  /// @name immediate operands
  /// @{

  uint64_t imm_bits() const {
    assert(kind() == IMM);
    return _imm.imm_bits;
  }

  /// @}

  /// @name memory & address operands
  /// @{

  bool is_mem() const {
    return kind() == MEM;
  }

  uint64_t address_bits() const {
    assert(kind() == MEM || kind() == ADDR);
    return _mem.address_bits;
  }

  uint64_t memory_bits() const {
    assert(kind() == MEM);
    return _mem.memory_bits;
  }
  std::optional<xed_reg_enum_t> base_reg() const {
    assert(kind() == MEM || kind() == ADDR);
    return _mem.base_reg;
  }

  /// @}

  /// @name branch target operands
  /// @{

  uint64_t brdisp_bits() const {
    assert(kind() == BRDISP);
    return _brdisp.disp_bits;
  }

  /// @}

  /// @name flag operands
  /// @{

  xed_reg_enum_t flag_reg() const {
    assert(kind() == FLAGS);
    return _flags.reg;
  }
  Pipedream::Flag_Set flags_read() const {
    assert(kind() == FLAGS);
    return _flags.read;
  }
  Pipedream::Flag_Set flags_written() const {
    assert(kind() == FLAGS);
    return _flags.write;
  }

  /// @}

  /// @name comparison
  /// @{

  bool operator<(const Machine_Value &that) const {
    return compare(that, std::less<void>());
  }

  bool operator==(const Machine_Value &that) const {
    return compare(that, std::equal_to<void>());
  }

  bool operator!=(const Machine_Value &that) const {
    return !(*this == that);
  }

  /// @}

  /// @name I/O
  /// @{

  friend std::ostream &operator<<(std::ostream &O, const Machine_Value &v) {
    switch (v.kind()) {
      case REG:
        O << "reg(" << Pipedream::register_class_name(v.reg_class());
        if (auto reg = v.reg()) {
          O << ", " << Pipedream::register_name(*reg);
        }
        O << ")";
        return O;
      case IMM:
        return O << "imm(" << v.imm_bits() << ")";
      case ADDR:
        return O << "addr(" << v.address_bits() << ")";
      case MEM:
        O << "mem(" << v.address_bits() << ", " << v.memory_bits();
        if (auto reg = v.base_reg()) {
          O << ", " << Pipedream::register_name(*reg);
        }
        O << ")";
        return O;
      case BRDISP:
        O << "brdisp(" << v.brdisp_bits() << ")";
        return O;
      case FLAGS:
        O << "flags(" << Pipedream::register_name(v.flag_reg()) << ", ";

        Pipedream::print_flag_set(v.flags_read(), O);
        O << ", ";
        Pipedream::print_flag_set(v.flags_written(), O);

        O << ")";
        return O;
    }
    return O << "invalid";
  }

  /// @}
private:
  template<typename Op>
  bool compare(const Machine_Value &that, Op &&comparator) const {
    if (_kind == that._kind) {
      switch (_kind) {
        case REG:
          return comparator(std::tie(_reg.reg_class, _reg.reg), std::tie(that._reg.reg_class, that._reg.reg));
        case IMM:
          return comparator(_imm.imm_bits, that._imm.imm_bits);
        case BRDISP:
          return comparator(_brdisp.disp_bits, that._brdisp.disp_bits);
        case ADDR:
        case MEM:
          return comparator(std::tie(_mem.address_bits, _mem.memory_bits, _mem.base_reg), std::tie(that._mem.address_bits, that._mem.memory_bits, that._mem.base_reg));
        case FLAGS:
          return comparator(std::tie(_flags.reg, _flags.read, _flags.write), std::tie(that._flags.reg, that._flags.read, that._flags.write));
      }
      abort();
    } else {
      return comparator(_kind, that._kind);
    }
  }

  Kind _kind;

  union {
    struct {
      Register_Class reg_class;
      std::optional<xed_reg_enum_t> reg;
    } _reg;
    struct {
      uint64_t imm_bits;
    } _imm;
    struct {
      /// width of memory address in bits
      uint64_t address_bits;
      /// number of bits loaded/stored
      uint64_t memory_bits;
      std::optional<xed_reg_enum_t> base_reg;
    } _mem;
    struct {
      uint64_t disp_bits;
    } _brdisp;
    struct {
      xed_reg_enum_t reg;
      Pipedream::Flag_Set read;
      Pipedream::Flag_Set write;
    } _flags;
  };
};


/// An operand of an instruction in the pipedream machine description
struct Pipedream_Operand_Template {
  xed_operand_enum_t name;

  xed_operand_visibility_enum_t visibility;

  Machine_Value value;
  xed_operand_action_enum_t action;

  size_t num_elements;
  Pipedream::Type type;

  std::optional<xed_reg_enum_t> reg() const {
    switch (value.kind()) {
      case Machine_Value::REG:   return value.reg();
      case Machine_Value::FLAGS: return value.flag_reg();
      default: return std::nullopt;
    }
  }

  bool is_reg() const {
    return value.is_reg();
  }

  bool is_mem() const {
    return value.is_mem();
  }

  bool is_brdisp() const {
    return value.kind() == Machine_Value::BRDISP;
  }

  bool operator<(const Pipedream_Operand_Template &that) const {
    return compare(that, std::less<void>());
  }
  bool operator==(const Pipedream_Operand_Template &that) const {
    return compare(that, std::equal_to<void>());
  }
  bool operator!=(const Pipedream_Operand_Template &that) const {
    return compare(that, std::not_equal_to<void>());
  }

  std::string type_name() const {
    std::string name = Pipedream::type_name(type);
    if (num_elements > 1) {
      name += "x";
      name += std::to_string(num_elements);
    }
    return name;
  }

  std::string short_name() const {
    switch (value.kind()) {
      case Machine_Value::REG:
        if (auto reg = value.reg()) {
          return Pipedream::register_name(*reg) + type_name();
        } else {
          return Pipedream::register_class_name(value.reg_class()) + type_name();
        }
      case Machine_Value::IMM:
        return "IMM" + type_name();
      case Machine_Value::ADDR:
        return "ADDR" + std::to_string(value.address_bits()) + type_name();
      case Machine_Value::MEM:
        return "MEM" + std::to_string(value.address_bits()) + type_name();
      case Machine_Value::BRDISP:
        return "BRDISP" + std::to_string(value.brdisp_bits());
      case Machine_Value::FLAGS:
        return "FLAGS";
    }
    abort();
  }

  friend std::ostream &operator<<(std::ostream &O, const Pipedream_Operand_Template &op) {
    const char *name       = xed_operand_enum_t2str(op.name);
    const char *visibility = xed_operand_visibility_enum_t2str(op.visibility);
    const char *action     = xed_operand_action_enum_t2str(op.action);
    const char *type       = Pipedream::type_name(op.type);

    return O << "Pipedream_Operand_Template" << std::tie(name, visibility, op.value, action, op.num_elements, type);
  }
private:
  template<typename Op>
  bool compare(const Pipedream_Operand_Template &that, Op &&compare) const {
    const auto tie = [](const Pipedream_Operand_Template &a) {
      return std::tie(a.name, a.visibility, a.value, a.action, a.num_elements, a.type);
    };

    return compare(tie(*this), tie(that));
  }
};


/// A XED instruction we are able to encode.
struct Pipedream_Instruction {
  /// the minimum amount of information (should uniquely identify the instruction)
  xed_iclass_enum_t iclass;
  Pipedream_Operand_Vector operands;
  xed_isa_set_enum_t isa;

  /// additional information (try to add as much as possible while still keeping the instruction uniquely identifiable by the keys above)
  xed_extension_enum_t extension;
  xed_category_enum_t category;
  std::set<std::string> tags;

  std::string att_mnemonic;
  std::string intel_mnemonic;

  bool operator<(const Pipedream_Instruction &that) const { return compare(that, std::less<void>{}); }
  bool operator==(const Pipedream_Instruction &that) const { return compare(that, std::equal_to<void>{}); }

  friend std::ostream &operator<<(std::ostream &O, const Pipedream_Instruction &x) {
    O << "  " << xed_iclass_enum_t2str(x.iclass) << "\n";

    for (const auto &op : x.operands) {
      O << "    " << op << "\n";
    }

    O << "    isa " << xed_isa_set_enum_t2str(x.isa) << "\n";
    O << "    ext " << xed_extension_enum_t2str(x.extension) << "\n";
    O << "    cat " << xed_category_enum_t2str(x.category) << "\n";
    O << "    tag";
    for (const std::string &tag : x.tags) {
      O << " " << tag;
    }
    O << "\n";
    O << "    at&t  " << x.att_mnemonic << "\n";
    O << "    intel " << x.intel_mnemonic << "\n";

    O << "\n";
    return O;
  }
private:
  template<typename Op>
  bool compare(const Pipedream_Instruction &that, Op &&cmp) const {
    const auto tie = [](const Pipedream_Instruction &ei) {
      return std::tie(ei.iclass, ei.operands, ei.isa, ei.extension, ei.category, ei.tags, ei.att_mnemonic, ei.intel_mnemonic);
    };

    return cmp(tie(*this), tie(that));
  }
};


/// Helper functions for converting from XED instructions to Pipedream intructions.
struct Convert {
  static std::optional<Machine_Value> machine_value_from_xed(
    XED_Instruction inst, XED_Instruction_Operand op, const XED_Encoder_State &state,
    unsigned memory_width
  ) {
    if (op.is_reg_op() || op.is_base_op()) {
      if (auto reg = op.get_reg(state)) {
        assert(reg != XED_REG_INVALID);
        return Machine_Value::make_reg(Pipedream::singleton_register_class(*reg), *reg);
      } else if (auto rc = op.get_reg_class(state)) {
        return Machine_Value::make_reg(*rc);
      }
    } else if (op.is_memory_op()) {
      return Machine_Value::make_mem(state.effective_address_width, memory_width, std::nullopt);
    } else if (op.is_address_generate_op()) {
      return Machine_Value::make_addr(state.effective_address_width);
    } else if (op.is_relative_branch_target()) {
      const unsigned brdisp_bits = xed_operand_width_bits(op.raw(), state.xed_eosz());

      return Machine_Value::make_brdisp(brdisp_bits);
    } else if (op.is_imm0_op() || op.is_imm1_op()) {
      const unsigned imm_bits = xed_operand_width_bits(op.raw(), state.xed_eosz());

      return Machine_Value::make_imm(imm_bits);
    }

    /// FIXME: can't handle yet
    (void) inst;
#if XED_EXTRACT_DEVELOPER_MODE
    std::cerr << "# TODO: " << __FILE__ << ":" << __LINE__ << ":" << __func__ << ":" << std::endl;
    std::cerr << "# TODO: Can not handle operand of " << inst.iform_str() << std::endl;
    std::cerr << "#   IFORM:     " << inst.iform_str() << std::endl;
    std::cerr << "#   ICLASS:    " << inst.iclass_str() << std::endl;
    std::cerr << "#   OPERAND:   " << op.name_str() << std::endl;
    if (op.is_lookup_fn()) {
      std::cerr << "#   LOOKUP_FN: " << op.get_lookup_fn_name() << std::endl;
    }
    std::cerr << "#   TYPE:      " << op.type_str() << std::endl;
    abort();
#endif
    return std::nullopt;
  }

  static std::optional<Machine_Value> machine_value_from_xed(XED_Decoded_Instruction inst,
                                                             XED_Decoded_Instruction_Operand dec_op,
                                                             const XED_Encoder_State &state) {
    XED_Instruction_Operand op = dec_op.static_op();

    if (op.is_reg_op()) {
      if (auto reg = op.get_reg(state)) {
        assert(reg != XED_REG_INVALID);

        switch (*reg) {
          case XED_REG_FLAGS:
          case XED_REG_EFLAGS:
          case XED_REG_RFLAGS:
          {
            const auto read  = read_flags_from_xed(inst);
            const auto write = write_flags_from_xed(inst);

            /// TODO: some shifts & rotates never read/write flags depending on machine mode.
            ///       XED only knows this after decoding. Drop the flags operand in this case.
            // assert(read | write);

            return Machine_Value::make_flags(*reg, read, write);
          }
          default:
            return Machine_Value::make_reg(Pipedream::singleton_register_class(*reg), *reg);
        }
      } else if (auto rc = op.get_reg_class(state)) {
        return Machine_Value::make_reg(*rc);
      }
    }

    if (op.is_memory_op()) {
      unsigned idx = dec_op.memory_operand_index();

      std::optional<xed_reg_enum_t> base_reg = inst.get_base_reg(idx);

      return Machine_Value::make_mem(dec_op.memory_address_bits(), dec_op.memory_operand_bits(), base_reg);
    } else if (op.is_address_generate_op()) {
      return Machine_Value::make_addr(dec_op.memory_address_bits());
    } else if (op.is_imm0_op()) {
      return Machine_Value::make_imm(dec_op.element_bits());
    } else if (op.is_relative_branch_target()) {
      return Machine_Value::make_brdisp(dec_op.element_bits());
    }

    return std::nullopt;
  }

  static Pipedream_Operand_Template pipedream_operand_from_xed(
    XED_Decoded_Instruction inst,
    XED_Decoded_Instruction_Operand xed_dec_op,
    const XED_Encoder_State &state
  ) {
    const XED_Instruction_Operand xed_op = xed_dec_op.static_op();

    const xed_operand_element_xtype_enum_t xed_xtype = xed_dec_op.element_xtype();
    const xed_operand_element_type_enum_t  xed_type  = xed_dec_op.element_type();

    unsigned bits = xed_dec_op.element_bits();
    unsigned num_elements = xed_dec_op.num_elements();

    const std::optional<Machine_Value> value = machine_value_from_xed(inst, xed_dec_op, state);

    if (!value) {
      auto &O = std::cerr;
      O << "TODO: COULD NOT REPRESENT INSTRUCTION OPERAND:\n";
      O << " iclass: " << inst.inst().iclass_str();
      O << " iform: "  << inst.inst().iform_str();
      O << " op name:" << xed_op.name_str();
      O << " op type:" << xed_op.type_str();
      if (xed_op.is_lookup_fn()) {
        O << " " << xed_op.get_lookup_fn_name();
      }
      O << "\n";
      abort();
    }

    Pipedream::Type type = Pipedream::OTHER;
    xed_operand_visibility_enum_t visibility = xed_dec_op.visibility();

    switch (value->kind()) {
      case Machine_Value::REG:
        switch (value->reg().value_or(XED_REG_INVALID)) {
          case XED_REG_FLAGS:
          case XED_REG_EFLAGS:
          case XED_REG_RFLAGS:
            std::cerr << "FLAGS/EFLAGS/RFLAGS in REG operand, should be FLAGS\n";
            abort();
            break;

          case XED_REG_X87STATUS:
          case XED_REG_X87TAG:
          case XED_REG_X87CONTROL:
            type         = Pipedream::I16;
            num_elements = 1; /// X87TAG: actually 8 2-bit tags
            bits         = 16;
            break;

           // case XED_REG_STACKPOP:
           // case XED_REG_STACKPUSH:

          case XED_REG_X87POP:
          case XED_REG_X87PUSH:
            type         = Pipedream::F80;
            num_elements = 1;
            bits         = 80;
            break;
          case XED_REG_X87POP2:
            type         = Pipedream::F80;
            num_elements = 2;
            bits         = 80;
            break;

          case XED_REG_ST0:
          case XED_REG_ST1:
          case XED_REG_ST2:
          case XED_REG_ST3:
          case XED_REG_ST4:
          case XED_REG_ST5:
          case XED_REG_ST6:
          case XED_REG_ST7:
            /// XED sometimes marks the second float stack operand implicit where it should be 'suppressed'
            switch (inst.inst().iclass()) {
              case XED_ICLASS_FCOM:
              case XED_ICLASS_FCOMP:
              case XED_ICLASS_FCOMI:
              case XED_ICLASS_FCOMIP:
              case XED_ICLASS_FCOMPP:
              case XED_ICLASS_FUCOM:
              case XED_ICLASS_FUCOMP:
              case XED_ICLASS_FUCOMI:
              case XED_ICLASS_FUCOMIP:
              case XED_ICLASS_FUCOMPP:
              case XED_ICLASS_FXCH:
              case XED_ICLASS_FLD:
                if (visibility != XED_OPVIS_EXPLICIT) {
                  visibility = XED_OPVIS_SUPPRESSED;
                }
                break;
              default:
                break;
            }
            break;

          case XED_REG_CR0:
            switch (state.cpu_mode.mode()) {
              case XED_MACHINE_MODE_LONG_64:
                /// FIXME: are there any other modes where CRO is 64 bits?
                type         = Pipedream::I64;
                num_elements = 1;
                bits         = 64;
                break;
              default:
                type         = Pipedream::I32;
                num_elements = 1;
                bits         = 32;
                break;
            }
            break;

          case XED_REG_GDTR:
          case XED_REG_LDTR:
          case XED_REG_IDTR:
          case XED_REG_TR:
            /// FIXME: introduce special machine types for these?
            type         = Pipedream::OTHER;
            num_elements = 1;
            break;

          case XED_REG_INVALID:
            /// Could be any register from register class, trust XED type info for operand.
          default:
            type = Pipedream::type(xed_xtype, xed_type, bits);
            break;
        }
        break;
      case Machine_Value::ADDR:
        assert(bits == 0);
        bits = xed_dec_op.memory_address_bits();
        type = Pipedream::type(xed_xtype, xed_type, bits);
        break;
      case Machine_Value::MEM:
        type = Pipedream::type(xed_xtype, xed_type, bits);
        assert(value->memory_bits() == (num_elements * bits));
        break;
      case Machine_Value::BRDISP:
        type = Pipedream::type(xed_xtype, xed_type, bits);
        break;
      case Machine_Value::IMM:
        type = Pipedream::type(xed_xtype, xed_type, bits);
        break;
      case Machine_Value::FLAGS:
        type         = Pipedream::OTHER;
        bits         = 0;
        num_elements = 0;
        break;
    }

    Pipedream_Operand_Template op;

    assert((type == Pipedream::OTHER) || bits == Pipedream::type_bits(type));

    op.type = type;
    op.name = xed_op.name();
    op.visibility    = visibility;
    op.action        = xed_dec_op.action();
    op.num_elements  = num_elements;
    op.value = *value;

    return op;
  }

  static Pipedream_Operand_Vector pipedream_operands_from_xed(
    const XED_Decoded_Instruction &xed_inst, XED_CPU_Mode cpu_mode, bool allow_rex
  ) {
    const Effective_Operand_Width effective_operand_width = xed_inst.effective_operand_width();
    const Effective_Address_Width effective_address_width = xed_inst.effective_address_width();

    const XED_Encoder_State state = {cpu_mode, effective_operand_width, effective_address_width, allow_rex};

    Pipedream_Operand_Vector operands;

    for (unsigned i = 0, end = xed_inst.num_operands(); i < end; i++) {
      const XED_Decoded_Instruction_Operand xed_op = xed_inst.operand(i);

      if (xed_op.static_op().is_memory_addressing_register()) {
        /// they are handled when the corresponding memory op gets handled
        continue;
      }

      auto op = pipedream_operand_from_xed(xed_inst, xed_op, state);

      bool ignore_op = false;

      switch (op.reg().value_or(XED_REG_INVALID)) {
        case XED_REG_STACKPOP:
        case XED_REG_STACKPUSH:
        case XED_REG_X87POP:
        case XED_REG_X87POP2:
        case XED_REG_X87PUSH:
          ignore_op = true;
          break;
        default:
          break;
      }

      if (ignore_op) { continue; }

      operands.push_back(op);
    }

    return operands;
  }

  /// Compute AT&T asm syntax instruction mnemonic from Pipedream_Instruction
  /// All fields except 'mnemonic' must already be filled in
  static std::string make_att_mnemonic(XED_Decoded_Instruction inst, const Pipedream_Operand_Vector &operands) {
    (void) operands;

    std::string att_asm = inst.asm_str(XED_SYNTAX_ATT);

    if (startswith(att_asm, "data16 ")) {
      att_asm.erase(0, std::strlen("data16 "));
    }

    std::string mnem;

    const size_t pos = att_asm.find(' ');
    if (pos != std::string::npos) {
      mnem = att_asm.substr(0, pos);
    } else {
      mnem = att_asm;
    }

    /// XED sometimes get's AT&T mnemonics wrong
    switch (inst.inst().iclass()) {
      case XED_ICLASS_MOVSXD:
        mnem = "movsxd";
        break;
      case XED_ICLASS_MOVSX:
        mnem = "movs";
        goto sign_extend_type_suffix;
      case XED_ICLASS_MOVZX:
        mnem = "movz";
        goto sign_extend_type_suffix;

      sign_extend_type_suffix:
      {
        assert(operands.size() == 2);
        assert(operands[0].value.kind() == Machine_Value::REG);

        /// add type suffix
        const char src = [&]() -> char {
          switch (inst.inst().iform()) {
            case XED_IFORM_MOVSX_GPRv_GPR8:  return 'b';
            case XED_IFORM_MOVSX_GPRv_GPR16: return 'w';
            case XED_IFORM_MOVSX_GPRv_MEMb:  return 'b';
            case XED_IFORM_MOVSX_GPRv_MEMw:  return 'w';

            case XED_IFORM_MOVZX_GPRv_GPR8:  return 'b';
            case XED_IFORM_MOVZX_GPRv_GPR16: return 'w';
            case XED_IFORM_MOVZX_GPRv_MEMb:  return 'b';
            case XED_IFORM_MOVZX_GPRv_MEMw:  return 'w';

            case XED_IFORM_MOVSXD_GPRv_GPRz:
            case XED_IFORM_MOVSXD_GPRv_MEMz:
              switch (operands[0].type) {
                case Pipedream::I16: return 'w';
                case Pipedream::I32: return 'l';
                case Pipedream::I64: return 'q';
                default: abort();
              }

            default: abort();
          }
        }();
        const char dst = [&]() -> char {
          switch (operands[0].type) {
            case Pipedream::I16: return 'w';
            case Pipedream::I32: return 'l';
            case Pipedream::I64: return 'q';
            default: abort();
          }
        }();

        mnem += src;
        mnem += dst;
        break;
      }

      case XED_ICLASS_PEXTRW_SSE4:
        /// TODO: stop using GAS and encode enstructions ourselves.
        assert(false && "XED_ICLASS_PEXTRW_SSE4 and XED_ICLASS_PEXTRW (SSE2) have the same mnemonic");
        mnem = "pextrw";
        break;

      /// In GAS x87 instructions use different size suffixes than normal intructions.
      /// XED of course does not honour this.
      case XED_ICLASS_FADD:
      case XED_ICLASS_FCOM:
      case XED_ICLASS_FDIV:
      case XED_ICLASS_FDIVR:
      case XED_ICLASS_FILD:
      case XED_ICLASS_FIST:
      case XED_ICLASS_FISTP:
      case XED_ICLASS_FISTTP:
      case XED_ICLASS_FLD:
      case XED_ICLASS_FST:
      case XED_ICLASS_FSTP:
      case XED_ICLASS_FSUB:
      {
        const char last = mnem.back();

        if (last == 'l') { mnem.back() = 's'; }
        if (last == 'q') { mnem.back() = 'l'; }
        break;
      }
      case XED_ICLASS_FMUL:
      {
        assert(mnem.size() >= 2);
        const size_t e = mnem.size() - 1;

        if ((mnem[e-1] == 'l') && (mnem[e] == 'l')) { mnem.back() = 's'; }
        if (mnem[e] == 'q') { mnem.back() = 'l'; }
        break;
      }
      case XED_ICLASS_FIADD:
      case XED_ICLASS_FICOM:
      case XED_ICLASS_FICOMP:
      case XED_ICLASS_FIDIV:
      case XED_ICLASS_FIDIVR:
      case XED_ICLASS_FIMUL:
      case XED_ICLASS_FISUB:
      case XED_ICLASS_FISUBR:
      {
        const char last = mnem.back();

        if (last == 'w') { mnem.back() = 's'; }
        if (last == 'l') { mnem.back() = 'l'; }
        break;
      }

      /// no type suffix allowed/necessary
      case XED_ICLASS_EXTRACTPS:
      case XED_ICLASS_INSERTPS:
      case XED_ICLASS_LAR:
      case XED_ICLASS_LSL:
      case XED_ICLASS_MOVD:
      case XED_ICLASS_MOVDDUP:
      case XED_ICLASS_MOVDIRI:
      case XED_ICLASS_MOVHPD:
      case XED_ICLASS_MOVHPS:
      case XED_ICLASS_MOVLPD:
      case XED_ICLASS_MOVLPS:
      case XED_ICLASS_MOVNTPD:
      case XED_ICLASS_MOVNTPS:
      case XED_ICLASS_MOVNTQ:
      case XED_ICLASS_MOVNTSD:
      case XED_ICLASS_MOVNTSS:
      case XED_ICLASS_MOVQ:
      case XED_ICLASS_PABSB:
      case XED_ICLASS_PABSD:
      case XED_ICLASS_PABSW:
      case XED_ICLASS_PADDB:
      case XED_ICLASS_PADDD:
      case XED_ICLASS_PADDQ:
      case XED_ICLASS_PADDSB:
      case XED_ICLASS_PADDSW:
      case XED_ICLASS_PADDUSB:
      case XED_ICLASS_PADDUSW:
      case XED_ICLASS_PADDW:
      case XED_ICLASS_PALIGNR:
      case XED_ICLASS_PAND:
      case XED_ICLASS_PANDN:
      case XED_ICLASS_PAVGB:
      case XED_ICLASS_PAVGW:
      case XED_ICLASS_PEXTRB:
      case XED_ICLASS_PEXTRD:
      case XED_ICLASS_PEXTRQ:
      case XED_ICLASS_PEXTRW:
      case XED_ICLASS_PF2ID:
      case XED_ICLASS_PF2IW:
      case XED_ICLASS_PFACC:
      case XED_ICLASS_PFADD:
      case XED_ICLASS_PFCMPEQ:
      case XED_ICLASS_PFCMPGE:
      case XED_ICLASS_PFCMPGT:
      case XED_ICLASS_PHADDD:
      case XED_ICLASS_PHADDSW:
      case XED_ICLASS_PHADDW:
      case XED_ICLASS_PHMINPOSUW:
      case XED_ICLASS_PHSUBD:
      case XED_ICLASS_PHSUBSW:
      case XED_ICLASS_PHSUBW:
      case XED_ICLASS_PINSRB:
      case XED_ICLASS_PINSRD:
      case XED_ICLASS_PINSRQ:
      case XED_ICLASS_PINSRW:
      case XED_ICLASS_PMADDUBSW:
      case XED_ICLASS_PMADDWD:
      case XED_ICLASS_PMAXSB:
      case XED_ICLASS_PMAXSD:
      case XED_ICLASS_PMAXSW:
      case XED_ICLASS_PMAXUB:
      case XED_ICLASS_PMAXUD:
      case XED_ICLASS_PMAXUW:
      case XED_ICLASS_PMINSB:
      case XED_ICLASS_PMINSD:
      case XED_ICLASS_PMINSW:
      case XED_ICLASS_PMINUB:
      case XED_ICLASS_PMINUD:
      case XED_ICLASS_PMINUW:
      case XED_ICLASS_PMULDQ:
      case XED_ICLASS_PMULHRSW:
      case XED_ICLASS_PMULHRW:
      case XED_ICLASS_PMULHUW:
      case XED_ICLASS_PMULHW:
      case XED_ICLASS_PMULLD:
      case XED_ICLASS_PMULLW:
      case XED_ICLASS_PMULUDQ:
      case XED_ICLASS_POR:
      case XED_ICLASS_PREFETCH_EXCLUSIVE:
      case XED_ICLASS_PREFETCHNTA:
      case XED_ICLASS_PREFETCHT0:
      case XED_ICLASS_PREFETCHT1:
      case XED_ICLASS_PREFETCHT2:
      case XED_ICLASS_PREFETCHW:
      case XED_ICLASS_PREFETCHWT1:
      case XED_ICLASS_PSADBW:
      case XED_ICLASS_PSHUFB:
      case XED_ICLASS_PSHUFD:
      case XED_ICLASS_PSHUFHW:
      case XED_ICLASS_PSHUFLW:
      case XED_ICLASS_PSHUFW:
      case XED_ICLASS_PSIGNB:
      case XED_ICLASS_PSIGND:
      case XED_ICLASS_PSIGNW:
      case XED_ICLASS_PSLLD:
      case XED_ICLASS_PSLLDQ:
      case XED_ICLASS_PSLLQ:
      case XED_ICLASS_PSLLW:
      case XED_ICLASS_PSRAD:
      case XED_ICLASS_PSRAW:
      case XED_ICLASS_PSRLD:
      case XED_ICLASS_PSRLQ:
      case XED_ICLASS_PSRLW:
      case XED_ICLASS_PSUBB:
      case XED_ICLASS_PSUBD:
      case XED_ICLASS_PSUBQ:
      case XED_ICLASS_PSUBSB:
      case XED_ICLASS_PSUBSW:
      case XED_ICLASS_PSUBUSB:
      case XED_ICLASS_PSUBUSW:
      case XED_ICLASS_PSUBW:
      case XED_ICLASS_PUNPCKHBW:
      case XED_ICLASS_PUNPCKHDQ:
      case XED_ICLASS_PUNPCKHQDQ:
      case XED_ICLASS_PUNPCKHWD:
      case XED_ICLASS_PUNPCKLBW:
      case XED_ICLASS_PUNPCKLDQ:
      case XED_ICLASS_PUNPCKLQDQ:
      case XED_ICLASS_PUNPCKLWD:
      case XED_ICLASS_PXOR:
      case XED_ICLASS_ROUNDPD:
      case XED_ICLASS_ROUNDPS:
      case XED_ICLASS_ROUNDSD:
      case XED_ICLASS_ROUNDSS:
      case XED_ICLASS_VEXTRACTPS:
      case XED_ICLASS_VMOVD:
      case XED_ICLASS_VMOVDDUP:
      case XED_ICLASS_VMOVQ:
      case XED_ICLASS_VPBROADCASTB:
      case XED_ICLASS_VPBROADCASTD:
      case XED_ICLASS_VPBROADCASTQ:
      case XED_ICLASS_VPBROADCASTW:
      case XED_ICLASS_VPEXTRB:
      case XED_ICLASS_VPEXTRD:
      case XED_ICLASS_VPEXTRQ:
      case XED_ICLASS_VPEXTRW:
      case XED_ICLASS_VPHADDBD:
      case XED_ICLASS_VPHADDBQ:
      case XED_ICLASS_VPHADDBW:
      case XED_ICLASS_VPHADDD:
      case XED_ICLASS_VPHADDDQ:
      case XED_ICLASS_VPHADDSW:
      case XED_ICLASS_VPHADDUBD:
      case XED_ICLASS_VPHADDUBQ:
      case XED_ICLASS_VPHADDUBW:
      case XED_ICLASS_VPHADDUDQ:
      case XED_ICLASS_VPHADDUWD:
      case XED_ICLASS_VPHADDUWQ:
      case XED_ICLASS_VPHADDW:
      case XED_ICLASS_VPHADDWD:
      case XED_ICLASS_VPHADDWQ:
      case XED_ICLASS_VPHSUBBW:
      case XED_ICLASS_VPHSUBD:
      case XED_ICLASS_VPHSUBDQ:
      case XED_ICLASS_VPHSUBSW:
      case XED_ICLASS_VPHSUBW:
      case XED_ICLASS_VPHSUBWD:
      case XED_ICLASS_VPINSRB:
      case XED_ICLASS_VPMAXSB:
      case XED_ICLASS_VPMAXSD:
      case XED_ICLASS_VPMAXSW:
      case XED_ICLASS_VPMAXUB:
      case XED_ICLASS_VPMAXUD:
      case XED_ICLASS_VPMAXUW:
      case XED_ICLASS_VPMINSB:
      case XED_ICLASS_VPMINSD:
      case XED_ICLASS_VPMINSW:
      case XED_ICLASS_VPMINUB:
      case XED_ICLASS_VPMINUD:
      case XED_ICLASS_VPMINUW:
      case XED_ICLASS_VPINSRD:
      case XED_ICLASS_VPINSRQ:
      case XED_ICLASS_VPINSRW:
        mnem = make_intel_mnemonic(inst);
        break;

      /// XED often produces wrong or invalid type suffixes, fix them.
      case XED_ICLASS_ADDPD:
      case XED_ICLASS_ADDPS:
      case XED_ICLASS_ADDSD:
      case XED_ICLASS_ADDSS:
      case XED_ICLASS_ADDSUBPD:
      case XED_ICLASS_ADDSUBPS:
      case XED_ICLASS_AESDEC:
      case XED_ICLASS_AESDECLAST:
      case XED_ICLASS_AESENC:
      case XED_ICLASS_AESENCLAST:
      case XED_ICLASS_AESIMC:
      case XED_ICLASS_AESKEYGENASSIST:
      case XED_ICLASS_ANDNPD:
      case XED_ICLASS_ANDNPS:
      case XED_ICLASS_ANDPD:
      case XED_ICLASS_ANDPS:
      case XED_ICLASS_BLENDPD:
      case XED_ICLASS_BLENDPS:
      case XED_ICLASS_BLENDVPD:
      case XED_ICLASS_BLENDVPS:
      case XED_ICLASS_CLFLUSH:
      case XED_ICLASS_CLFLUSHOPT:
      case XED_ICLASS_CLWB:
      case XED_ICLASS_CMPPD:
      case XED_ICLASS_CMPPS:
      case XED_ICLASS_CMPSB:
      case XED_ICLASS_CMPSD:
      case XED_ICLASS_CMPSD_XMM:
      case XED_ICLASS_CMPSQ:
      case XED_ICLASS_CMPSS:
      case XED_ICLASS_CMPSW:
      case XED_ICLASS_CMPXCHG16B:
      case XED_ICLASS_COMISD:
      case XED_ICLASS_COMISS:
      case XED_ICLASS_CVTDQ2PD:
      case XED_ICLASS_CVTDQ2PS:
      case XED_ICLASS_CVTPD2DQ:
      case XED_ICLASS_CVTPD2PI:
      case XED_ICLASS_CVTPD2PS:
      case XED_ICLASS_CVTPI2PD:
      case XED_ICLASS_CVTPI2PS:
      case XED_ICLASS_CVTPS2DQ:
      case XED_ICLASS_CVTPS2PD:
      case XED_ICLASS_CVTPS2PI:
      case XED_ICLASS_CVTSD2SI:
      case XED_ICLASS_CVTSD2SS:
      case XED_ICLASS_CVTSS2SD:
      case XED_ICLASS_CVTSS2SI:
      case XED_ICLASS_CVTTPD2DQ:
      case XED_ICLASS_CVTTPD2PI:
      case XED_ICLASS_CVTTPS2DQ:
      case XED_ICLASS_CVTTPS2PI:
      case XED_ICLASS_CVTTSD2SI:
      case XED_ICLASS_CVTTSS2SI:
      case XED_ICLASS_DIVPD:
      case XED_ICLASS_DIVPS:
      case XED_ICLASS_DIVSD:
      case XED_ICLASS_DIVSS:
      case XED_ICLASS_DPPD:
      case XED_ICLASS_DPPS:
      case XED_ICLASS_HADDPD:
      case XED_ICLASS_HADDPS:
      case XED_ICLASS_HSUBPD:
      case XED_ICLASS_HSUBPS:
      case XED_ICLASS_INSB:
      case XED_ICLASS_INSD:
      case XED_ICLASS_INSW:
      case XED_ICLASS_LDDQU:
      case XED_ICLASS_LDMXCSR:
      case XED_ICLASS_MASKMOVDQU:
      case XED_ICLASS_MASKMOVQ:
      case XED_ICLASS_MAXPD:
      case XED_ICLASS_MAXPS:
      case XED_ICLASS_MAXSD:
      case XED_ICLASS_MAXSS:
      case XED_ICLASS_MINPD:
      case XED_ICLASS_MINPS:
      case XED_ICLASS_MINSD:
      case XED_ICLASS_MINSS:
      case XED_ICLASS_MOVAPD:
      case XED_ICLASS_MOVAPS:
      case XED_ICLASS_MOVDQA:
      case XED_ICLASS_MOVDQU:
      case XED_ICLASS_MOVNTDQ:
      case XED_ICLASS_MOVNTDQA:
      case XED_ICLASS_MOVNTI:
      case XED_ICLASS_MOVSB:
      case XED_ICLASS_MOVSD:
      case XED_ICLASS_MOVSD_XMM:
      case XED_ICLASS_MOVSHDUP:
      case XED_ICLASS_MOVSLDUP:
      case XED_ICLASS_MOVSQ:
      case XED_ICLASS_MOVSS:
      case XED_ICLASS_MOVSW:
      case XED_ICLASS_MOVUPD:
      case XED_ICLASS_MOVUPS:
      case XED_ICLASS_MPSADBW:
      case XED_ICLASS_MULPD:
      case XED_ICLASS_MULPS:
      case XED_ICLASS_MULSD:
      case XED_ICLASS_MULSS:
      case XED_ICLASS_ORPD:
      case XED_ICLASS_ORPS:
      case XED_ICLASS_PACKSSDW:
      case XED_ICLASS_PACKSSWB:
      case XED_ICLASS_PACKUSDW:
      case XED_ICLASS_PACKUSWB:
      case XED_ICLASS_PBLENDVB:
      case XED_ICLASS_PBLENDW:
      case XED_ICLASS_PCLMULQDQ:
      case XED_ICLASS_PCMPEQB:
      case XED_ICLASS_PCMPEQD:
      case XED_ICLASS_PCMPEQQ:
      case XED_ICLASS_PCMPEQW:
      case XED_ICLASS_PCMPESTRI64:
      case XED_ICLASS_PCMPESTRI:
      case XED_ICLASS_PCMPESTRM64:
      case XED_ICLASS_PCMPESTRM:
      case XED_ICLASS_PCMPGTB:
      case XED_ICLASS_PCMPGTD:
      case XED_ICLASS_PCMPGTQ:
      case XED_ICLASS_PCMPGTW:
      case XED_ICLASS_PCMPISTRI64:
      case XED_ICLASS_PCMPISTRI:
      case XED_ICLASS_PCMPISTRM:
      case XED_ICLASS_PMOVSXBD:
      case XED_ICLASS_PMOVSXBQ:
      case XED_ICLASS_PMOVSXBW:
      case XED_ICLASS_PMOVSXDQ:
      case XED_ICLASS_PMOVSXWD:
      case XED_ICLASS_PMOVSXWQ:
      case XED_ICLASS_PMOVZXBD:
      case XED_ICLASS_PMOVZXBQ:
      case XED_ICLASS_PMOVZXBW:
      case XED_ICLASS_PMOVZXDQ:
      case XED_ICLASS_PMOVZXWD:
      case XED_ICLASS_PMOVZXWQ:
      case XED_ICLASS_PTEST:
      case XED_ICLASS_RCPPS:
      case XED_ICLASS_RCPSS:
      case XED_ICLASS_RSQRTPS:
      case XED_ICLASS_RSQRTSS:
      case XED_ICLASS_SCASB:
      case XED_ICLASS_SCASD:
      case XED_ICLASS_SCASQ:
      case XED_ICLASS_SCASW:
      case XED_ICLASS_SHUFPD:
      case XED_ICLASS_SHUFPS:
      case XED_ICLASS_SQRTPD:
      case XED_ICLASS_SQRTPS:
      case XED_ICLASS_SQRTSD:
      case XED_ICLASS_SQRTSS:
      case XED_ICLASS_STMXCSR:
      case XED_ICLASS_STOSB:
      case XED_ICLASS_STOSD:
      case XED_ICLASS_STOSQ:
      case XED_ICLASS_STOSW:
      case XED_ICLASS_SUBPD:
      case XED_ICLASS_SUBPS:
      case XED_ICLASS_SUBSD:
      case XED_ICLASS_SUBSS:
      case XED_ICLASS_UCOMISD:
      case XED_ICLASS_UCOMISS:
      case XED_ICLASS_UNPCKHPD:
      case XED_ICLASS_UNPCKHPS:
      case XED_ICLASS_UNPCKLPD:
      case XED_ICLASS_UNPCKLPS:
      case XED_ICLASS_VADDPD:
      case XED_ICLASS_VADDPS:
      case XED_ICLASS_VADDSD:
      case XED_ICLASS_VADDSS:
      case XED_ICLASS_VADDSUBPD:
      case XED_ICLASS_VADDSUBPS:
      case XED_ICLASS_VAESDEC:
      case XED_ICLASS_VAESDECLAST:
      case XED_ICLASS_VAESENC:
      case XED_ICLASS_VAESENCLAST:
      case XED_ICLASS_VAESIMC:
      case XED_ICLASS_VAESKEYGENASSIST:
      case XED_ICLASS_VANDNPD:
      case XED_ICLASS_VANDNPS:
      case XED_ICLASS_VANDPD:
      case XED_ICLASS_VANDPS:
      case XED_ICLASS_VBLENDMPD:
      case XED_ICLASS_VBLENDMPS:
      case XED_ICLASS_VBLENDPD:
      case XED_ICLASS_VBLENDPS:
      case XED_ICLASS_VBLENDVPD:
      case XED_ICLASS_VBLENDVPS:
      case XED_ICLASS_VBROADCASTF128:
      case XED_ICLASS_VBROADCASTI128:
      case XED_ICLASS_VBROADCASTSD:
      case XED_ICLASS_VBROADCASTSS:
      case XED_ICLASS_VCMPPD:
      case XED_ICLASS_VCMPPS:
      case XED_ICLASS_VCMPSD:
      case XED_ICLASS_VCMPSS:
      case XED_ICLASS_VCOMISD:
      case XED_ICLASS_VCOMISS:
      case XED_ICLASS_VCVTDQ2PD:
      case XED_ICLASS_VCVTDQ2PS:
      case XED_ICLASS_VCVTPD2DQ:
      case XED_ICLASS_VCVTPD2PS:
      case XED_ICLASS_VCVTPD2QQ:
      case XED_ICLASS_VCVTPD2UDQ:
      case XED_ICLASS_VCVTPD2UQQ:
      case XED_ICLASS_VCVTPH2PS:
      case XED_ICLASS_VCVTPS2DQ:
      case XED_ICLASS_VCVTPS2PD:
      case XED_ICLASS_VCVTPS2PH:
      case XED_ICLASS_VCVTPS2QQ:
      case XED_ICLASS_VCVTPS2UDQ:
      case XED_ICLASS_VCVTPS2UQQ:
      case XED_ICLASS_VCVTQQ2PD:
      case XED_ICLASS_VCVTQQ2PS:
      case XED_ICLASS_VCVTSD2SI:
      case XED_ICLASS_VCVTSD2SS:
      case XED_ICLASS_VCVTSD2USI:
      case XED_ICLASS_VCVTSI2SD:
      case XED_ICLASS_VCVTSI2SS:
      case XED_ICLASS_VCVTSS2SD:
      case XED_ICLASS_VCVTSS2SI:
      case XED_ICLASS_VCVTSS2USI:
      case XED_ICLASS_VCVTTPD2DQ:
      case XED_ICLASS_VCVTTPD2QQ:
      case XED_ICLASS_VCVTTPD2UDQ:
      case XED_ICLASS_VCVTTPD2UQQ:
      case XED_ICLASS_VCVTTPS2DQ:
      case XED_ICLASS_VCVTTPS2QQ:
      case XED_ICLASS_VCVTTPS2UDQ:
      case XED_ICLASS_VCVTTPS2UQQ:
      case XED_ICLASS_VCVTTSD2SI:
      case XED_ICLASS_VCVTTSD2USI:
      case XED_ICLASS_VCVTTSS2SI:
      case XED_ICLASS_VCVTTSS2USI:
      case XED_ICLASS_VCVTUDQ2PD:
      case XED_ICLASS_VCVTUDQ2PS:
      case XED_ICLASS_VCVTUQQ2PD:
      case XED_ICLASS_VCVTUQQ2PS:
      case XED_ICLASS_VCVTUSI2SD:
      case XED_ICLASS_VCVTUSI2SS:
      case XED_ICLASS_VDIVPD:
      case XED_ICLASS_VDIVPS:
      case XED_ICLASS_VDIVSD:
      case XED_ICLASS_VDIVSS:
      case XED_ICLASS_VDPPD:
      case XED_ICLASS_VDPPS:
      case XED_ICLASS_VEXTRACTF128:
      case XED_ICLASS_VEXTRACTF32X4:
      case XED_ICLASS_VEXTRACTF32X8:
      case XED_ICLASS_VEXTRACTF64X2:
      case XED_ICLASS_VEXTRACTF64X4:
      case XED_ICLASS_VEXTRACTI128:
      case XED_ICLASS_VEXTRACTI32X4:
      case XED_ICLASS_VEXTRACTI32X8:
      case XED_ICLASS_VEXTRACTI64X2:
      case XED_ICLASS_VEXTRACTI64X4:
      case XED_ICLASS_VFMADD132PD:
      case XED_ICLASS_VFMADD132PS:
      case XED_ICLASS_VFMADD132SD:
      case XED_ICLASS_VFMADD132SS:
      case XED_ICLASS_VFMADD213PD:
      case XED_ICLASS_VFMADD213PS:
      case XED_ICLASS_VFMADD213SD:
      case XED_ICLASS_VFMADD213SS:
      case XED_ICLASS_VFMADD231PD:
      case XED_ICLASS_VFMADD231PS:
      case XED_ICLASS_VFMADD231SD:
      case XED_ICLASS_VFMADD231SS:
      case XED_ICLASS_VFMADDPD:
      case XED_ICLASS_VFMADDPS:
      case XED_ICLASS_VFMADDSD:
      case XED_ICLASS_VFMADDSS:
      case XED_ICLASS_VFMADDSUB132PD:
      case XED_ICLASS_VFMADDSUB132PS:
      case XED_ICLASS_VFMADDSUB213PD:
      case XED_ICLASS_VFMADDSUB213PS:
      case XED_ICLASS_VFMADDSUB231PD:
      case XED_ICLASS_VFMADDSUB231PS:
      case XED_ICLASS_VFMADDSUBPD:
      case XED_ICLASS_VFMADDSUBPS:
      case XED_ICLASS_VFMSUB132PD:
      case XED_ICLASS_VFMSUB132PS:
      case XED_ICLASS_VFMSUB132SD:
      case XED_ICLASS_VFMSUB132SS:
      case XED_ICLASS_VFMSUB213PD:
      case XED_ICLASS_VFMSUB213PS:
      case XED_ICLASS_VFMSUB213SD:
      case XED_ICLASS_VFMSUB213SS:
      case XED_ICLASS_VFMSUB231PD:
      case XED_ICLASS_VFMSUB231PS:
      case XED_ICLASS_VFMSUB231SD:
      case XED_ICLASS_VFMSUB231SS:
      case XED_ICLASS_VFMSUBADD132PD:
      case XED_ICLASS_VFMSUBADD132PS:
      case XED_ICLASS_VFMSUBADD213PD:
      case XED_ICLASS_VFMSUBADD213PS:
      case XED_ICLASS_VFMSUBADD231PD:
      case XED_ICLASS_VFMSUBADD231PS:
      case XED_ICLASS_VFMSUBADDPD:
      case XED_ICLASS_VFMSUBADDPS:
      case XED_ICLASS_VFMSUBPD:
      case XED_ICLASS_VFMSUBPS:
      case XED_ICLASS_VFMSUBSD:
      case XED_ICLASS_VFMSUBSS:
      case XED_ICLASS_VFNMADD132PD:
      case XED_ICLASS_VFNMADD132PS:
      case XED_ICLASS_VFNMADD132SD:
      case XED_ICLASS_VFNMADD132SS:
      case XED_ICLASS_VFNMADD213PD:
      case XED_ICLASS_VFNMADD213PS:
      case XED_ICLASS_VFNMADD213SD:
      case XED_ICLASS_VFNMADD213SS:
      case XED_ICLASS_VFNMADD231PD:
      case XED_ICLASS_VFNMADD231PS:
      case XED_ICLASS_VFNMADD231SD:
      case XED_ICLASS_VFNMADD231SS:
      case XED_ICLASS_VFNMADDPD:
      case XED_ICLASS_VFNMADDPS:
      case XED_ICLASS_VFNMADDSD:
      case XED_ICLASS_VFNMADDSS:
      case XED_ICLASS_VFNMSUB132PD:
      case XED_ICLASS_VFNMSUB132PS:
      case XED_ICLASS_VFNMSUB132SD:
      case XED_ICLASS_VFNMSUB132SS:
      case XED_ICLASS_VFNMSUB213PD:
      case XED_ICLASS_VFNMSUB213PS:
      case XED_ICLASS_VFNMSUB213SD:
      case XED_ICLASS_VFNMSUB213SS:
      case XED_ICLASS_VFNMSUB231PD:
      case XED_ICLASS_VFNMSUB231PS:
      case XED_ICLASS_VFNMSUB231SD:
      case XED_ICLASS_VFNMSUB231SS:
      case XED_ICLASS_VFNMSUBPD:
      case XED_ICLASS_VFNMSUBPS:
      case XED_ICLASS_VFNMSUBSD:
      case XED_ICLASS_VFNMSUBSS:
      case XED_ICLASS_VFRCZPD:
      case XED_ICLASS_VFRCZPS:
      case XED_ICLASS_VHADDPD:
      case XED_ICLASS_VHADDPS:
      case XED_ICLASS_VHSUBPD:
      case XED_ICLASS_VHSUBPS:
      case XED_ICLASS_VINSERTF128:
      case XED_ICLASS_VINSERTF32X4:
      case XED_ICLASS_VINSERTF32X8:
      case XED_ICLASS_VINSERTF64X2:
      case XED_ICLASS_VINSERTF64X4:
      case XED_ICLASS_VINSERTI128:
      case XED_ICLASS_VINSERTI32X4:
      case XED_ICLASS_VINSERTI32X8:
      case XED_ICLASS_VINSERTI64X2:
      case XED_ICLASS_VINSERTI64X4:
      case XED_ICLASS_VINSERTPS:
      case XED_ICLASS_VLDDQU:
      case XED_ICLASS_VLDMXCSR:
      case XED_ICLASS_VMASKMOVDQU:
      case XED_ICLASS_VMASKMOVPD:
      case XED_ICLASS_VMASKMOVPS:
      case XED_ICLASS_VMAXPD:
      case XED_ICLASS_VMAXPS:
      case XED_ICLASS_VMAXSD:
      case XED_ICLASS_VMAXSS:
      case XED_ICLASS_VMINPD:
      case XED_ICLASS_VMINPS:
      case XED_ICLASS_VMINSD:
      case XED_ICLASS_VMINSS:
      case XED_ICLASS_VMOVAPD:
      case XED_ICLASS_VMOVAPS:
      case XED_ICLASS_VMOVDQA32:
      case XED_ICLASS_VMOVDQA64:
      case XED_ICLASS_VMOVDQA:
      case XED_ICLASS_VMOVDQU16:
      case XED_ICLASS_VMOVDQU32:
      case XED_ICLASS_VMOVDQU64:
      case XED_ICLASS_VMOVDQU8:
      case XED_ICLASS_VMOVDQU:
      case XED_ICLASS_VMOVHLPS:
      case XED_ICLASS_VMOVHPD:
      case XED_ICLASS_VMOVHPS:
      case XED_ICLASS_VMOVLHPS:
      case XED_ICLASS_VMOVLPD:
      case XED_ICLASS_VMOVLPS:
      case XED_ICLASS_VMOVMSKPD:
      case XED_ICLASS_VMOVMSKPS:
      case XED_ICLASS_VMOVNTDQ:
      case XED_ICLASS_VMOVNTDQA:
      case XED_ICLASS_VMOVNTPD:
      case XED_ICLASS_VMOVNTPS:
      case XED_ICLASS_VMOVSD:
      case XED_ICLASS_VMOVSHDUP:
      case XED_ICLASS_VMOVSLDUP:
      case XED_ICLASS_VMOVSS:
      case XED_ICLASS_VMOVUPD:
      case XED_ICLASS_VMOVUPS:
      case XED_ICLASS_VMPSADBW:
      case XED_ICLASS_VMULPD:
      case XED_ICLASS_VMULPS:
      case XED_ICLASS_VMULSD:
      case XED_ICLASS_VMULSS:
      case XED_ICLASS_VORPD:
      case XED_ICLASS_VORPS:
      case XED_ICLASS_VPABSB:
      case XED_ICLASS_VPABSD:
      case XED_ICLASS_VPABSQ:
      case XED_ICLASS_VPABSW:
      case XED_ICLASS_VPACKSSDW:
      case XED_ICLASS_VPACKSSWB:
      case XED_ICLASS_VPACKUSDW:
      case XED_ICLASS_VPACKUSWB:
      case XED_ICLASS_VPADDB:
      case XED_ICLASS_VPADDD:
      case XED_ICLASS_VPADDQ:
      case XED_ICLASS_VPADDSB:
      case XED_ICLASS_VPADDSW:
      case XED_ICLASS_VPADDUSB:
      case XED_ICLASS_VPADDUSW:
      case XED_ICLASS_VPADDW:
      case XED_ICLASS_VPALIGNR:
      case XED_ICLASS_VPAND:
      case XED_ICLASS_VPANDN:
      case XED_ICLASS_VPAVGB:
      case XED_ICLASS_VPAVGW:
      case XED_ICLASS_VPBLENDD:
      case XED_ICLASS_VPBLENDVB:
      case XED_ICLASS_VPBLENDW:
      case XED_ICLASS_VPCLMULQDQ:
      case XED_ICLASS_VPCMPB:
      case XED_ICLASS_VPCMPD:
      case XED_ICLASS_VPCMPEQB:
      case XED_ICLASS_VPCMPEQD:
      case XED_ICLASS_VPCMPEQQ:
      case XED_ICLASS_VPCMPEQW:
      case XED_ICLASS_VPCMPESTRI64:
      case XED_ICLASS_VPCMPESTRI:
      case XED_ICLASS_VPCMPESTRM64:
      case XED_ICLASS_VPCMPESTRM:
      case XED_ICLASS_VPCMPGTB:
      case XED_ICLASS_VPCMPGTD:
      case XED_ICLASS_VPCMPGTQ:
      case XED_ICLASS_VPCMPGTW:
      case XED_ICLASS_VPCMPISTRI64:
      case XED_ICLASS_VPCMPISTRI:
      case XED_ICLASS_VPCMPISTRM:
      case XED_ICLASS_VPCMPQ:
      case XED_ICLASS_VPCMPUB:
      case XED_ICLASS_VPCMPUD:
      case XED_ICLASS_VPCMPUQ:
      case XED_ICLASS_VPCMPUW:
      case XED_ICLASS_VPCMPW:
      case XED_ICLASS_VPERM2F128:
      case XED_ICLASS_VPERM2I128:
      case XED_ICLASS_VPERMD:
      case XED_ICLASS_VPERMILPD:
      case XED_ICLASS_VPERMILPS:
      case XED_ICLASS_VPERMPD:
      case XED_ICLASS_VPERMPS:
      case XED_ICLASS_VPERMQ:
      case XED_ICLASS_VPHMINPOSUW:
      case XED_ICLASS_VPMADD52HUQ:
      case XED_ICLASS_VPMADD52LUQ:
      case XED_ICLASS_VPMADDUBSW:
      case XED_ICLASS_VPMADDWD:
      case XED_ICLASS_VPMASKMOVD:
      case XED_ICLASS_VPMASKMOVQ:
      case XED_ICLASS_VPMOVSDB:
      case XED_ICLASS_VPMOVSDW:
      case XED_ICLASS_VPMOVSQB:
      case XED_ICLASS_VPMOVSQD:
      case XED_ICLASS_VPMOVSQW:
      case XED_ICLASS_VPMOVSWB:
      case XED_ICLASS_VPMOVSXBD:
      case XED_ICLASS_VPMOVSXBQ:
      case XED_ICLASS_VPMOVSXBW:
      case XED_ICLASS_VPMOVSXDQ:
      case XED_ICLASS_VPMOVSXWD:
      case XED_ICLASS_VPMOVSXWQ:
      case XED_ICLASS_VPMOVZXBD:
      case XED_ICLASS_VPMOVZXBQ:
      case XED_ICLASS_VPMOVZXBW:
      case XED_ICLASS_VPMOVZXDQ:
      case XED_ICLASS_VPMOVZXWD:
      case XED_ICLASS_VPMOVZXWQ:
      case XED_ICLASS_VPMULDQ:
      case XED_ICLASS_VPMULHRSW:
      case XED_ICLASS_VPMULHUW:
      case XED_ICLASS_VPMULHW:
      case XED_ICLASS_VPMULLD:
      case XED_ICLASS_VPMULLW:
      case XED_ICLASS_VPMULUDQ:
      case XED_ICLASS_VPOR:
      case XED_ICLASS_VPORD:
      case XED_ICLASS_VPORQ:
      case XED_ICLASS_VPSADBW:
      case XED_ICLASS_VPSHUFB:
      case XED_ICLASS_VPSHUFD:
      case XED_ICLASS_VPSHUFHW:
      case XED_ICLASS_VPSHUFLW:
      case XED_ICLASS_VPSIGNB:
      case XED_ICLASS_VPSIGND:
      case XED_ICLASS_VPSIGNW:
      case XED_ICLASS_VPSLLD:
      case XED_ICLASS_VPSLLDQ:
      case XED_ICLASS_VPSLLQ:
      case XED_ICLASS_VPSLLVD:
      case XED_ICLASS_VPSLLVQ:
      case XED_ICLASS_VPSLLVW:
      case XED_ICLASS_VPSLLW:
      case XED_ICLASS_VPSRAD:
      case XED_ICLASS_VPSRAVD:
      case XED_ICLASS_VPSRAW:
      case XED_ICLASS_VPSRLD:
      case XED_ICLASS_VPSRLQ:
      case XED_ICLASS_VPSRLVD:
      case XED_ICLASS_VPSRLVQ:
      case XED_ICLASS_VPSRLVW:
      case XED_ICLASS_VPSRLW:
      case XED_ICLASS_VPSUBB:
      case XED_ICLASS_VPSUBD:
      case XED_ICLASS_VPSUBQ:
      case XED_ICLASS_VPSUBSB:
      case XED_ICLASS_VPSUBSW:
      case XED_ICLASS_VPSUBUSB:
      case XED_ICLASS_VPSUBUSW:
      case XED_ICLASS_VPSUBW:
      case XED_ICLASS_VPTEST:
      case XED_ICLASS_VPUNPCKHBW:
      case XED_ICLASS_VPUNPCKHDQ:
      case XED_ICLASS_VPUNPCKHQDQ:
      case XED_ICLASS_VPUNPCKHWD:
      case XED_ICLASS_VPUNPCKLBW:
      case XED_ICLASS_VPUNPCKLDQ:
      case XED_ICLASS_VPUNPCKLQDQ:
      case XED_ICLASS_VPUNPCKLWD:
      case XED_ICLASS_VPXOR:
      case XED_ICLASS_VRCPPS:
      case XED_ICLASS_VRCPSS:
      case XED_ICLASS_VROUNDPD:
      case XED_ICLASS_VROUNDPS:
      case XED_ICLASS_VROUNDSD:
      case XED_ICLASS_VROUNDSS:
      case XED_ICLASS_VRSQRT14PD:
      case XED_ICLASS_VRSQRT14PS:
      case XED_ICLASS_VRSQRT14SD:
      case XED_ICLASS_VRSQRT14SS:
      case XED_ICLASS_VRSQRT28PD:
      case XED_ICLASS_VRSQRT28PS:
      case XED_ICLASS_VRSQRT28SD:
      case XED_ICLASS_VRSQRT28SS:
      case XED_ICLASS_VRSQRTPS:
      case XED_ICLASS_VRSQRTSS:
      case XED_ICLASS_VSHUFPD:
      case XED_ICLASS_VSHUFPS:
      case XED_ICLASS_VSQRTPD:
      case XED_ICLASS_VSQRTPS:
      case XED_ICLASS_VSQRTSD:
      case XED_ICLASS_VSQRTSS:
      case XED_ICLASS_VSTMXCSR:
      case XED_ICLASS_VSUBPD:
      case XED_ICLASS_VSUBPS:
      case XED_ICLASS_VSUBSD:
      case XED_ICLASS_VSUBSS:
      case XED_ICLASS_VTESTPD:
      case XED_ICLASS_VTESTPS:
      case XED_ICLASS_VUCOMISD:
      case XED_ICLASS_VUCOMISS:
      case XED_ICLASS_VUNPCKHPD:
      case XED_ICLASS_VUNPCKHPS:
      case XED_ICLASS_VUNPCKLPD:
      case XED_ICLASS_VUNPCKLPS:
      case XED_ICLASS_VXORPD:
      case XED_ICLASS_VXORPS:
      case XED_ICLASS_XORPD:
      case XED_ICLASS_XORPS:
      case XED_ICLASS_XRESLDTRK:
      case XED_ICLASS_XSTORE:
      case XED_ICLASS_XSUSLDTRK:
      {
        /// remove trailing type suffix ('l', 'q', 'x', 'y', 'z')
        mnem = make_intel_mnemonic(inst);

        /// add back a real type suffix only for memory operands
        const Pipedream_Operand_Template *mem_op = nullptr;

        for (const Pipedream_Operand_Template &op : operands) {
          /// suppressed operands not visible in ASM, GAS does not care
          if (op.visibility == XED_OPVIS_SUPPRESSED) { continue; }
          /// b/w/d/l/q operand suffixes are only relevant for scalar instrs.
          if (op.num_elements != 1) { continue; }

          if (op.is_mem()) {
            assert(!mem_op && "can not handle two memory operands");
            mem_op = &op;
          }
        }

        if (mem_op) {
          switch (mem_op->type) {
            case Pipedream::I8:
            case Pipedream::U8:
              mnem += 'b';
              break;
            case Pipedream::I16:
            case Pipedream::U16:
              mnem += 'w';
              break;
            case Pipedream::I32:
            case Pipedream::U32:
              mnem += 'l';
              break;
            case Pipedream::I64:
            case Pipedream::U64:
              mnem += 'q';
              break;
            default:
              break;
          }
        }
        break;
      }

      default:
        break;
    }

    return mnem;
  }

  /// Compute AT&T asm syntax instruction mnemonic from Pipedream_Instruction
  /// All fields except 'mnemonic' must already be filled in
  static std::string make_intel_mnemonic(XED_Decoded_Instruction inst) {
    std::string mnem = xed_iform_to_iclass_string_intel(inst.inst().iform());

    for (char &c : mnem) {
      c = std::tolower(c);
    }

    return mnem;
  }

  static Pipedream::Flag_Set flag_set_from_xed(const xed_flag_set_t *p) {
    using Flag     = Pipedream::Flag;
    using Flag_Set = Pipedream::Flag_Set;

    Flag_Set flags = 0;

    if (p->s.of) { flags |= Flag_Set(Flag::OF); }
    if (p->s.sf) { flags |= Flag_Set(Flag::SF); }
    if (p->s.zf) { flags |= Flag_Set(Flag::ZF); }
    if (p->s.af) { flags |= Flag_Set(Flag::AF); }
    if (p->s.pf) { flags |= Flag_Set(Flag::PF); }
    if (p->s.cf) { flags |= Flag_Set(Flag::CF); }
    if (p->s.df) { flags |= Flag_Set(Flag::DF); }
    if (p->s.vif) { flags |= Flag_Set(Flag::VIF); }
    if (p->s.iopl) { flags |= Flag_Set(Flag::IOPL); }
    if (p->s._if) { flags |= Flag_Set(Flag::IF); }
    if (p->s.ac) { flags |= Flag_Set(Flag::AC); }
    if (p->s.vm) { flags |= Flag_Set(Flag::VM); }
    if (p->s.rf) { flags |= Flag_Set(Flag::RF); }
    if (p->s.nt) { flags |= Flag_Set(Flag::NT); }
    if (p->s.tf) { flags |= Flag_Set(Flag::TF); }
    if (p->s.id) { flags |= Flag_Set(Flag::ID); }
    if (p->s.vip) { flags |= Flag_Set(Flag::VIP); }
    if (p->s.fc0) { flags |= Flag_Set(Flag::FC0); }
    if (p->s.fc1) { flags |= Flag_Set(Flag::FC1); }
    if (p->s.fc2) { flags |= Flag_Set(Flag::FC2); }
    if (p->s.fc3) { flags |= Flag_Set(Flag::FC3); }

    return flags;
  }

  static Pipedream::Flag_Set read_flags_from_xed(XED_Decoded_Instruction inst) {
    Pipedream::Flag_Set out = 0;

    if (xed_decoded_inst_uses_rflags(inst.raw())) {
      const xed_simple_flag_t *const rfi = xed_decoded_inst_get_rflags_info(inst.raw());

      const xed_flag_set_t *const read_set = xed_simple_flag_get_read_flag_set(rfi);

      out |= flag_set_from_xed(read_set);
    }

    return out;
  }

  static Pipedream::Flag_Set write_flags_from_xed(XED_Decoded_Instruction inst) {
    Pipedream::Flag_Set out = 0;

    if (xed_decoded_inst_uses_rflags(inst.raw())) {
      const xed_simple_flag_t *const rfi = xed_decoded_inst_get_rflags_info(inst.raw());

      /// write flags includes "undefined" flags
      /// TODO: seperate out undefined flags? what are they?
      const xed_flag_set_t *const write_set = xed_simple_flag_get_written_flag_set(rfi);

      out |= flag_set_from_xed(write_set);
    }

    return out;
  }
};

