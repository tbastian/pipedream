
import itertools
import typing as ty

__all__ = [
  'chunks',
  'nub',
  'Sentinel',
]

T = ty.TypeVar('T')


def chunks(seq: ty.Sequence[T], n: int) -> ty.Iterable[ty.Iterable[T]]:
  """
    Generate an iterable of chunks of length :n: from :seq:.
    The last chunk will have length :len(seq) % n:.
  """
  if n == 0:
    return

  it = iter(seq)
  while True:
    chunk = tuple(itertools.islice(it, n))
    if not chunk:
      break
    yield chunk


def nub(seq: ty.Iterable[T]) -> ty.Iterable[T]:
  """
    Filter duplicate elements from iterable.
    Elements in :seq: must be hashable.
  """

  seen = set()

  for elem in seq:
    if elem in seen:
      continue
    seen.add(elem)
    yield elem


class Sentinel:
  """
    Simple helper class for making unique objects with a name attached.
    A sentinel is always equal ('==') to itself, but never to any other object
    (i.e. '==' behaves like 'is').
  """

  _instances: ty.ClassVar[ty.Dict[str, 'Sentinel']] = {}

  def __init__(self, name):
    if type(name) is not str:
      raise TypeError('Sentinel name must be a string, not ' + repr(type(name).__name__))

    self.__name = name

  def __repr__(self):
    return "<" + self.__name + ">"

  def __new__(clss, name):
    try:
      return clss._instances[name]
    except KeyError:
      obj = super().__new__(clss)
      clss._instances[name] = obj
      return obj
