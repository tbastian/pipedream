# -*- coding: utf-8 -*-

"""
  Wrappers around pyyaml with slightly less horrible ergonomics
"""

import abc
import datetime
import enum
import sys
import yaml
import types
import typing as ty

__all__ = [
  'YAML_Serializable',
  'YAML_Struct',
  'Node',
]

Representer = yaml.representer.SafeRepresenter
Constructor = yaml.constructor.SafeConstructor

YAMLError        = yaml.error.YAMLError
ConstructorError = yaml.constructor.ConstructorError
ScannerError     = yaml.scanner.ScannerError

Node         = yaml.Node
SequenceNode = yaml.SequenceNode
MappingNode  = yaml.MappingNode
ScalarNode   = yaml.ScalarNode

SEQUENCE_TAG = 'tag:yaml.org,2002:seq'
MAPPING_TAG  = 'tag:yaml.org,2002:map'

T = ty.TypeVar('T')
K = ty.TypeVar('K')
V = ty.TypeVar('V')
E = ty.TypeVar('E', bound=enum.Enum)


class YAML_Serializer(abc.ABC, ty.Generic[T]):
  """
    turn an object into a YAML Node
  """

  @classmethod
  @ty.no_type_check
  def for_type(clss, want: type) -> 'YAML_Serializer[T]':
    ## FIXME: as of Python 3.8 there are public typing.get_origin and typing.get_args functions.

    origin = getattr(want, '__origin__', None)

    # python typing types
    if origin is not None:

      # python 3.6: __origin__ is typing.List
      # python 3.7: __origin__ is list
      if origin in (ty.List, list):
        args = want.__args__

        assert len(args) == 1

        return List_Serializer(
          clss.for_type(args[0])
        )

      if origin in (ty.Dict, dict):
        args = want.__args__

        assert len(args) == 2

        return Dict_Serializer(
          clss.for_type(args[0]),
          clss.for_type(args[1]),
        )

      if origin is ty.Union and len(want.__args__) == 2:
        args = want.__args__

        assert args[0] is not type(None)
        assert args[1] is type(None)

        return Optional_Serializer(
          clss.for_type(args[0])
        )

    # base types
    if want is str:
      return Str_Serializer()
    if want is int:
      return Int_Serializer()
    if want is float:
      return Float_Serializer()
    if want is datetime.datetime:
      return Date_Time_Serializer()
    if want is datetime.timedelta:
      return Time_Delta_Serializer()

    # normal classes
    if issubclass(want, YAML_Serializable):
      out = want.yaml_serializer()
      assert isinstance(out, YAML_Serializer), repr(out)
      return out

    # not supported
    raise TypeError('No serializer for type ' + ty._type_repr(want))

  @abc.abstractmethod
  def to_yaml(self, obj: T) -> yaml.Node:
    pass

  @abc.abstractmethod
  def from_yaml(self, node: yaml.Node) -> T:
    pass


class Str_Serializer(YAML_Serializer[str]):
  def to_yaml(self, obj: str) -> yaml.Node:
    return represent_str(obj)

  def from_yaml(self, node: yaml.Node) -> str:
    return construct_str(node)


class Int_Serializer(YAML_Serializer[int]):
  def to_yaml(self, obj):
    return represent_int(obj)

  def from_yaml(self, node):
    return construct_int(node)


class Float_Serializer(YAML_Serializer[float]):
  def to_yaml(self, obj):
    return represent_float(obj)

  def from_yaml(self, node):
    return construct_float(node)


class Date_Time_Serializer(YAML_Serializer[datetime.datetime]):
  def to_yaml(self, obj):
    return represent_datetime(obj)

  def from_yaml(self, node):
    return construct_datetime(node)


class Time_Delta_Serializer(YAML_Serializer[datetime.timedelta]):
  def to_yaml(self, obj):
    return represent_timedelta(obj)

  def from_yaml(self, node):
    return construct_timedelta(node)


class List_Serializer(ty.Generic[T], YAML_Serializer[ty.List[T]]):
  def __init__(self, item: YAML_Serializer[T]):
    self.item = item

  def to_yaml(self, obj):
    return represent_list(obj, self.item)

  def from_yaml(self, node):
    return construct_list(node, self.item)


class Dict_Serializer(ty.Generic[K, V], YAML_Serializer[ty.Dict[K, V]]):
  def __init__(self, key: YAML_Serializer[K], val: YAML_Serializer[V]):
    self.key = key
    self.val = val

  def to_yaml(self, obj):
    return represent_dict(obj, self.key, self.val)

  def from_yaml(self, node):
    return construct_dict(node, self.key, self.val)


class Optional_Serializer(ty.Generic[E], YAML_Serializer[ty.Optional[E]]):
  """
    Serializer for a typing.Optional[E] value (i.e. either None or an E).
  """

  def __init__(self, value: YAML_Serializer[E]):
    self.value = value

  def to_yaml(self, obj):
    if obj is None:
      return Representer().represent_none(None)
    else:
      return self.value.to_yaml(obj)

  def from_yaml(self, node):
    if type(node) is yaml.ScalarNode and node.tag == 'tag:yaml.org,2002:null':
      assert node.value == 'null'
      return None
    else:
      return self.value.from_yaml(node)


class Enum_Serializer(ty.Generic[E], YAML_Serializer[E]):
  """
    Serializer for a enum.Enum enumeration class.
    Enum values are simply represented by their name as a str
  """

  # TODO: use a YAML tag?

  def __init__(self, enum_class: ty.Type[E]):
    assert issubclass(enum_class, enum.Enum)
    self.enum_class = enum_class

  def to_yaml(self, obj: E):
    return represent_enum(self.enum_class, obj)

  def from_yaml(self, node):
    return construct_enum(node, self.enum_class)


class YAML_Serializable(abc.ABC):
  """
    Helper class for serializing to/from YAML.

    A bit less heavy-weight than yaml.YAMLObject, does not do any
    metaclass magic.
  """

  @abc.abstractclassmethod
  def yaml_serializer(clss) -> YAML_Serializer:
    raise NotImplementedError('abstract')

  def from_yaml(clss, node: Node) -> ty.Type['YAML_Serializable']:
    return clss.yaml_serializer().from_yaml(node)

  def to_yaml(self) -> Node:
    """
      serialize to yaml.Node
    """
    return self.yaml_serializer().to_yaml(self)


class YAML_Struct(YAML_Serializable):
  """
    Object that is represented by a mapping in YAML.

    Default implementations of methods from YAML_Serializable tries to create
    a mapping from attributes in __slots__ if it is present
    ('_' in attribute names will be replaced by '-').
  """

  _yaml_slots_: ty.ClassVar[ty.Tuple['Slot']]

  @classmethod
  def yaml_flow_style(clss) -> bool:
    """
      return true iff you want to use flow style when serializing this type
    """
    return False

  @classmethod
  def yaml_serializer(clss):
    return YAML_Struct_Serializer(clss)

  def __new__(clss, *args, **kwargs):
    obj = super().__new__(clss)

    for slot in clss._yaml_default_slots_:
      slot.set_default(obj)

    return obj

  def __init_subclass__(clss):
    super().__init_subclass__()

    ## FIXME: forbid inheritance
    assert clss.mro()[1] is YAML_Struct

    yaml_slots = []

    for v in clss.__dict__.values():
      if not isinstance(v, Slot):
        continue

      yaml_slots.append(v)

    clss._yaml_slots_         = tuple(yaml_slots)
    clss._yaml_default_slots_ = tuple(s for s in yaml_slots if s.has_default)
    clss._yaml_slot_dict_     = types.MappingProxyType({s.yaml_name: s for s in yaml_slots})


class YAML_Struct_Serializer(YAML_Serializer):
  """
    Default serializer for YAML_Struct objects
  """

  def __init__(self, struct):
    self.struct = struct

  def to_yaml(self, obj):
    def yaml_items():
      for slot in self.struct._yaml_slots_:
        key = represent_str(slot.yaml_name)
        val = slot.serializer.to_yaml(slot.__get__(obj))

        assert val is not None, repr(slot.type)

        yield key, val

    return make_mapping_node(
      yaml_items(),
      flow_style=self.struct.yaml_flow_style(),
    )

  def from_yaml(self, node: yaml.Node) -> list:
    check_type(MappingNode, node)

    kwargs = {}
    slots  = self.struct._yaml_slot_dict_

    for k, v in node.value:
      k = construct_str(k)

      slot = slots.get(k)

      if slot is None:
        raise yaml.constructor.ConstructorError('invalid field ' + repr(k) + ' in ' + self.struct.__name__)

      v = slot.serializer.from_yaml(v)

      kwargs[slot.py_name] = v

    return self.struct(**kwargs)


class Slot:
  """
    Descriptor for declaring fields in a YAML_Struct.
  """

  NO_DEFAULT = object()

  def __init__(self, type_, default = NO_DEFAULT):
    self._type       = type_
    self._serializer = YAML_Serializer.for_type(type_)

    if default is self.NO_DEFAULT:
      self._default     = self._fail_no_default
      self._has_default = False
    else:
      if type(default) is type:
        self._default = default
      else:
        self._default = lambda: default
      self._has_default = True

  def __set_name__(self, owner, name):
    self._py_name   = name
    self._yaml_name = name.replace('_', '-')

  def __get__(self, instance, owner=None):
    if instance is None:
      return self
    else:
      return instance.__dict__[self._py_name]

  def __set__(self, instance, value):
    instance.__dict__[self._py_name] = value

  @property
  def has_default(self) -> bool:
    """
      Check if this descriptor has a default value.
    """

    return self._has_default

  def set_default(self, instance):
    """
      Set property in *instance* to default value.
    """

    assert self.has_default, (repr(self.py_name) + ' ' +
                              ' (' + repr(self.yaml_name) + ') has no default')

    setattr(instance, self._py_name, self.default)

  @property
  def py_name(self):
    return self._py_name

  @property
  def yaml_name(self):
    return self._yaml_name

  @property
  def type(self):
    return self._type

  @property
  def serializer(self):
    return self._serializer

  @property
  def default(self):
    return self._default()

  def _fail_no_default(self):
    raise ValueError(f'Slot {self.yaml_name!r} has no default')


def load(serializer: YAML_Serializer, stream: ty.IO[str]) -> YAML_Serializable:
  """
    Load and deserialize one YAML document into an object.
  """

  loader = yaml.SafeLoader(stream)

  try:
    node = loader.get_single_node()

    if node is not None:
      return serializer.from_yaml(node)

    raise yaml.YAMLError('Empty document')
  finally:
    loader.dispose()


def load_all(serializer: YAML_Serializer, stream: ty.IO[str]) -> ty.Iterable[YAML_Serializable]:
  """
    Load and deserialize one YAML document into an object.
  """

  loader = yaml.SafeLoader(stream)

  try:
    while loader.check_data():
      if loader.check_node():
        node = loader.get_node()

        yield serializer.from_yaml(node)
  finally:
    loader.dispose()


def dump(obj: YAML_Serializable, stream: ty.IO[str] = sys.stdout):
  dumper = yaml.SafeDumper(stream)

  try:
    dumper.open()
    stream.write('\n')

    dumper.represent_data('')

    node = represent_object(obj)

    dumper.serialize(node)

    dumper.close()
  finally:
    dumper.dispose()


def dump_all(seq: ty.List[YAML_Serializable], stream: ty.IO[str] = sys.stdout):
  for obj in seq:
    dump(obj, stream)


################################################################################
##### helpers for parsing yaml.Node objects


def construct_str(node: yaml.Node) -> str:
  check_type(ScalarNode, node)

  return node.value


def construct_int(node: yaml.Node) -> int:
  return Constructor().construct_yaml_int(node)


def construct_float(node: yaml.Node) -> float:
  return Constructor().construct_yaml_float(node)


def construct_datetime(node: yaml.Node) -> datetime.datetime:
  return Constructor().construct_yaml_timestamp(node)


def construct_timedelta(node: yaml.Node) -> datetime.timedelta:
  txt = construct_str(node)

  if not txt.endswith('s'):
    raise yaml.constructor.ConstructorError(f'expected a string like "\d+([.]\d+)?s", got {txt!r}')

  txt = txt[:-1]

  return datetime.timedelta(seconds=float(txt))


def construct_list(node: yaml.Node, item: YAML_Serializer[T]) -> ty.List[T]:
  check_type(SequenceNode, node)
  check_tag(SEQUENCE_TAG, node)

  return [
    item.from_yaml(n) for n in node.value
  ]


def construct_dict(node: yaml.Node, key: YAML_Serializer[K], val: YAML_Serializer[V]) -> ty.Dict[K, V]:
  check_type(MappingNode, node)
  check_tag(MAPPING_TAG, node)

  return {
    key.from_yaml(kv[0]): val.from_yaml(kv[1]) for kv in node.value
  }


def construct_enum(node: yaml.Node, enum_class: ty.Type[E]):
  assert issubclass(enum_class, enum.Enum)
  check_type(ScalarNode, node)

  txt = construct_str(node)

  return enum_class[txt]


def construct_field(node: yaml.MappingNode, field: str, serializer: YAML_Serializer):
  """
    Helper for deserialzing structs.
    Takes a MappingNode, all keys must be strings.
    Find and pop key from *mapping.value*, then de-serialize and return value.
  """

  assert isinstance(node, MappingNode)

  val = None

  for i, kv in enumerate(node.value):
    k, v = kv

    k = construct_str(k)

    if k == field:
      node.value.pop(i)
      val = v

  if val is None:
    raise ValueError('Key ' + repr(field) + ' not in mapping')

  return serializer.from_yaml(val)


def check_tag(expected_tag: str, node: Node):
  """
    Check if *node* has expected tag, raise exception otherwise.
  """

  assert node.tag

  if node.tag != expected_tag:
    raise yaml.constructor.ConstructorError('expected a ' + expected_tag + ', got a ' + node.tag)


def check_type(expected_clss: ty.Type[Node], node: Node):
  """
    Check if *node* has expected type (scalar, mapping, ...), raise exception otherwise.
  """

  ## according to typeshed's pyyaml type annotations all subtypes of 'Node' have an 'id' field.
  ## But the 'Node' type does not :/
  assert node.id

  if type(node) is not expected_clss:
    raise yaml.constructor.ConstructorError('expected a ' + expected_clss.id + ', got a ' + node.id)


################################################################################
##### helpers for building yaml.Node objects


def represent_object(obj):
  serializer = YAML_Serializer.for_type(type(obj))

  return serializer.to_yaml(obj)


def represent_str(obj: str):
  return Representer().represent_str(obj)


def represent_int(obj: int):
  return Representer().represent_int(obj)


def represent_float(obj: float):
  return Representer().represent_float(obj)


def represent_datetime(obj: datetime.datetime):
  return Representer().represent_datetime(obj)


def represent_timedelta(obj: datetime.timedelta):
  assert type(obj) is datetime.timedelta, obj
  return represent_str('%fs' % obj.total_seconds())


def represent_list(sequence: ty.Iterable, itemser: YAML_Serializer, flow_style=None):
  value = []

  best_style = True

  for item in sequence:
    node_item = itemser.to_yaml(item)

    if not (isinstance(node_item, ScalarNode) and not node_item.style):
      best_style = False

    value.append(node_item)

  if flow_style is None:
    flow_style = best_style

  return SequenceNode(SEQUENCE_TAG, value, flow_style=flow_style)


def represent_dict(mapping: ty.Dict[ty.Any, ty.Any], key: YAML_Serializer, val: YAML_Serializer, flow_style=None):
  if not isinstance(mapping, dict):
    raise TypeError(type(mapping))

  def items():
    for item_key, item_value in mapping.items():
      node_key   = key.to_yaml(item_key)
      node_value = val.to_yaml(item_value)

      yield node_key, node_value

  return make_mapping_node(items(), flow_style)


def represent_enum(enum_class: ty.Type[E], enum_val: E):
  assert issubclass(enum_class, enum.Enum)
  assert enum_val in enum_class

  return represent_str(enum_val.name)


def make_mapping_node(mapping: ty.Iterable[ty.Tuple[Node, Node]], flow_style=None):
  """
    low level helper for creating MappingNode objects
  """

  tag        = MAPPING_TAG
  best_style = True

  value: ty.List[ty.Tuple[Node, Node]] = []
  key: Node
  val: Node

  for key, val in mapping:
    assert isinstance(key, Node), repr(key)
    assert isinstance(val, Node), repr(val)

    if not (isinstance(key, ScalarNode) and not key.style):
      best_style = False

    if not (isinstance(val, ScalarNode) and not val.style):
      best_style = False

    value.append((key, val))

  if flow_style is None:
    flow_style = best_style

  return MappingNode(tag, value, flow_style=flow_style)
