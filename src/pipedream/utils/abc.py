
import abc

__all__ = [
  'ABCMeta',
  'ABC',
  'abstractmethod',
  'override',
]


class ABCMeta(abc.ABCMeta):
  pass


class ABC(metaclass=ABCMeta):
  pass


abstractmethod      = abc.abstractmethod
abstractproperty    = abc.abstractproperty
abstractclassmethod = abc.abstractclassmethod


def override(f):
  f._abc_is_override = True
  return f


def final(f):
  f._abc_is_final = True
  return f
