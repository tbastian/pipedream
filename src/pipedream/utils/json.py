
import json
import re

__all__ = [
  'load',
  'remove_comments',
  'JSONDecodeError'
]

_RE_COMMENT        = re.compile(r'\s*(#|//).*$')
_RE_COMMENT_LINE   = re.compile(r'^\s*(#|//).*$')
_RE_INLINE_COMMENT = re.compile(r'(:?(?:\s)*([A-Za-z\d\.{}]*)|((?<=\").*\"),?)(?:\s)*(((#|(//)).*)|)$')


JSONDecodeError = json.JSONDecodeError


def remove_comments(txt: str) -> str:
  lines = txt.splitlines()

  for lineno, line in enumerate(lines):
    if re.search(_RE_COMMENT, line):
      if re.match(_RE_COMMENT_LINE, line):
        lines[lineno] = ""
      elif re.search(_RE_INLINE_COMMENT, line):
        lines[lineno] = re.sub(_RE_INLINE_COMMENT, r'\1', line)

  return '\n'.join(lines)


def load(fp, *, allow_comments: bool = True, **kwargs) -> object:
  """
    Load JSON from file.
  """

  if allow_comments:
    txt = fp.read()
    txt = remove_comments(txt)

    return json.loads(txt, **kwargs)
  else:
    return json.load(fp, **kwargs)
