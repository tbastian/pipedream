"""
  helpers for printing to ANSI terminals.
"""

import copy
import datetime
import io
import os
import sys
import typing as ty

__all__ = [
  'Colors',
  'Info_Line',
  'get_terminal_size',
]


class ANSI_Codes:
  ESC = '\033'

  RESET             = '0'   # ESC [ 0 m       # reset all (colors and brightness)

  BRIGHT            = '1'   # ESC [  1 m      # bright
  DIM               = '2'   # ESC [  2 m      # dim (looks same as normal brightness)
  NORMAL_BRIGHTENSS = '22'  # ESC [ 22 m      # normal brightness
  UNDERLINE         = '4'   # ESC [  4 m      # underline

  FG_BLACK    = '30'  # ESC [ 30 m                       # black
  FG_RED      = '31'  # ESC [ 31 m                       # red
  FG_GREEN    = '32'  # ESC [ 32 m                       # green
  FG_YELLOW   = '33'  # ESC [ 33 m                       # yellow
  FG_BLUE     = '34'  # ESC [ 34 m                       # blue
  FG_MAGENTA  = '35'  # ESC [ 35 m                       # magenta
  FG_CYAN     = '36'  # ESC [ 36 m                       # cyan
  FG_WHITE    = '37'  # ESC [ 37 m                       # white
  FG_RGB      = '38'  # ESC [ 38 ; 2 ; <r> ; <g> ; <b> m # foreground colour RGB
  FG_RESET    = '39'  # ESC [ 39 m                       # reset

  BG_BLACK    = '40'  # ESC [ 40 m      # black
  BG_RED      = '41'  # ESC [ 41 m      # red
  BG_GREEN    = '42'  # ESC [ 42 m      # green
  BG_YELLOW   = '43'  # ESC [ 43 m      # yellow
  BG_BLUE     = '44'  # ESC [ 44 m      # blue
  BG_MAGENTA  = '45'  # ESC [ 45 m      # magenta
  BG_CYAN     = '46'  # ESC [ 46 m      # cyan
  BG_WHITE    = '47'  # ESC [ 47 m      # white
  BG_RGB      = '48'  # ESC [ 38 ; 2 ; <r> ; <g> ; <b> m # foreground colour RGB
  BG_RESET    = '49'  # ESC [ 49 m      # reset

  @classmethod
  def style_str(clss, styles):
    return clss.ESC + '[' + ';'.join(styles) + 'm'

  @classmethod
  def reset(clss):
    return clss.style_str([clss.RESET])

  @classmethod
  def fg_rgb(clss, r: int, g: int, b: int):
    assert 0 <= r <= 255
    assert 0 <= g <= 255
    assert 0 <= b <= 255

    return clss.style_str([clss.FG_RGB, '2', str(r), str(g), str(b)])

  @classmethod
  def bg_rgb(clss, r: int, g: int, b: int):
    assert 0 <= r <= 255
    assert 0 <= g <= 255
    assert 0 <= b <= 255

    return clss.style_str([clss.BG_RGB, '2', str(r), str(g), str(b)])

  # # cursor positioning
  # ESC [ y;x H     # position cursor at x across, y down
  # ESC [ y;x f     # position cursor at x across, y down
  # ESC [ n A       # move cursor n lines up
  # ESC [ n B       # move cursor n lines down
  # ESC [ n C       # move cursor n characters forward
  # ESC [ n D       # move cursor n characters backward

  # # clear the screen
  # ESC [ mode J    # clear the screen

  # # clear the line
  # ESC [ mode K    # clear the line


class Colors:
  """
    Helper for writing terminal colors.
  """

  class Style:
    def __init__(self, *codes):
      self.codes = codes

    def __or__(self, that):
      if type(that) is not type(self):
        return NotImplemented

      return type(self)(*self.codes, *that.codes)

    def __call__(self, *args, sep='', ljust=0, rjust=0):
      return Colors.Formatter(
        self,
        *args,
        Colors.reset,
        sep=sep,
        ljust=ljust,
        rjust=rjust,
      )

    def __str__(self):
      return ANSI_Codes.style_str(self.codes)

  class Formatter:
    def __init__(self, *strs, sep='', ljust=0, rjust=0):
      self.sep    = sep
      self._ljust = ljust
      self._rjust = rjust
      self.strs   = strs

    def __len__(self):
      return max([
        self._len_no_just(),
        self._ljust,
        self._rjust
      ])

    def ljust(self, just) -> 'Colors.Formatter':
      c = self._copy()
      c._ljust = just
      return c

    def rjust(self, just) -> 'Colors.Formatter':
      c = self._copy()
      c._rjust = just
      return c

    def __str__(self):
      assert not (self._ljust and self._rjust), "can't justify both right and left"

      slen  = self._len_no_just()
      ljust = max(0, self._ljust - slen)
      rjust = max(0, self._rjust - slen)

      txt = self.sep.join(map(str, self.strs))
      txt = txt + ' ' * ljust
      txt = ' ' * rjust + txt

      return txt

    def __add__(self, that):
      return Colors.Formatter(self, that)

    def __radd__(self, that):
      return Colors.Formatter(that, self)

    def _len_no_just(self):
      """
        __len__ without ljust/rjust
      """

      return sum(len(s) for s in self.strs if not isinstance(s, Colors.Style))

    def _copy(self):
      return copy.deepcopy(self)

  none      = Style()

  reset     = Style(ANSI_Codes.RESET)

  bold      = Style(ANSI_Codes.BRIGHT)
  underline = Style(ANSI_Codes.UNDERLINE)

  red       = Style(ANSI_Codes.FG_RED)
  blue      = Style(ANSI_Codes.FG_BLUE)
  green     = Style(ANSI_Codes.FG_GREEN)
  yellow    = Style(ANSI_Codes.FG_YELLOW)
  cyan      = Style(ANSI_Codes.FG_CYAN)
  magenta   = Style(ANSI_Codes.FG_MAGENTA)

  @classmethod
  def fg_rgb(clss, r, g, b):
    return clss.Style(ANSI_Codes.FG_RGB, '2', str(r), str(g), str(b))

  @classmethod
  def bg_rgb(clss, r, g, b):
    return clss.Style(ANSI_Codes.BG_RGB, '2', str(r), str(g), str(b))


class Info_Line:
  '''
    Helper for showing a line of text of information on a terminal that can
    be changed and removed again after begin printed.
  '''

  def __call__(self, *args, rprompt: str=None):
    """Prints an info message to stdout or stderr if they are TTYs."""

    stream = get_terminal_stream()
    if not stream:
      return

    tmp = io.StringIO()
    print('#', *args, file=tmp, end='')
    txt = tmp.getvalue()
    if rprompt:
      txt += ' ' + rprompt

    columns = self.get_terminal_size().columns

    if len(txt) > columns:
      txt = txt[:self.get_terminal_size().columns - 1]

      if rprompt:
        txt = txt[:-len(rprompt) - 1]
        txt += ' ' + rprompt

    assert len(txt) <= columns

    self.clear()
    print(txt, file=stream, end='', flush=True)

  @staticmethod
  def timestamp(now: datetime.datetime = None) -> str:
    if now is None:
      now = datetime.datetime.now()
    return format(now, '%04Y.%02m.%02d-%H:%M:%S')

  def warning(self, *args):
    """
      Print warning message to stderr
    """

    self.clear()
    print(
      '# warning:', *args,
      file=sys.stderr,
      flush=True,
    )

  def log(self, *args):
    """
      Print log message to stdout
    """

    self.clear()
    print(
      '#', *args,
      file=sys.stdout,
      flush=True,
    )

  def undo_last_line(self):
    """Prints an info message to stdout."""

    stream = get_terminal_stream()
    if not stream:
      return

    print('\r\033[0F', file=stream, end='', flush=True)

  def clear(self):
    """Prints an info message to stdout."""

    stream = get_terminal_stream()
    if not stream:
      return

    print('\r\033[0K', file=stream, end='', flush=True)

  def commit(self):
    """
      Print '\n' and flush (whatever is currently displayed can't be undone anymore; is thus `committed`)
    """

    stream = get_terminal_stream()
    if not stream:
      return

    print(flush=True)

  @staticmethod
  def get_terminal_size():
    return get_terminal_size()


def get_terminal_stream() -> ty.Optional[ty.TextIO]:
  """
    Return stdout if it is a TTY, else stderr if that is a TTY else None.
  """
  if sys.stdout.isatty():
    return sys.stdout
  elif sys.stderr.isatty():
    return sys.stderr
  else:
    return None


def get_terminal_size() -> os.terminal_size:
  """
    Version of os.get_terminal_size that tries stderr if stdout
    is not a TTY
  """

  fallback = os.terminal_size((80, 24))

  try:
    columns = int(os.environ['COLUMNS'])
  except (KeyError, ValueError):
    columns = 0

  try:
    lines = int(os.environ['LINES'])
  except (KeyError, ValueError):
    lines = 0

  # only query if necessary
  if columns <= 0 or lines <= 0:
    try:
      stream: ty.TextIO

      if sys.stdout.isatty():
        stream = sys.stdout
      elif sys.stderr.isatty():
        stream = sys.stderr
      else:
        raise ValueError()

      size = os.get_terminal_size(stream.fileno())
    except (AttributeError, ValueError, OSError):
      # stdout is None, closed, detached, or not a terminal, or
      # os.get_terminal_size() is unsupported
      size = os.terminal_size(fallback)
    if columns <= 0:
      columns = size.columns
    if lines <= 0:
      lines = size.lines

  return os.terminal_size((columns, lines))
