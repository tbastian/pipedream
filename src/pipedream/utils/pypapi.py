
import ctypes
import ctypes.util
from ctypes import c_int, c_char_p, POINTER, pointer
import numbers
import typing as ty

from pipedream.utils import nub

__all__ = [
  'Papi',
  'PapiException',
  'PapiEventManager',
  'PapiEventSet',
  'make_null_event_set',
]

T = ty.TypeVar('T')

_PAPI_OK   = 0
_PAPI_NULL = -1


class PapiException(Exception):
  pass


class Papi:
  def __init__(self):
    lib = ctypes.cdll.LoadLibrary(ctypes.util.find_library('papi'))

    if not lib:
      raise ImportError('Could not find PAPI shared library')

    self.lib = lib

    self.PAPI_library_init       = self._get_fn('PAPI_library_init',       c_int,    c_int)
    self.PAPI_is_initialized     = self._get_fn('PAPI_is_initialized',     c_int)
    self.PAPI_strerror           = self._get_fn('PAPI_strerror',           c_char_p, ctypes.c_int)
    self.PAPI_event_name_to_code = self._get_fn('PAPI_event_name_to_code', c_int,    c_char_p, POINTER(c_int))
    self.PAPI_add_event          = self._get_fn('PAPI_add_event',          c_int,    c_int, c_int)

    self.PAPI_create_eventset    = self._get_fn('PAPI_create_eventset',    c_int,    POINTER(c_int))
    self.PAPI_cleanup_eventset   = self._get_fn('PAPI_cleanup_eventset',   c_int,    c_int)
    self.PAPI_destroy_eventset   = self._get_fn('PAPI_destroy_eventset',   c_int,    POINTER(c_int))

    self.PAPI_query_event        = self._get_fn('PAPI_query_event',        c_int,    c_int)

    PAPI_EINVAL = -1

    success = False

    ALLOWED_VERSIONS = [
      *[(5, v) for v in range(4, 42)],
      *[(6, v) for v in range(0, 10)],
    ]

    for major_version, minor_version in ALLOWED_VERSIONS:
      version = _PapiVersion(major_version, minor_version)

      ret = self.PAPI_library_init(version.encode())

      if ret == PAPI_EINVAL:
        ## bad PAPI version, try another one
        continue

      if _PapiVersion.decode(ret) != version:
        _die('PAPI_library_init failed with', ret)

      self.version = version
      success      = True
      break

    if not success:
      min_major, min_minor = min(ALLOWED_VERSIONS)
      max_major, max_minor = max(ALLOWED_VERSIONS)

      _die(f'PAPI has unsupported version, want >={min_major}.{min_minor} <={max_major}.{max_minor}')

  def __del__(self):
    if self.lib:
      # lookup on the fly in case of half-way initialised self
      self.lib.PAPI_shutdown()

  def can_count_event(self, name: str):
    event_name_bytes = name.encode('utf-8')

    event_code = pointer(c_int(_PAPI_NULL))

    ret = self.PAPI_event_name_to_code(c_char_p(event_name_bytes), event_code)
    if ret != _PAPI_OK:
      return False

    ret = self.PAPI_query_event(event_code[0])
    if ret != _PAPI_OK:
      return False

    return True

  def make_event_manager(self, events: ty.Sequence[str], events_in_every_set: ty.Sequence[str] = ()) -> 'PapiEventManager':
    """
      create event manager with as many event sets as necessary to measure
      all given events.
      Every set contains all events from `events_in_every_set` and at least one
      event from `events`.
    """

    events_in_every_set = list(nub(events_in_every_set))
    events              = [e for e in nub(events) if e not in events_in_every_set]

    if not events:
      if not events_in_every_set:
        _die("tried to create empty event set")
      else:
        event_set = self._make_event_set(events_in_every_set)

        return PapiEventManager(self, [event_set])
    else:
      event_sets = []

      while events:
        ## create new event set
        event_set = self._make_event_set(events_in_every_set)

        ## add events to set until it is full
        added_one = False

        while events:
          event_name = events[0]

          try:
            self._add_event_to_set(event_set, event_name)
            added_one = True
            events.pop(0)
          except PapiException:
            if added_one:
              # event set is full (or there are conflicts)
              break
            elif not self._event_exists(event_name):
              _die('Invalid event name', repr(event_name))
            else:
              # can't fit event next to common events
              _die(
                'cannot fit event', repr(event_name), 'into set with common events:',
                ', '.join(map(repr, events_in_every_set))
              )

        event_sets.append(event_set)

      return PapiEventManager(self, event_sets)

  def make_exact_event_sets(self, event_sets: ty.Sequence[ty.Sequence[str]]) -> 'PapiEventManager':
    """
      given a list of lists of event names create an event manager with
      exactly these event sets.
      Fail with a PapiException if PAPI cannot create exactly these event sets.
    """

    event_sets = [list(set) for set in event_sets]

    if not event_sets:
      _die("tried to create event manager with no event sets")

      for set in event_sets:
        _die("tried to create empty event set")

    built_event_sets = []

    for events in event_sets:
      event_set = self._make_event_set(events)

      built_event_sets.append(event_set)

    return PapiEventManager(self, built_event_sets)

  ## private parts

  def _make_event_set(self, events: ty.Sequence[str]) -> 'PapiEventSet':
    """
      create new, possibly empty, event set.
    """

    c_event_set = pointer(c_int(_PAPI_NULL))
    c_event_set[0] = c_int(_PAPI_NULL)

    ret = self.PAPI_create_eventset(c_event_set)
    if ret != _PAPI_OK:
      self._handle_PAPI_error(ret, "could not allocate event set")

    ## FIXME: mypy (reasonably) thinks that POINTER(c_int).__getitem__ returns a c_int,
    ##        but actually it is a special case that returns an int.
    event_set_id: int = ty.cast(int, c_event_set[0])

    py_event_set = PapiEventSet(self, event_set_id, ())

    ## add events to set
    for event_name in events:
      self._add_event_to_set(py_event_set, event_name)

    return py_event_set

  def _event_exists(self, event_name: str) -> bool:
    event_name_bytes = event_name.encode('ascii')

    event_code = pointer(c_int(_PAPI_NULL))

    ret = self.PAPI_event_name_to_code(c_char_p(event_name_bytes), event_code)
    return (ret != _PAPI_OK)

  def _add_event_to_set(self, event_set: 'PapiEventSet', event_name: str) -> 'PapiEvent':
    event_name_bytes = event_name.encode('ascii')

    event_code = pointer(c_int(_PAPI_NULL))

    ret = self.PAPI_event_name_to_code(c_char_p(event_name_bytes), event_code)
    if ret != _PAPI_OK:
      self._handle_PAPI_error(ret, "could not find event", repr(event_name))

    ret = self.PAPI_add_event(event_set.id, event_code[0])
    if ret != _PAPI_OK:
      self._handle_PAPI_error(ret, "could not add event to set", repr(event_name))

    event = PapiEvent(event_code[0], event_name)

    event_set.events += (event,)

    return event

  ## FIXME: calculate arg types of returned callable from *argtypes*.
  def _get_fn(self, name: str, restype: ty.Type, *argtypes) -> ty.Callable[[], T]:
    fn = getattr(self.lib, name)

    fn.restype  = restype
    fn.argtypes = tuple(argtypes)
    return fn

  def _handle_PAPI_error(self, err_code: int, *args):
    msg: ty.Sequence[ty.Any]

    if err_code:
      msg = [self.PAPI_strerror(err_code).decode('ascii') + ' (' + str(err_code) + '): ']
    else:
      msg = ()

    _die(*msg, *args)

  def __repr__(self):
    return type(self).__name__ + "('" + str(self.version) + "')"


def make_null_event_set() -> 'PapiEventSet':
  """
    Create a fake empty PapiEventSet object, corresponds to _PAPI_NULL.
    It does not contain any events, you cannot add any events.
  """

  return PapiEventSet.make_null()


class PapiEventManager:
  def __init__(self, papi, event_sets: ty.List['PapiEventSet']):
    self._papi       = papi
    self._event_sets = event_sets

  @property
  def event_sets(self) -> ty.Iterable['PapiEventSet']:
    """
      Iterator event sets of this manager
    """

    return iter(self._event_sets)

  @property
  def num_event_sets(self) -> int:
    """
      Number of event sets of this manager
    """

    return len(self._event_sets)

  @property
  def event_names(self) -> ty.Iterable[str]:
    """
      Iterator over names of all events in all event sets of this manager.
    """

    for set in self.event_sets:
      yield from set.event_names

  def __repr__(self):
    return type(self).__name__ + '(' + ', '.join(repr(list(set.event_names)) for set in self.event_sets) + ')'


class PapiEventSet:
  def __init__(self, papi, id: int, events: ty.Sequence['PapiEvent']):
    self.papi   = papi
    self.id     = c_int(id)
    self.events = tuple(events)

  @classmethod
  def make_null(clss):
    return clss(papi=None, id=0, events=())

  def __del__(self):
    if self.papi is not None:
      set_ptr = pointer(self.id)

      ret = self.papi.PAPI_cleanup_eventset(self.id)
      if ret != _PAPI_OK:
        self.papi._handle_PAPI_error(ret, "Could not destroy event set", self)

      ret = self.papi.PAPI_destroy_eventset(set_ptr)
      if ret != _PAPI_OK:
        self.papi._handle_PAPI_error(ret, "Could not destroy event set", self)

  def __iter__(self) -> ty.Iterator['PapiEvent']:
    return iter(self.events)

  def all_present(self, events) -> ty.Iterable[str]:
    """
      returns sequence of event names present in both *self* and *events*
      (in the order they are given in *events*).
    """

    for evt in events:
      if evt in self.event_names:
        yield evt

  def index_of(self, event_name) -> int:
    for i, evt in enumerate(self):
      if evt.name == event_name:
        return i

    raise IndexError()

  @property
  def event_names(self):
    for event in self:
      yield event.name

  @property
  def num_events(self):
    return len(self.events)

  def __repr__(self):
    return type(self).__name__ + '(' + str(self.id) + ', [' + ', '.join(evt.name for evt in self.events) + '])'


class PapiEvent:
  def __init__(self, code, name):
    self.code = c_int(code)
    self.name = name

  def __repr__(self):
    return type(self).__name__ + '(' + repr(self.name) + ')'


class _PapiVersion:
  def __init__(self, major: int, minor: int, revision: int = 0, increment: int = 0):
    self.major     = int(major)
    self.minor     = int(minor)
    self.revision  = int(revision)
    self.increment = int(increment)

  @staticmethod
  def decode(version):
    major     = (version >> 24) & 0xFF
    minor     = (version >> 16) & 0xFF
    revision  = (version >> 8) & 0xFF
    increment = version & 0xFF

    return _PapiVersion(major, minor, revision, increment)

  def encode(self):
    return (self.major << 24) | (self.minor << 16) | (self.revision << 8) | self.increment

  def __eq__(self, that):
    if isinstance(that, _PapiVersion):
      return self.encode() == that.encode()

    if isinstance(that, numbers.Integral):
      return self.encode() == that

    return NotImplemented

  def __str__(self):
    return '.'.join(map(str, [self.major, self.minor, self.increment, self.revision]))

  def __repr__(self):
    txt = type(self).__name__ + '('
    txt += ', '.join(map(str, [self.major, self.minor, self.increment, self.revision])) + ')'
    return txt


def _die(*args) -> ty.NoReturn:
  raise PapiException(' '.join(map(str, args)))
