
import enum
import typing as ty

__all__ = [
  'X86_Flags',
]


class X86_Flags(enum.IntFlag):
  """
    flags for CPU flags register
  """
  ## FLAGS
  CF   = 0x0000_0000_0001  # Carry flag
  PF   = 0x0000_0000_0004  # Parity flag
  AF   = 0x0000_0000_0010  # Adjust flag
  ZF   = 0x0000_0000_0040  # Zero flag
  SF   = 0x0000_0000_0080  # Sign flag
  TF   = 0x0000_0000_0100  # Trap flag
  IF   = 0x0000_0000_0200  # Interrupt enable flag
  DF   = 0x0000_0000_0400  # Direction flag
  OF   = 0x0000_0000_0800  # Overflow flag
  IOPL = 0x0000_0000_3000  # I/O privilege level (286+ only)
  NT   = 0x0000_0000_4000  # Nested task flag (286+ only)
  ## EFLAGS
  RF   = 0x0000_0001_0000  # Resume flag (386+ only)
  VM   = 0x0000_0002_0000  # Virtual 8086 mode flag (386+ only)
  AC   = 0x0000_0004_0000  # Alignment check (486SX+ only)
  VIF  = 0x0000_0008_0000  # Virtual interrupt flag (Pentium+)
  VIP  = 0x0000_0010_0000  # Virtual interrupt pending (Pentium+)
  ID   = 0x0000_0020_0000  # Able to use CPUID instruction (Pentium+)
  ## RFLAGS

  ## X87 FLAGS
  FC0  = 0x0001_0000_0000
  FC1  = 0x0002_0000_0000
  FC2  = 0x0004_0000_0000
  FC3  = 0x0008_0000_0000

  def __iter__(self) -> ty.Iterable['X86_Flags']:
    for flag in X86_Flags:
      if self & flag:
        yield flag
