
import collections
import dataclasses
import datetime
import enum
import functools
import heapq
import io
import json
import math
import numpy
import pathlib
import sys
import typing as ty

import msgpack

from pipedream.utils import yaml
from pipedream.utils.statistics import Statistics
from pipedream.asm   import ir


class Benchmark_Kind(enum.Enum):
  THROUGHPUT = 1
  LATENCY    = 2


class Loop_Overhead(yaml.YAML_Struct):
  instructions = yaml.Slot(int)
  muops        = yaml.Slot(int)

  def __init__(self, instructions: int, muops: int):
    assert type(instructions) is int
    assert type(muops) is int

    self.instructions = instructions
    self.muops        = muops

  def __str__(self):
    return f'instructions: {self.instructions}, muops: {self.muops}'

  def __repr__(self):
    return f'{type(self).__name__}(instructions={self.instructions}, muops={self.muops})'


class Benchmark_Spec(yaml.YAML_Serializable):
  """
    information used to emit one benchmark function.
  """

  def __init__(self, name: str, kind: Benchmark_Kind,
               unroll_factor: int, kernel_iterations: int,
               arch: ir.Architecture, instructions: ty.List[ir.Instruction],
               register_pools: ty.Optional[ty.Dict[ir.Instruction, int]],
               align_kernel: bool, loop_overhead: Loop_Overhead):
    """
      ctor.

      :param name                 name of benchmark
      :param kind                 type of benchmark (Benchmark_Kind)
      :param unroll_factor        how many times the kernel is unrolled when generating the code
      :param kernel_iterations    how many iterations the inner benchmark loop is executed
      :param kernel               list of instructions inside the kernel (before unrolling)
      :param arch                 asm.Architecture for the CPU architecture this benchmark is for
      :param instructions         instructions in this benchmark kernel
      :param register_pools       relative importance of the instruction in terms of amount of register reserved
      :param align_kernel         whether we add padding after each unrolling of `instructions`
      :param loop_overhead        number of instructions dynamically executed to run loop around
                                  benchmark kernel (does not include instructions of kernel itself)
    """

    assert kind in Benchmark_Kind

    self.name              = name
    self.kind              = kind
    self.unroll_factor     = unroll_factor
    self.kernel_iterations = kernel_iterations
    self.arch              = arch
    self.instructions      = instructions
    self.register_pools    = register_pools
    self.align_kernel      = align_kernel
    self.loop_overhead     = loop_overhead

  @classmethod
  def from_instructions(clss, *,
                        kind: Benchmark_Kind,
                        arch: ir.Architecture,
                        instructions: ty.Sequence[ir.Instruction],
                        register_pools: ty.Optional[ty.Dict[ir.Instruction, int]] = None,
                        align_kernel: bool = False,
                        name: str = None,
                        num_dynamic_instructions: int,
                        unrolled_length: int = None,
                        unroll_factor: int = None):
    """
      creates Benchmark_Spec from a list of instructions.
      Finds the smalleset *unroll_factor* such that the number of instructions in the kernel
      is >= *unrolled_length*.
      Finds the smallest *kernel_iterations* such that the number of of dynamic instructions
      executed by the kernel >= *num_dynamic_instructions*
    """

    if not unroll_factor and not unrolled_length:
      raise ValueError('need to specify either unroll_factor or unrolled_length')

    if unroll_factor and unrolled_length:
      raise ValueError('can not specify both unroll_factor and unrolled_length')

    instructions = tuple(instructions)

    for inst in instructions:
      assert isinstance(inst, ir.Instruction)

      if not inst.can_benchmark:
        raise ValueError(f'can not benchmark instruction {inst.name}')

    if name is None:
      name = clss.name_from_instructions(instructions)

    if not name:
      raise ValueError('empty name')

    def div_round_up(a, b):
      return (a + b - 1) // b

    if not unroll_factor:
      assert unrolled_length
      if len(instructions):
        unroll_factor = div_round_up(unrolled_length, len(instructions))
      else:
        unroll_factor = 0

    if instructions:
      static_length     = unroll_factor * len(instructions)
      kernel_iterations = div_round_up(num_dynamic_instructions, static_length)
    else:
      kernel_iterations = num_dynamic_instructions

    return Benchmark_Spec(
      name              = name,
      kind              = kind,
      unroll_factor     = unroll_factor,
      kernel_iterations = kernel_iterations,
      arch              = arch,
      instructions      = instructions,
      register_pools    = register_pools,
      align_kernel      = align_kernel,
      loop_overhead     = arch.loop_overhead(kernel_iterations),
    )

  @staticmethod
  def from_name_str(instruction_set: ir.Instruction_Set, spec: str) -> ty.List[ir.Instruction]:
    """
        parse a kernel spec returned by name_from_instructions() into a list of instructions.

        Raises ValueError if the spec is invalid.
    """
    assert isinstance(instruction_set, ir.Instruction_Set)

    spec = spec.strip()

    if not spec:
      return []
      raise ValueError('empty kernel spec')

    insts = []

    for expr in spec.split():
      assert expr

      if expr.count('^') > 1:
        raise ValueError('invalid kernel spec: expression has multiple "^": ' + repr(expr))

      name, _, count = expr.partition('^')

      if not name:
        raise ValueError('invalid kernel spec: no instruction name in expression: ' + repr(expr) + repr([name, _, count]))

      if count:
        try:
          count = int(count)
        except ValueError:
          raise ValueError('invalid kernel spec: invalid count ' + repr(count) + ' in expression ' + repr(expr))
      else:
        count = 1

      try:
        inst = instruction_set[name]
      except KeyError:
        raise ValueError('invalid kernel spec: invalid name ' + repr(name) + ' in expression ' + repr(expr))

      insts.extend([inst] * count)

    return insts

  @staticmethod
  def name_from_instructions(instructions: ty.Sequence[ir.Instruction]):
    """
      generate name of a benchmark from the names of a list of instructions.

      The result is basically a run-length compression of the instruction names.
    """

    return Benchmark_Spec.name_from_instruction_names(i.name for i in instructions)

  @staticmethod
  def name_from_instruction_names(instructions: ty.Iterable[str]):
    """
      generate name of a benchmark from the names of a list of instruction names.

      The result is basically a run-length compression of the instruction names.
    """

    instructions = list(instructions)

    if not instructions:
      return '<empty>'

    current = instructions.pop(0)
    count   = 1
    name    = current

    while instructions:
      next = instructions.pop(0)

      if current != next:
        if count > 1:
          name += '^' + str(count)

        name  += ' ' + str(next)
        current = next
        count   = 1
      else:
        count += 1

    if count > 1:
      name += '^' + str(count)

    return name

  @classmethod
  def yaml_serializer(clss):
    return clss._Serializer()

  class _Serializer(yaml.YAML_Serializer['Benchmark_Spec']):
    def to_yaml(self, obj) -> yaml.Node:
      return yaml.make_mapping_node(
        (
          (yaml.represent_str('name'),              yaml.represent_str(obj.name)),
          (yaml.represent_str('kind'),              yaml.represent_enum(Benchmark_Kind, obj.kind)),
          (yaml.represent_str('unroll_factor'),     yaml.represent_int(obj.unroll_factor)),
          (yaml.represent_str('kernel_iterations'), yaml.represent_int(obj.kernel_iterations)),
          (yaml.represent_str('arch'),              yaml.represent_str(obj.arch.name)),
          (yaml.represent_str('instructions'),      yaml.represent_list((i.name for i in obj.instructions),
                                                                        yaml.Str_Serializer())),
          (yaml.represent_str('loop_overhead'),     obj.loop_overhead.to_yaml()),
        )
      )

    def from_yaml(self, node: yaml.Node) -> str:
      name              = yaml.construct_field(node, 'name', yaml.Str_Serializer())
      kind              = yaml.construct_field(node, 'kind', yaml.Enum_Serializer(Benchmark_Kind))
      unroll_factor     = yaml.construct_field(node, 'unroll_factor', yaml.Int_Serializer())
      kernel_iterations = yaml.construct_field(node, 'kernel_iterations', yaml.Int_Serializer())
      loop_overhead     = yaml.construct_field(node, 'loop_overhead', Loop_Overhead.yaml_serializer())

      arch = yaml.construct_field(node, 'arch', yaml.Str_Serializer())
      arch = ir.Architecture.for_name(arch)

      instruction_set = arch.instruction_set()

      instructions = yaml.construct_field(node, 'instructions', yaml.List_Serializer(yaml.Str_Serializer()))
      instructions = [instruction_set[i] for i in instructions]

      return Benchmark_Spec(
        name,
        kind,
        unroll_factor,
        kernel_iterations,
        arch,
        instructions,
        loop_overhead,
      )


class Event_Set_Run(yaml.YAML_Struct):
  """
    Run one event set over a benchmark
  """

  cycles         = yaml.Slot(Statistics)
  instructions   = yaml.Slot(ty.Optional[Statistics])
  fused_muops    = yaml.Slot(ty.Optional[Statistics])
  unfused_muops  = yaml.Slot(ty.Optional[Statistics])

  ## IPC not counting instructions for the kernel loop
  clean_IPC    = yaml.Slot(ty.Optional[Statistics])
  clean_fMPC   = yaml.Slot(ty.Optional[Statistics])
  clean_uMPC   = yaml.Slot(ty.Optional[Statistics])

  IPC          = yaml.Slot(ty.Optional[Statistics])
  fMPC         = yaml.Slot(ty.Optional[Statistics])
  uMPC         = yaml.Slot(ty.Optional[Statistics])

  port_muops   = yaml.Slot(ty.Dict[int, Statistics], default={})
  misc         = yaml.Slot(ty.Dict[str, Statistics], default={})

  def __init__(self, *,
               cycles,
               instructions = None, IPC = None, clean_IPC = None,
               fused_muops = None, clean_fMPC = None, fMPC = None,
               unfused_muops = None, clean_uMPC = None, uMPC = None,
               port_muops = None, misc = None):
    self.cycles        = cycles
    self.instructions  = instructions
    self.fused_muops   = fused_muops
    self.unfused_muops = unfused_muops

    self.clean_IPC    = clean_IPC
    self.clean_fMPC   = clean_fMPC
    self.clean_uMPC   = clean_uMPC

    self.IPC          = IPC
    self.fMPC         = fMPC
    self.uMPC         = uMPC

    self.port_muops   = port_muops or {}
    self.misc         = misc       or {}

  def drop_details(self):
    """
      Throw away the histogram and all percentiles except 10,25,50,75,90 in all Statistics.
      This is intended to reduce memory usage.
    """

    for slot in Event_Set_Run._yaml_slots_:
      stat = getattr(self, slot.py_name)

      if type(stat) is not Statistics:
        continue

      stat.drop_details()

    for stat in self.port_muops.values():
      stat.drop_details()

    for stat in self.misc.values():
      stat.drop_details()


class Benchmark_Run(yaml.YAML_Struct):
  """
    Results from running a benchmark with a set of perf counters
  """

  benchmark = yaml.Slot(Benchmark_Spec)
  timestamp = yaml.Slot(datetime.datetime)
  runtime   = yaml.Slot(datetime.timedelta)

  cycles        = yaml.Slot(Statistics)
  instructions  = yaml.Slot(Statistics)
  fused_muops   = yaml.Slot(Statistics)
  unfused_muops = yaml.Slot(Statistics)

  ## IPC not counting instructions for the kernel loop
  clean_IPC    = yaml.Slot(Statistics)
  ## fused muops -//-
  clean_fMPC   = yaml.Slot(Statistics)
  ## unfused muops -//-
  clean_uMPC   = yaml.Slot(Statistics)

  ## instructions per cycle
  IPC          = yaml.Slot(Statistics)
  ## fused muops -//-
  fMPC         = yaml.Slot(Statistics)
  ## unfused muops -//-
  uMPC         = yaml.Slot(Statistics)

  event_sets   = yaml.Slot(ty.List[Event_Set_Run])

  def __init__(self, *, benchmark, timestamp, runtime = datetime.timedelta(seconds = 0),
               cycles,
               instructions, IPC, clean_IPC,
               fused_muops, clean_fMPC, fMPC,
               unfused_muops, clean_uMPC, uMPC,
               event_sets):
    self.benchmark    = benchmark
    self.timestamp    = timestamp
    self.runtime      = runtime
    self.cycles       = cycles

    self.instructions = instructions
    self.clean_IPC    = clean_IPC
    self.IPC          = IPC

    self.fused_muops  = fused_muops
    self.clean_fMPC   = clean_fMPC
    self.fMPC         = fMPC

    self.unfused_muops = unfused_muops
    self.clean_uMPC    = clean_uMPC
    self.uMPC          = uMPC

    self.event_sets   = event_sets

  ## XXX

  @property
  def name(self):
    return self.benchmark.name

  @property
  def kernel(self):
    return tuple(sorted(i.name for i in self.benchmark.instructions))

  @property
  def ipc(self):
    return self.clean_IPC

  @property
  def fmpc(self):
    return self.clean_fMPC

  @property
  def umpc(self):
    return self.clean_uMPC

  @property
  def port_muops(self):
    return self.ports_usage()

  ## XXX

  def drop_details(self):
    """
      Throw away the histogram and all percentiles except 10,25,50,75,90 in all Statistics.
      This is intended to reduce memory usage.
    """
    # If we are saving raw values, then we should not care about memory usage
    for slot in Benchmark_Run._yaml_slots_:
      stat = getattr(self, slot.py_name)

      if type(stat) is not Statistics:
        continue

      stat.drop_details()

    for event_set in self.event_sets:
      event_set.drop_details()

  @property
  def num_fused_muops(self) -> int:
    return round(self.clean_fMPC.mean / self.clean_IPC.mean)

  @property
  def num_unfused_muops(self) -> int:
    return round(self.clean_uMPC.mean / self.clean_IPC.mean)

  def ports_usage(self, min_usage: float = 0.05) -> ty.Dict[int, Statistics]:
    assert 0 < min_usage <= 1, min_usage

    out = {}

    for m in self.event_sets:
      for port, stat in m.port_muops.items():
        assert port not in out, port

        if (stat.mean / self.unfused_muops.mean) < min_usage:
          continue

        out[port] = stat

    return out

  def ports_used(self, min_usage: float = 0.05) -> ty.FrozenSet[int]:
    """
      Return set of ports where at least :min_usage: percent of all muops where executed.
    """

    return frozenset(self.ports_usage(min_usage).keys())

  def to_json(self) -> str:
    """
      Convert to JSON string (one line of text)
    """
    return json.dumps(self.to_jsonish())

  @staticmethod
  def from_json(txt) -> 'Benchmark_Run':
    """
      Parse from JSON string
    """
    data = json.loads(txt)
    assert type(data) is dict

    return Benchmark_Run.from_jsonish(data)

  def write_msgpack(self, fd: ty.IO[str]):
    """
      Write to file in msgpack format
    """
    msgpack.pack(self.to_jsonish(), fd)

  @staticmethod
  def read_msgpack(unpacker: msgpack.Unpacker) -> 'Benchmark_Run':
    """
      Read from file in msgpack format
    """
    data = unpacker.unpack()
    assert type(data) is dict

    return Benchmark_Run.from_jsonish(data)

  def to_jsonish(self) -> dict:
    out = {}

    for slot in self._yaml_slots_:
      assert slot.yaml_name not in out

      val = getattr(self, slot.py_name)

      if slot.type is Statistics:
        val = val.to_jsonish()
      elif slot.type is Benchmark_Spec:
        val = self._Benchmark_Spec_to_json(val)
      elif slot.type is datetime.datetime:
        val = self._datetime_to_json(val)
      elif slot.type is datetime.timedelta:
        val = val.total_seconds()
      elif slot.type is ty.List[Event_Set_Run]:
        val = [self._Event_Set_Run_to_json(e) for e in val]
      else:
        raise TypeError(slot.type)

      out[slot.yaml_name] = val

    return out

  @staticmethod
  def from_jsonish(data: dict) -> 'Benchmark_Run':
    out = {}

    for slot in Benchmark_Run._yaml_slots_:
      val = data[slot.yaml_name]

      if slot.type is Statistics:
        val = Statistics.from_jsonish(val)
      elif slot.type is Benchmark_Spec:
        val = Benchmark_Run._Benchmark_Spec_from_json(val)
      elif slot.type is datetime.datetime:
        val = Benchmark_Run._datetime_from_json(val)
      elif slot.type is datetime.timedelta:
        val = datetime.timedelta(seconds=val)
      elif slot.type is ty.List[Event_Set_Run]:
        val = [Benchmark_Run._Event_Set_Run_from_json(e) for e in val]
      else:
        raise TypeError(slot.type)

      out[slot.py_name] = val

    return Benchmark_Run(**out)

  @staticmethod
  def _Benchmark_Spec_to_json(spec) -> dict:
    out = {
      'name':               spec.name,
      'kind':               spec.kind.name,
      'unroll_factor':      spec.unroll_factor,
      'kernel_iterations':  spec.kernel_iterations,
      'arch':               spec.arch.name,
      'instructions':       [i.name for i in spec.instructions],
      'loop_overhead':      {'instructions': spec.loop_overhead.instructions,
                             'muops': spec.loop_overhead.muops}
    }
    return out

  @staticmethod
  def _Benchmark_Spec_from_json(obj: dict) -> Benchmark_Spec:
    arch            = ir.Architecture.for_name(obj['arch'])
    instruction_set = arch.instruction_set()
    instructions    = [instruction_set[i] for i in obj['instructions']]

    return Benchmark_Spec(
      name              = obj['name'],
      kind              = Benchmark_Kind[obj['kind']],
      unroll_factor     = obj['unroll_factor'],
      kernel_iterations = obj['kernel_iterations'],
      arch              = arch,
      instructions      = instructions,
      loop_overhead     = Loop_Overhead(obj['loop_overhead']['instructions'],
                                        obj['loop_overhead']['muops'],)
    )

  @staticmethod
  def _Event_Set_Run_to_json(event_set_run) -> dict:
    d = {}

    for slot in Event_Set_Run._yaml_slots_:
      val = getattr(event_set_run, slot.py_name)

      if slot.type is Statistics:
        val = val.to_jsonish()
      elif slot.type is ty.Optional[Statistics]:
        if val is not None:
          val = val.to_jsonish()
      elif slot.type is ty.Dict[int, Statistics]:
        val = {k: s.to_jsonish() for k, s in val.items()}
      elif slot.type is ty.Dict[str, Statistics]:
        val = {k: s.to_jsonish() for k, s in val.items()}

      d[slot.yaml_name] = val

    return d

  @staticmethod
  def _Event_Set_Run_from_json(d: dict) -> Event_Set_Run:
    out = {}

    for slot in Event_Set_Run._yaml_slots_:
      val = d.get(slot.yaml_name)

      if slot.type is Statistics:
        val = Statistics.from_jsonish(val)
      elif slot.type is ty.Optional[Statistics]:
        if val is not None:
          val = Statistics.from_jsonish(val)
      elif slot.type is ty.Dict[int, Statistics]:
        val = {int(k): Statistics.from_jsonish(s) for k, s in val.items()}
      elif slot.type is ty.Dict[str, Statistics]:
        val = {k: Statistics.from_jsonish(s) for k, s in val.items()}
      else:
        raise TypeError(slot.type)

      out[slot.py_name] = val

    return Event_Set_Run(**out)

  @staticmethod
  def _datetime_to_json(date) -> str:
    return date.strftime('%Y-%m-%d %H:%M:%S.%f')

  @staticmethod
  def _datetime_from_json(date_str) -> datetime.datetime:
    return datetime.datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S.%f')


class Benchmark_Run_Aggregator:
  """
    Helper for filtering & aggregating Benchmark_Run objects.
  """

  DEFAULT_MIN_STDDEV  = 0.0
  DEFAULT_MAX_STDDEV  = 0.1
  DEFAULT_MIN_SAMPLES = 1000

  def __init__(self, *,
               min_stddev: float = DEFAULT_MIN_STDDEV,
               max_stddev: float = DEFAULT_MAX_STDDEV,
               min_samples: int = DEFAULT_MIN_SAMPLES,
               predicate: ty.Callable[[ty.Sequence[str]], bool] = None,):
    self.min_stddev  = min_stddev
    self.max_stddev  = max_stddev
    self.min_samples = min_samples
    self.predicate   = predicate

    self._measurements = collections.defaultdict(list)

  def add_measurement(self, run: Benchmark_Run) -> bool:
    assert type(run) is Benchmark_Run, type(run)

    ## user provided filters
    if run.ipc.num_samples < self.min_samples or run.fmpc.num_samples < self.min_samples or \
       run.umpc.num_samples < self.min_samples:
      return False

    if run.ipc.stddev < self.min_stddev or run.fmpc.stddev < self.min_stddev or run.umpc.stddev < self.min_stddev:
      return False

    if run.ipc.stddev > self.max_stddev or run.fmpc.stddev > self.max_stddev or run.umpc.stddev > self.max_stddev:
      return False

    if self.predicate and self.predicate(run.kernel):
      return False

    key = self._normalize_key(run.kernel)
    heapq.heappush(self._measurements[key], self.Item(run))
    return True

  def force_add(self, run: Benchmark_Run) -> bool:
    assert type(run) is Benchmark_Run, type(run)

    key = self._normalize_key(run.kernel)
    heapq.heappush(self._measurements[key], self.Item(run))

  def kernels_with_instruction(self, inst: str) -> ty.Iterable[ty.Tuple[str]]:
    for run in self.all_measurements():
      if inst in run.kernel:
        yield run.kernel

  def remove_measurement(self, insts: ty.Sequence[str]):
    key = self._normalize_key(insts)
    self._measurements.pop(key, None)

  def __getitem__(self, key: ty.Sequence[str]) -> Benchmark_Run:
    key = self._normalize_key(key)
    try:
      return self._measurements[key][0].run
    except IndexError:
      raise KeyError(key)

  def get(self, key: ty.Sequence[str]) -> Benchmark_Run:
    key = self._normalize_key(key)
    heap = self._measurements[key]
    try:
      return heap[0].run
    except IndexError:
      return None

  def num_runs(self, key: ty.Sequence[str]) -> int:
    key = self._normalize_key(key)
    heap = self._measurements[key]
    return len(heap)

  def __contains__(self, key: ty.Sequence[str]):
    key = self._normalize_key(key)
    return key in self._measurements

  def __iter__(self) -> ty.Iterable[Benchmark_Run]:
    for heap in self._measurements.values():
      if heap:
        yield heap[0].run

  def all_measurements(self) -> ty.Iterable[Benchmark_Run]:
    for heap in self._measurements.values():
      for item in heap:
        yield item.run

  def __len__(self) -> int:
    return sum(len(heap) for heap in self._measurements.values())

  ######################################################################################################################
  ##### I/O

  class File_Format(enum.Enum):
    JSONL   = '.jsonl'
    MSGPACK = '.msgpack'

  def read_from_file(self, path: pathlib.Path, format: File_Format = None):
    """
      Read measurements from file.
      If not given, file format is detected from the suffix of :path:. If it has no known prefix we default to JSONL
    """

    if format is None:
      if path.suffix == '.jsonl':
        format = self.File_Format.JSONL
      elif path.suffix == '.msgpack':
        format = self.File_Format.MSGPACK
      else:
        format = self.File_Format.JSONL

    if format == self.File_Format.JSONL:
      with open(path, 'r') as fd:
        return self.read_jsonl_file(fd)
    elif format == self.File_Format.MSGPACK:
      with open(path, 'rb') as fd:
        return self.read_msgpack_file(fd)
    else:
      print('error: unknown file format:', repr(str(path)), file=sys.stderr)
      exit(1)

  def write_to_file(self, path: pathlib.Path, format: File_Format = None, only_best: bool = False):
    """
      Write measurements to file.
      If not given, file format is detected from the suffix of :path:. If it has no known prefix we default to JSONL
    """

    if format is None:
      if path.suffix == '.jsonl':
        format = self.File_Format.JSONL
      elif path.suffix == '.msgpack':
        format = self.File_Format.MSGPACK
      else:
        format = self.File_Format.JSONL

    if format == self.File_Format.JSONL:
      with open(path, 'w') as fd:
        return self.write_jsonl_file(fd, only_best=only_best)
    elif format == self.File_Format.MSGPACK:
      with open(path, 'wb') as fd:
        return self.write_msgpack_file(fd, only_best=only_best)
    else:
      print('error: unknown file format:', repr(str(path)), file=sys.stderr)
      exit(1)

  def read_jsonl_file(self, fd: ty.IO[str]) -> int:
    """
      Read measurements from file in JSONL format (one json dict per line of text)

      Returns the number of records read
    """

    from_json       = Benchmark_Run.from_json
    add_measurement = self.add_measurement

    i = 0
    for line in fd:
      add_measurement(from_json(line))
      i += 1

    return i

  def write_jsonl_file(self, fd: ty.IO[str], only_best: bool = False) -> int:
    """
      Write measurements to file in JSONL format (one json dict per line of text)

      If :only_best: is true only the run with the best unfused MPC for each benchmark will be stored

      Returns the number of records written
    """

    to_jsonish = Benchmark_Run.to_jsonish
    dump       = json.dump
    write      = fd.write
    runs       = iter(self) if only_best else self.all_measurements()

    i = 0
    for run in runs:
      dump(to_jsonish(run), fd, check_circular=False)
      write('\n')
      i += 1

    return i

  def read_msgpack_file(self, fd: ty.IO[bytes]) -> int:
    """
      Read measurements from file in msgpack binary format

      Returns the number of records read
    """

    from_jsonish    = Benchmark_Run.from_jsonish
    add_measurement = self.add_measurement

    i = 0
    u = msgpack.Unpacker(fd, raw=False)
    for obj in u:
      add_measurement(from_jsonish(obj))
      i += 1

    return i

  def write_msgpack_file(self, fd: ty.IO[bytes], only_best: bool = False) -> int:
    """
      Write measurements to file in msgpack binary format

      If :only_best: is true only the run with the best unfused MPC for each benchmark will be stored

      Returns the number of records written
    """

    to_jsonish = Benchmark_Run.to_jsonish
    pack       = msgpack.Packer(use_bin_type=True).pack
    write      = fd.write
    runs       = iter(self) if only_best else self.all_measurements()

    i = 0
    for run in runs:
      write(pack(to_jsonish(run)))
      i += 1

    return i

  ######################################################################################################################
  ##### private parts

  @staticmethod
  def _normalize_key(key):
    if type(key) is str:
      return (key,)

    if not key:
      return ()

    key = collections.Counter(key)

    gcd = functools.reduce(math.gcd, key.values(), next(iter(key.values())))

    for k, v in key.items():
      key[k] = v // gcd

    return tuple(sorted(key.elements()))

  @dataclasses.dataclass(frozen=True)
  class Item:
    run: Benchmark_Run

    def __lt__(self, that):
      # reverse comparison
      return self.run.umpc.mean >= that.run.umpc.mean

