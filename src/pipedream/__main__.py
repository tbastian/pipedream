#!/usr/bin/env python3

import sys


def main(argv):
  import pipedream.benchmark.__main__

  def cmd_ilp(argv):
    import pipedream.ilp.__main__
    return pipedream.ilp.__main__.main(argv)

  CMDS = {
    'benchmark': pipedream.benchmark.__main__.main,
    'ilp':       cmd_ilp,
  }
  cmd  = CMDS.get(argv[0]) if argv else None
  argv = argv[1:]

  if not cmd:
    fst = 'usage:'
    snd = '      '
    for cmd in CMDS:
      print(fst, cmd, '...')
      fst = snd
    exit(1)

  cmd(argv)


if __name__ == '__main__':
  try:
    main(sys.argv[1:])
  except KeyboardInterrupt:
    exit(1)
