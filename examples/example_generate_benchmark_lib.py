#!/usr/bin/env python3

__doc__ = '''
  Example showing how to use the pipedream benchmark module as a library.
  Generates a benchmark library but does not run it.
'''

import pathlib
import sys
from typing import *

try:
  import pipedream
except ImportError:
  sys.path.append(str(pathlib.Path(__file__).parent.parent / 'src'))

import pipedream.benchmark as pipebench
import pipedream.asm.ir    as pipeasm


def main():
  instruction_names = [
    'ADDPD_VR128f64x2_MEM64f64x2',
    'ADD_GPR64i64_MEM64i64',
    'ADD_GPR64i64_MEM64i64',
  ]

  ## optional, but will make your benchmark results much more reliable
  pipebench.set_scheduler_params()

  ## set up benchmark

  arch         = pipeasm.Architecture.for_name('x86')
  inst_set     = arch.instruction_set()
  instructions = [inst_set.instruction_for_name(name) for name in instruction_names]

  benchmark: pipebench.Benchmark_Spec = pipebench.Benchmark_Spec.from_instructions(
    arch=arch,
    kind=pipebench.Benchmark_Kind.THROUGHPUT,
    ## kernel will contain these instructions, in this order
    instructions             = instructions,
    ## benchmark will execute this many instructions in total (~approximate)
    num_dynamic_instructions = 2_000_000,
    ## benchmark kernel will be unrolled to be this many instructions long
    unrolled_length          = 200,
  )

  ## generate benchmark library

  should_measure_performance_with_papi: bool = False

  lib: Benchmark_Lib = pipebench.gen_benchmark_lib(
    arch                  = arch,
    benchmarks            = [benchmark],
    ## how often is each benchmark run?
    num_iterations        = 50,
    ## where should we store the files we generate?
    dst_dir = 'directory-where-the-libary-will-be-placed',
    ## extra log messages
    verbose = False,
    ## generate error checks in around benchmark code
    debug = False,
    ## benchmarks don't call out to PAPI.
    gen_papi_calls = should_measure_performance_with_papi,
  )

  ## print results

  print('Generated ASM file with benchmark code: ', lib.asm_file)
  print('Object file assembled from ASM file:    ', lib.object_file)
  print('Dynamic libray linked from object file: ', lib.shared_lib_file)

  print('Dynamic library contains', len(lib.benchmarks), 'benchmark function(s):')
  for bench in lib.benchmarks:
    fn = lib.benchmark_function(bench)

    print('  benchmark:    ', bench.name)
    print('  function name:', fn.__name__)
    print()

    run_the_benchmark: bool = False

    if run_the_benchmark:
      if should_measure_performance_with_papi:
        raise RuntimeError('need to set up PAI, use pipebench.run_benchmarks instead')
      else:
        fn()

if __name__ == '__main__':
  main()
